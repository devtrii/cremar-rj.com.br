(function($, window) {
	$(document).on({
		'ready': function(e) {
//$(document).ready(function(){

	$( '#billing_phone' ).addClass( 'trii-masks-telefone-ddd' );
	$( '#billing_postcode' ).addClass( 'trii-masks-cep' );


	$('.trii-masks-data').mask('00/00/0000');
	$('.trii-masks-hora').mask('00:00:00');
	$('.trii-masks-data-hora').mask('00/00/0000 00:00:00');
	$('.trii-masks-cep').mask('00000-000');
	$('.trii-masks-cpf').mask('000.000.000-00');
	$('.trii-masks-cnpj').mask('00.000.000/0000-00');
	$('.trii-masks-dinheiro').mask('000.000.000.000.000,00', {reverse: true});
	$('.trii-masks-dinheiro2').mask("#.##0,00", {reverse: true});
	$('.trii-masks-percent').mask('##0,00%', {reverse: true});

	// Telefone
	var options =  {
		onKeyPress: function(telefone, e, field, options){
			var masks = ['0000-0000Z', '00000-0000'];
			mask = ( telefone.length >= 10 ) ? masks[1] : masks[0];
			$('.trii-masks-telefone').mask(mask, options);
		},
		translation: {
	      'Z': {
	        pattern: /[0-9]/, optional: true
	      }
	    }

	};
	$('.trii-masks-telefone').mask('0000-0000', options);

	// Telefone com DDD
	var options =  {
		onKeyPress: function(telefone, e, field, options){
			var masks = ['(00) 0000-0000Z', '(00) 00000-0000'];
			mask = ( telefone.length > 14 ) ? masks[1] : masks[0];
			field.mask(mask, options);
		},
		translation: {
	      'Z': {
	        pattern: /[0-9]/, optional: true
	      }
	    }
	};
	$('.trii-masks-telefone-ddd').mask('(00) 00000-0000', options);

	// CPF ou CNPJ
	var options =  {
		onKeyPress: function(cpf, e, field, options){
			var masks = ['000.000.000-000Z', '00.000.000/0000-00'];
			mask = ( cpf.length > 14 ) ? masks[1] : masks[0];
			$('.trii-masks-cpf-cnpj').mask(mask, options);
		}
	};
	$('.trii-masks-cpf-cnpj').mask('000.000.000-000', options);


	// IP

	$('.trii-masks-ip').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
		translation: {
			'Z': {
				pattern: /[0-9]/, optional: true
			}
		}
	});

//});
		} // Document Ready
	});
})(jQuery);