<?php
/**
 * Plugin Name: Trii - Masks
 * Plugin URI:
 * Description: Uses jquery mask plugin to create default masks classes
 * Version: 2.1
 * Author: Raphael Freitas @ Trii
 * Author URI: 
 * 
 * RELEASE NOTES:
 * 	2.0 - 12/11/2015 - Versão Inicial
 *  2.1 - 23/11/2015 - Auto apagar campo de telefone se não tiver número mínimo de caracteres.
 */

function trii_masks(){

	$folder = plugins_url('trii-masks');

    //echo '<script type="text/javascript" src="'.$folder.'/js/jquery-2.1.4.min.js"></script>';
	wp_deregister_script('jquery');
    wp_register_script('jquery', $folder . '/js/jquery-2.1.4.min.js', false, '2.1.4');
    wp_enqueue_script( 'jquery-mask-min-js', $folder . '/js/jquery.mask.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'trii-mask-min-js', $folder . '/js/trii.mask.min.js', array('jquery'), '1.0.0', true );

}
add_action('wp_head', 'trii_masks');
