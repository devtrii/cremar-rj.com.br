<?php
	
	if( isset($_GET['opt']) && $_GET['opt'] != "" ){

		require_once "../../../../../wp-config.php";

		header('Content-type: text/javascript');

		$option = get_option( 'tstp_options', true );

		$option = (array) $option;

		$option = str_replace('<script>', '',$option[$_GET['opt']]);

		$option = str_replace('</script>', '',$option);
		
		$option = str_replace('type="text/javascript"', '',$option);

		$option = str_replace('\\', '',$option);

		echo $option;
	}
?>