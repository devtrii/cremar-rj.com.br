$ = jQuery;

$(document).on('click', '.tabs .tab', function(e){

	currentTab = $(this).parent('.tabs').find('.tab.active').data('tab');
	newTab = $(this).data('tab');

	$(this).parent('.tabs').find('.tab').removeClass('active');
	$(this).addClass('active');

	$('#'+currentTab).removeClass('active');
	$('#'+newTab).addClass('active');

});

$( document ).on( 'submit', '#codigos form', function(e){

	var $js 		= $( '.isJS' ),
		$html 		= $( '.isHTML' ),
		errorJS		= 0,
		errorHTML	= 0;

	$js.each( function(){

		var $this = $(this);

		var chkEl = document.createElement('div'), 
		    isok,
		    value = $this.val();

		if ( !value.length ) { return true; }

		chkEl.innerHTML = value;
		
		var tags 	= chkEl.getElementsByTagName('*'),
			qtd 	= chkEl.getElementsByTagName('*').length;

		$.each( tags, function( key, val ){
			if( val.nodeName == 'SCRIPT' ){
				qtd--;
			}
		});

		isok = !qtd;

		if( !isok ){
			$this.css({'border-color': 'red'}).addClass('hasError');
			errorJS++;
		}else{
			$this.css({'border-color': '#ddd'}).removeClass('hasError');
		}

	});

	$html.each( function(){

		var $this 	= $(this),
			valor 	= $this.val();

		if ( !valor.length ) { return true; }

		if( valor.indexOf( 'function' ) != -1 ||
			valor.indexOf( 'document' ) != -1 ||
			valor.indexOf( 'window' ) != -1 ){
			$this.css({'border-color': 'red'}).addClass('hasError');
			errorHTML++;
		}else{
			$this.css({'border-color': '#ddd'}).removeClass('hasError');
		}

	});

	if( errorJS > 0 || errorHTML > 0 ){
		e.preventDefault();

		$('html, body').animate({
	        scrollTop: $(".hasError").first().offset().top - 200
	    }, 1000);
	}

	if( errorJS > 0 && errorHTML <= 0 ){
		alert( 'Você não pode adicionar HTML no campo de JAVASCRIPT.' );
	}else if( errorHTML > 0 && errorJS <= 0 ){
		alert( 'Você não pode adicionar JAVASCRIPT no campo de HTML.' );
	}else if( errorHTML > 0 && errorJS > 0 ){
		alert( 'Você adicionou HTML no campo de JAVASCRIPT e JAVASCRIPT no campo de HTML, recomendamos que você peça ajuda a um desenvolvedor.' );
	}

});