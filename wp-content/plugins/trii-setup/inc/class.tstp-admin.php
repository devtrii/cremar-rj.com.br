<?php
class TSTP_Admin {
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'tstp_register_menu' ) );
		add_action( 'admin_init', array( $this, 'tstp_save_data' ) );
		add_action( 'admin_init', array( $this, 'tstp_save_file' ) );
		add_action( 'admin_init', array( $this, 'tstp_save_robots' ) );

		add_action( 'wp_footer', array( $this, 'tstp_load_codes_page' ), 999 );
	}

	public function tstp_register_menu() {
		add_options_page( 'Trii Setup', 'Trii Setup', 'manage_options', 'tstp-settings', array( $this, 'tstp_menu_view' ) );
	}

	public function tstp_menu_view() {

		wp_enqueue_style( 'tstp_css', TSTP__PLUGIN_URL . 'assets/css/style.css');

		wp_enqueue_script( 'tstp_js', TSTP__PLUGIN_URL . 'assets/js/main.js', array('jquery') );

		require_once TSTP__PLUGIN_DIR . 'view/settings.php';
	}

	public static function tstp_get_data() {
		$options['html'] = get_option( 'tstp_options', false );

		$locations = array( 
				'inicio_head','fim_head',
				'inicio_body','fim_body',
				'inicio_footer','fim_footer'
				);

		foreach ($locations as $key => $location) {
			if( @file_exists( TSTP__PLUGIN_DIR.'assets/js/'.$location.'.js' ) ){ 
				$options['javascript'][$location] = file_get_contents(TSTP__PLUGIN_DIR.'assets/js/'.$location.'.js');
			}
		}
		
		return ( $options );
	}

	public function tstp_save_data() {
		if ( isset( $_POST['tstp_submit_data'] ) && $_GET['page'] == 'tstp-settings' ){
			$option = self::tstp_get_data();

			// HTML			
			$htmlData = $_POST['tstp_html'];
			$newHtmlData = array();
			foreach ($htmlData as $key => $value) {
				$newHtmlData[$key] = stripslashes($value); 
			}

			$newHtmlData = (object) $newHtmlData;
			update_option( 'tstp_options', $newHtmlData );

			// Javascript
			$jsData = $_POST['tstp_js'];
			$newJsData = array();
			foreach ($jsData as $key => $value) {

				$value = str_replace('<script>', '', $value);
				$value = str_replace('</script>', '', $value);
				$value = str_replace('"text/javascript"', '', $value);

				file_put_contents(TSTP__PLUGIN_DIR.'assets/js/'.$key.'.js', stripslashes($value));
			}
	
			$option = TSTP_Admin::tstp_get_data();

			echo '<div id="message" class="updated notice is-dismissible"><p>Códigos <strong>salvos com sucesso</strong>!</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
		}
	}

	public function tstp_load_codes_page(){

		if( !is_admin() ){
			wp_enqueue_script( 'jquery' );
			require_once TSTP__PLUGIN_DIR . 'inc/load_codes_page.php';
		}

	}

	public function tstp_save_file() {
		if ( isset( $_POST['tstp_send_file'] ) && $_GET['page'] == 'tstp-settings' ){

			$fileDir = TSTP_Admin::root_path();

			if (!is_dir($fileDir) or !is_writable($fileDir)) {
			    echo '<div id="message" class="error notice is-dismissible"><p><strong>Erro:</strong> O arquivo <i>'.$_FILES['arquivo_raiz']['name'].'</i> não pode ser enviado pois a pasta de destino não existe ou não tem permissões para enviar o arquivo!</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';

			    return false;
			}


			if ( $_FILES['arquivo_raiz']['type'] != 'text/html' ) {
			    echo '<div id="message" class="error notice is-dismissible"><p><strong>Erro:</strong> Extensão inválida, só é permitido enviar aquivo com extensão <i>.html</i>!</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';

			    return false;
			}

			if ( !@file_exists($fileDir.'/'.$_FILES['arquivo_raiz']['name']) ){

				file_put_contents($fileDir.'/'.$_FILES['arquivo_raiz']['name'], file_get_contents($_FILES['arquivo_raiz']['tmp_name']));

				echo '<div id="message" class="updated notice is-dismissible"><p>Arquivo <i>'.$_FILES['arquivo_raiz']['name'].'</i> <strong>enviado com sucesso</strong>! <small>(Para remover o arquivo você precisará acessar a pasta raiz via FTP).</small></p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
			} else{
				echo '<div id="message" class="error notice is-dismissible"><p><strong>Erro:</strong> O arquivo <i>'.$_FILES['arquivo_raiz']['name'].'</i> já existe na pasta raiz!</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
			}
		}
	}

	public function root_path(){
	    $base = dirname(__FILE__);
	    $path = false;

	    if (@file_exists(dirname(dirname($base))."/wp-config.php"))
	    {
	        $path = dirname(dirname($base));
	    }
	    else
	    if (@file_exists(dirname(dirname(dirname($base)))."/wp-config.php"))
	    {
	        $path = dirname(dirname(dirname($base)));
	    }
	    else
	    if (@file_exists(dirname(dirname(dirname(dirname($base))))."/wp-config.php"))
	    {
	        $path = dirname(dirname(dirname(dirname($base))));
	    }
	    else
	    $path = false;

	    if ($path != false)
	    {
	        $path = str_replace("\\", "/", $path);
	    }
	    return $path;
	}

	public function tstp_get_robots() {

		if( @file_exists( TSTP_Admin::root_path().'/robots.txt' ) ){
			$robots = file_get_contents(TSTP_Admin::root_path().'/robots.txt');
		} else{
			$robots = "";
			file_put_contents(TSTP_Admin::root_path().'/robots.txt', '');
		}

		return $robots;
	}

	public function tstp_save_robots() {
		if ( isset( $_POST['tstp_submit_robots'] ) && $_GET['page'] == 'tstp-settings' ){

			file_put_contents(self::root_path().'/robots.txt', $_POST['robots_text']);

			echo '<div id="message" class="updated notice is-dismissible"><p>Arquivo robots.txt <strong>modificado com sucesso</strong>!</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>';
		}
	}

}
new TSTP_Admin();
