<?php
$option = (array) TSTP_Admin::tstp_get_data();
$option['html'] = (array) $option['html'];
$option['javascript'] = (array) $option['javascript'];

//echo "<pre>".print_r($option, true)."</pre>";

if( is_dir( TSTP_Admin::root_path() ) ){
	$files = scandir( TSTP_Admin::root_path() );
}

$robots = TSTP_Admin::tstp_get_robots();


$locations = array(
	'inicio_head'=>'início do head',
	'fim_head'=>'fim do head',
	'inicio_body'=>'início do body',
	'fim_body'=>'fim do body',
	'inicio_footer'=>'início do footer',
	'fim_footer'=>'fim do footer',
);

$colors = array(
	'inicio_head'=>'#FF0000',
	'fim_head'=>'#20B2AA',
	'inicio_body'=>'#FF1493',
	'fim_body'=>'#1E90FF',
	'inicio_footer'=>'#3CB371',
	'fim_footer'=>'#4B0082',
);

?>
<style type="text/css">
	.box-ajuda {
		width: 100%;
		background: #f1bbbb;
		border-radius: 5px;
		margin-top: 20px;
		margin-bottom: 10px;
		box-sizing: border-box;
	}
	.box-ajuda .panel{
		color: #FFF;
		padding: 15px;
	}
	
	.box-panel {
		width: 100%;
		display: inline-block;
		padding: 20px;
		background: white;
		box-sizing: border-box;
		border: 4px solid #f1bbbb;
	}
	xmp {
	    display: inline-block;
	    margin: 0;
	    color: red;
	    font-weight: bold;
	    white-space: pre-wrap;
	}
	.col-half {
	    width: 45%;
	    float: left;
	    box-sizing: border-box;
	    padding-right: 0;
	    margin-right: 10%;
	}
	textarea{
		width: 100%;
	}
</style>
<div class="wrap">
	<h2>Trii Setup</h2>

	<div class="tabs">
		<div class="tab <?php echo ( isset( $_POST['tstp_submit_data'] ) || ( !isset($_POST['tstp_send_file']) && !isset($_POST['tstp_submit_robots']))  ? 'active' : '' ); ?>" data-tab="codigos">Códigos</div>
		<div class="tab <?php echo ( isset( $_POST['tstp_send_file'] ) ? 'active' : '' ); ?>" data-tab="upload">Upload</div>
		<div class="tab <?php echo ( isset( $_POST['tstp_submit_robots'] ) ? 'active' : '' ); ?>" data-tab="robots">Robots</div>
	</div>

	<div id="codigos" class="tab-container <?php echo ( isset( $_POST['tstp_submit_data'] ) || ( !isset($_POST['tstp_send_file']) && !isset($_POST['tstp_submit_robots']))  ? 'active' : '' ); ?>">

		<div class="box-ajuda">
			<div class="panel">Precisa de ajuda para entender oque é HTML e JS ?</div>
			<div class="box-panel">
				<div class="col-half">
					<h2>Oque é o campo JAVASCRIPT ?</h2>
					Basicamente o javascript serve para controlar o HTML e CSS que é exibido na página, e também, para incluir funcionalidades de terceiros no site, exemplo: botão de like do facebook.
					<br>
					<br>
					Exemplo de códigos JS:<br><xmp>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1684002708540174');
fbq('track', "PageView");</xmp>

				</div>

				<div class="col-half" style="margin-right: 0px;">
					<h2>Oque é o campo HTML ?</h2>
					O html possúi tags, tags são rótulos usados para informar ao navegador como deve ser apresentado o website. Todas as tags têm o mesmo formato: começam com um sinal de menor "<" e acabam com um sinal de maior ">". Genericamente falando, existem dois tipos de tags - tags de abertura: <xmp><comando> e tags de fechamento: </comando></xmp>.
					<br>
					<br>
					Na utilização desse plugin as tags de HTML mais comuns são: <b>noscript</b>, <b>img</b> e por fim a tag <b>script</b> QUE TENHA O ATRIBUTO SRC.
					<br>
					<br>
					Exemplo de código HTML:<br><xmp><noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1684002708540174&ev=PageView&noscript=1"/></noscript></xmp>
					<br>
					<br>
					Exemplo com a tag script que tem o atributo SRC: <br><xmp><script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"/></xmp>
				</div>
			</div>
		</div>

		<form method="POST" name="atualizar_codigos">
			<table>
				<tr>
					<td colspan="2"><h3>Códigos</h3></td>
				</tr>
						
				<?php

					foreach ($locations as $key => $location) {
					$cor = $colors[$key];
				?>

				<tr valign="bottom" style="height: 50px;vertical-align: bottom;">
					<td colspan="2">
						<label style="font-size: 16px;font-weight: bold;color:<?php echo $cor; ?>">Código no <?php echo $location;?>:</label>
					</td>
				</tr>

				<tr style="color:#999; width: 50%; float: left;">
					<td>
						<strong style="color:<?php echo $cor; ?>;display: block;">JAVASCRIPT</strong>
						<textarea name="tstp_js[<?php echo $key; ?>]" rows="10" cols="100" class="isJS"><?php echo $option['javascript'][$key]; ?></textarea>
					</td>
				</tr>

				<tr style="color:#999; width: 50%; float: left;position: relative;    right: -20px;">
					<td>
						<strong style="color:<?php echo $cor; ?>; display: block;">HTML</strong>
						<textarea name="tstp_html[<?php echo $key; ?>]" rows="10" cols="100" class="isHTML"><?php echo $option['html'][$key]; ?></textarea>
					</td>
				</tr>

				<tr>
					<td>
						<hr />
					</td>
				</tr>

				<?php } ?>
								
				<tr>
					<td colspan="2">
						<?php submit_button('Salvar', 'button-primary', 'tstp_submit_data'); ?>
					</td>
				</tr>
			</table>
		</form>

	</div>

	<div id="upload" class="tab-container <?php echo ( isset( $_POST['tstp_send_file'] ) ? 'active' : '' ); ?>">

		<form method="POST" name="upload" enctype="multipart/form-data">
			<table>
				<tr>
					<td>
						<h3>Upload</h3>
						Selecione algum arquivo para enviar para a raiz do site
						<small>(somente arquivo HTML)</small>.
					</td>
				</tr>
				<tr>
					<td><input type="file" name="arquivo_raiz" accept='.html' required /></td>
				</tr>
				<tr>
					<td>
						<?php submit_button('Enviar', 'button-primary', 'tstp_send_file'); ?>
					</td>
				</tr>
			</table>
		</form>

		Arquivos na pasta:<br>
		<small>(<?php echo TSTP_Admin::root_path(); ?>)</small><br><br>
		<table border="0" class="table" width="300px">
			<?php foreach ($files as $key => $file) { 
					if( $file == '.' || $file == '..' ) continue;
				?>
			<tr>
				<td><?php echo $file; ?></td>
			</tr>
			<?php } ?>
		</table>

	</div>

	<div id="robots" class="tab-container <?php echo ( isset( $_POST['tstp_submit_robots'] ) ? 'active' : '' ); ?>">

		<form method="POST" name="upload">
			<table>
				<tr>
					<td><h3>Robots</h3></td>
				</tr>
				<tr>
					<td>
						<label>Arquivo robots.txt:</label><br>
						<textarea name="robots_text" rows="15" cols="120"><?php echo $robots; ?></textarea>
					</td>
				</tr>										
				<tr>
					<td>
						<?php submit_button('Salvar', 'button-primary', 'tstp_submit_robots'); ?>
					</td>
				</tr>
			</table>
		</form>

	</div>

</div>
