<?php
/*
Plugin Name: Trii - Setup
Description: Configurações padrões
Version: 1.3

RELEASE NOTES:
  • 1.0 - 24/11/2015 - Versão Inicial
  • 1.1 - 07/02/2017 - Filtro para não adicionar html no campo js.
  • 1.2 - 15/02/2017 - Ajuste no uso do jQuery no arquivo load_codes_page.php.
  • 1.3 - 07/03/2019 - Remoção total do uso de Jquery para melhor combatibilidade com o page speed insights
*/

define( 'TSTP__VERSION', '1.3' );
define( 'TSTP__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'TSTP__PLUGIN_URL', plugin_dir_url( __FILE__ ) );

require_once TSTP__PLUGIN_DIR . 'inc/load.php';
