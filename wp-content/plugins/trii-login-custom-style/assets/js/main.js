(function($, window) {
	$(document).on({
		'ready': function(e) {

			Visualizacao();

			$( document ).on( 'change', '.jscolor', function(){
				Visualizacao();
			});

			// Uploading files
			var file_frame;
			var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
			var set_to_post_id = 1; // Set this

			jQuery( '.btn-select-img' ).on( 'click', function( event ){
				event.preventDefault();
				
				if( $( '.rowIMG' ).length > 11 ){
					bootbox.alert( 'Você só pode colocar até 12 imagens.' );
					return false;
				}
			     // If the media frame already exists, reopen it.
				    if ( file_frame != undefined && file_frame ) {
				      file_frame.open();
				      return;
				    }
				 
				    // Create the media frame.
				    file_frame = wp.media.frames.file_frame = wp.media({
				      title: jQuery( this ).data( 'uploader_title' ),
				      button: {
				        text: jQuery( this ).data( 'uploader_button_text' ),
				      },
				      multiple: false  // Set to true to allow multiple files to be selected
				    });
				 
				    // When an image is selected, run a callback.
				    file_frame.on( 'select', function() {
				      // We set multiple to false so only get one image from the uploader
				      attachment = file_frame.state().get('selection').first().toJSON();
						//$('.foto_loader').show();
										      			     
			      		if( attachment.width > 320 || attachment.height > 320 ){
					    	alert( 'Arquivo muito grande, tamanho máximo: 320x320px' );
					    }else if( attachment.subtype != 'jpeg' && attachment.subtype != 'jpg' && attachment.subtype != 'png' ){
				      		alert( 'Extensão de arquivo não permitida. \n \n Extensões Permitidas: JPEG, JPG, PNG' );
				      	}else{

							$( '#logo_url' ).val( attachment.url );
							$( '#logo_width' ).val( attachment.width );
							$( '#logo_height' ).val( attachment.height );
							$( '.logowp' ).attr( 'src', attachment.url );
							$( '.btn-select-img' ).text( 'Imagem Carregada' );

				      	}
				 
				    });
				 
				    file_frame.open();
			});
			// Restore the main ID when the add media button is pressed
			jQuery('a.add_media').on('click', function() {
				wp.media.model.settings.post.id = wp_media_post_id;
			});


		} // Document Ready
	});
})(jQuery);

function Visualizacao(){

	var bg1 = '#' + jQuery( '#bg1' ).val(),
		bg2 = '#' + jQuery( '#bg2' ).val(),
		bg3 = '#' + jQuery( '#bg3' ).val();

	var style =	"background: "+ bg1 +";" +
				"background: -moz-linear-gradient(top, "+ bg1 +" 0%, "+ bg2 +" 47%, "+ bg3 +" 100%);" +
				"background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, "+ bg1 +"), color-stop(47%, "+ bg2 +"), color-stop(100%, "+ bg3 +"));" +
				"background: -webkit-linear-gradient(top, "+ bg1 +" 0%, "+ bg2 +" 47%, "+ bg3 +" 100%);" +
				"background: -o-linear-gradient(top, "+ bg1 +" 0%, "+ bg2 +" 47%, "+ bg3 +" 100%);" +
				"background: -ms-linear-gradient(top, "+ bg1 +" 0%, "+ bg2 +" 47%, "+ bg3 +" 100%);" +
				"background: linear-gradient(to bottom, "+ bg1 +" 0%,"+ bg2 +" 47%,"+ bg3 +" 100%);" +
				"filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='"+ bg1 +"', endColorstr='"+ bg3 +"',GradientType=0 );";

	jQuery( '.visualizar' ).attr( 'style', style );

	//----------- BOX -------------//

	var bg_box 			= '#' + jQuery( '#bg_box' ).val(),
		bg_botao 		= '#' + jQuery( '#bg_botao' ).val(),
		txt_botao		= '#' + jQuery( '#txt_botao' ).val(),
		txt_box			= '#' + jQuery( '#txt_box' ).val(),
		txt_rodape		= '#' + jQuery( '#txt_rodape' ).val(),
		logo_url		= jQuery( '#logo_url' ).val(),
		style_box 		= 'background-color:' + bg_box + ';color:'+ txt_box +';',
		style_box_inner = 'background-color:' + bg_botao + ';color:'+ txt_botao +';border-color:'+ bg_botao +';box-shadow: 0 1px 0 '+ bg_botao +';',
		style_rodape	= 'color:'+ txt_rodape +';';

	jQuery( '.box' ).attr( 'style', style_box );
	jQuery( '.box' ).find( '.button' ).attr( 'style', style_box_inner );
	jQuery( '.rodape' ).attr( 'style', style_rodape );

	if( logo_url != '' && logo_url != undefined ) jQuery( '.logowp' ).attr( 'src', logo_url );


}