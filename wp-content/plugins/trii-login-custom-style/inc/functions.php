<?php
	
	$hasOption = get_option( 'pl_style_login_trii' );
	if( !$hasOption ){
		$optionOBJ = array(
				'habilitar' => 1,
			    'logo' => Array
			        (
			            'url' => PLUGIN_URL . 'assets/image/logo-trii-default.png',
			            'width' => 61,
			            'height' => 37,
			        ),
			    'bg1' => '00E5FF',
			    'bg2' => '0099E6',
			    'bg3' => '006BA1',
			    'bg_box' => 'FFFFFF',
			    'txt_box' => '0099E6',
			    'bg_botao' => '0099E6',
			    'txt_botao' => 'FFFFFF',
			    'txt_rodape' => 'FFFFFF'
			);

		update_option( 'pl_style_login_trii', $optionOBJ );
	}
	unset( $hasOption );

	add_action('admin_menu', 'll_register_options');
	function ll_register_options() {
		add_options_page( 'Trii Page Login - Settings', 'Trii Page Login', 'manage_options', 'll-page-login-settings', 'll_view_page' );
	}

	function ll_view_page() {
		require_once PLUGIN_DIR . 'view/options.php';
	}

	function ll_register_scripsts_head() {
		if ( $_GET['page'] && !$_GET['page'] == 'll-page-login-settings' ) return;
		wp_enqueue_style( 'jquery-ui' );
		wp_enqueue_script( 'll_main-js', PLUGIN_URL . 'assets/js/main.js', array( 'jquery' ), true );
		wp_enqueue_script( 'll_jscolor', PLUGIN_URL . 'assets/js/jscolor.min.js', array( 'jquery' ), true );
		wp_enqueue_style( 'll_style', PLUGIN_URL . 'assets/css/ll_style.css' );
	}
	add_action( 'admin_head', 'll_register_scripsts_head' );

	function is_login_page() {
	    return in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'));
	}

	add_action( 'login_head', 'll_init_configs' );
	function ll_init_configs() {
	    
		if( is_login_page() ){

			$pl_style_login_trii = ( Object ) get_option( 'pl_style_login_trii' );

			if( $pl_style_login_trii->habilitar == 1 && is_object( $pl_style_login_trii ) ){

				//Change Logo
				if( !empty( $pl_style_login_trii->logo['url'] ) ){
					echo "<style type='text/css'>
					        .login h1 a {
					            background-image: url({$pl_style_login_trii->logo['url']}) !important;
					            height: {$pl_style_login_trii->logo['height']}px !important;
					            width: {$pl_style_login_trii->logo['width']}px !important;
					            background-size: cover !important;
					            display: inline-flex !important;
					        }
					    </style>";
				}

				echo "<style type='text/css'>
						.login #login_error, .login .message{
							background-color: #{$pl_style_login_trii->bg_box} !important;
							color: #{$pl_style_login_trii->txt_box} !important;
						}
						#login form p {
							color: #{$pl_style_login_trii->txt_box} !important;
						}
						body.login{
				        	background: #{$pl_style_login_trii->bg1} !important;
				        	background: -moz-linear-gradient(top, #{$pl_style_login_trii->bg1} 0%, #{$pl_style_login_trii->bg2} 47%, #{$pl_style_login_trii->bg3} 100%) !important;
							background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #{$pl_style_login_trii->bg1}), color-stop(47%, #{$pl_style_login_trii->bg2}), color-stop(100%, #{$pl_style_login_trii->bg3})) !important;
							background: -webkit-linear-gradient(top, #{$pl_style_login_trii->bg1} 0%, #{$pl_style_login_trii->bg2} 47%, #{$pl_style_login_trii->bg3} 100%) !important;
							background: -o-linear-gradient(top, #{$pl_style_login_trii->bg1} 0%, #{$pl_style_login_trii->bg2} 47%, #{$pl_style_login_trii->bg3} 100%) !important;
							background: -ms-linear-gradient(top, #{$pl_style_login_trii->bg1} 0%, #{$pl_style_login_trii->bg2} 47%, #{$pl_style_login_trii->bg3} 100%) !important;
							background: linear-gradient(to bottom, #{$pl_style_login_trii->bg1} 0%,#{$pl_style_login_trii->bg2} 47%,#{$pl_style_login_trii->bg3} 100%) !important;
							filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#{$pl_style_login_trii->bg1}', endColorstr='#{$pl_style_login_trii->bg3}',GradientType=0 ) !important;
				        }
				        .login form{
				        	/*box-shadow: 0px 0px 35px 0px #797979 !important;*/
				        	background-color: #{$pl_style_login_trii->bg_box} !important;
				        }
				        .login form .button{
				        	background-color: #{$pl_style_login_trii->bg_botao} !important;
				        	color: #{$pl_style_login_trii->txt_botao} !important;
				        	border-color: #{$pl_style_login_trii->bg_botao} !important;
				        	box-shadow: 0 1px 0 #{$pl_style_login_trii->bg_botao} !important;
				        	text-shadow: initial !important;
				    	}
				    	.login label{
				    		color: #{$pl_style_login_trii->txt_box} !important;
				    	}
				    	.login #nav, .login #backtoblog{
				    		padding: 0px !important;
				    		z-index: 2;
							position: relative;
				    	}
				    	a{
				    		color: #{$pl_style_login_trii->txt_rodape} !important;
				    		cursor: pointer;
				    	}
				    	.login #nav{
				    		float: left;
				    	}
				    	.login #backtoblog {
							float: right;
							margin-top: 24px !important;
							max-width: 54px;
							overflow: hidden;
							height: 19px;
						}
				    	.forgetmenot{
				    		margin-top: 8px !important;
				    	}
				    	input[type=checkbox]:checked:before{
				    		color: #{$pl_style_login_trii->txt_box} !important;
				    	}
				    	.rodape-trii{
				    		text-align: center;
							margin-top: 1em;
							width: 320px;
							margin: auto !important;
							position: relative;
							top: 20px;
							color: #{$pl_style_login_trii->txt_rodape} !important;
			    		}
			    		.rodape-trii hr{
			    			margin-bottom: 10px;
			    			width: 100%;
			    			border: 0px;
							background: #{$pl_style_login_trii->txt_rodape};
							height: 2px;
			    		}
					</style>";

				function ll_my_loginfooter() {
	    			echo '<div class="rodape-trii">
	    					<hr />
	    					Desenvolvido por: <a href="http://trii.com.br" target="_blank" title="Agência Digital no RJ">Agência Trii</a>
						</div>';
				}
				add_action('login_footer','ll_my_loginfooter');

			}

		}

	}
