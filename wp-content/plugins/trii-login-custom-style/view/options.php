<?php
	wp_enqueue_script('media-upload');
	wp_enqueue_media();
	
	if( isset( $_POST['pl_save_settings'] ) ){

		$option = array();

		foreach ( $_POST as $key => $post ) {
			if( $key != 'pl_save_settings' ){
				$option[$key] = $post;
			}
		}

		update_option( 'pl_style_login_trii', $option );

		$sucesso = "Atualizado com sucesso!";
	}

	$pl_style_login_trii = get_option( 'pl_style_login_trii' );
	if( $pl_style_login_trii ) $pl_style_login_trii = ( Object ) $pl_style_login_trii;

	$logo_default = PLUGIN_URL . 'assets/image/logo-trii-default.png';
?>

<div class="wrap">
	<form method="POST" class="half">
		<table class="form-table">
			<tr>
	        	<td>Habilitar</td>
	        	<td>
	        		<input type="checkbox" name="habilitar" id="habilitar" value="1" <?php echo $pl_style_login_trii->habilitar || !is_object( $pl_style_login_trii ) ? 'checked' : ''; ?> /> <label for="habilitar">Sim</label>
	        	</td>
	        </tr>
			<tr>
	        	<td>Logo</td>
	        	<td>
	        		<button type="button" class="btn-select-img"><?php echo $pl_style_login_trii->logo['url'] ? 'Imagem Carregada' : 'Selecione a Imagem'; ?></button>
	        		<input type="hidden" name="logo[url]" id="logo_url" value="<?php echo $pl_style_login_trii->logo['url'] ? $pl_style_login_trii->logo['url'] : $logo_default; ?>" />
	        		<input type="hidden" name="logo[width]" id="logo_width" value="<?php echo $pl_style_login_trii->logo['width'] ? $pl_style_login_trii->logo['width'] : '61'; ?>" />
	        		<input type="hidden" name="logo[height]" id="logo_height" value="<?php echo $pl_style_login_trii->logo['height'] ? $pl_style_login_trii->logo['height'] : '37'; ?>" />
	        	</td>
	        </tr>
	        <tr>
	        	<td>Background Color 1</td>
	        	<td>
	        		<input class="jscolor" name="bg1" id="bg1" value="<?php echo $pl_style_login_trii->bg1 ? $pl_style_login_trii->bg1 : '00E5FF'; ?>" style="margin-bottom:5px;"/ > <br />
	        		<i>Cor do topo do degradê.</i>
	        	</td>
	        </tr>
	        <tr>
	        	<td>Background Color 2</td>
	        	<td>
	        		<input class="jscolor" name="bg2" id="bg2" value="<?php echo $pl_style_login_trii->bg2 ? $pl_style_login_trii->bg2 : '0099E6'; ?>" style="margin-bottom:5px;"/ > <br />
	        		<i>Cor do meio do degradê.</i>
	        	</td>
	        </tr>
	        <tr>
	        	<td>Background Color 3</td>
	        	<td>
	        		<input class="jscolor" name="bg3" id="bg3" value="<?php echo $pl_style_login_trii->bg3 ? $pl_style_login_trii->bg3 : '006BA1'; ?>" style="margin-bottom:5px;"/ > <br />
	        		<i>Cor do final do degradê.</i>
	        	</td>
	        </tr>
	        <tr>
	        	<td>Background Do Box</td>
	        	<td>
	        		<input class="jscolor" name="bg_box" id="bg_box" value="<?php echo $pl_style_login_trii->bg_box ? $pl_style_login_trii->bg_box : 'FFFFFF'; ?>" style="margin-bottom:5px;"/ > <br />
	        		<i>Cor do box de login</i>
	        	</td>
	        </tr>
	        <tr>
	        	<td>Texto Do Box</td>
	        	<td>
	        		<input class="jscolor" name="txt_box" id="txt_box" value="<?php echo $pl_style_login_trii->txt_box ? $pl_style_login_trii->txt_box : '0099E6'; ?>" style="margin-bottom:5px;"/ > <br />
	        	</td>
	        </tr>
	        <tr>
	        	<td>Background Do Botão</td>
	        	<td>
	        		<input class="jscolor" name="bg_botao" id="bg_botao" value="<?php echo $pl_style_login_trii->bg_botao ? $pl_style_login_trii->bg_botao : '0099E6'; ?>" style="margin-bottom:5px;"/ > <br />
	        	</td>
	        </tr>
	        <tr>
	        	<td>Texto Do Botão</td>
	        	<td>
	        		<input class="jscolor" name="txt_botao" id="txt_botao" value="<?php echo $pl_style_login_trii->txt_botao ? $pl_style_login_trii->txt_botao : 'FFFFFF'; ?>" style="margin-bottom:5px;"/ > <br />
	        	</td>
	        </tr>
	        <tr>
	        	<td>Texto Do Rodapé</td>
	        	<td>
	        		<input class="jscolor" name="txt_rodape" id="txt_rodape" value="<?php echo $pl_style_login_trii->txt_rodape ? $pl_style_login_trii->txt_rodape : 'FFFFFF'; ?>" style="margin-bottom:5px;"/ > <br />
	        		<i>Cor dos links no final do box.</i>
	        	</td>
	        </tr>
	        <tr>
				<td scope="row"></td>
				<td><?php submit_button( 'SALVAR', 'primary', 'pl_save_settings', true ); ?></td>
			</tr>
	    </table>
	</form>
	<div class="visualizar half">
		<img src="images/wordpress-logo.svg" class="logowp" />
		<div class="box">
			<label>Nome de usuário</label>
			<input type="text" class="blur" disabled />
			<label>Senha</label>
			<input type="text" class="blur" disabled />
			<div class="lembrar">
				<input type="checkbox" class="blur" disabled />
				<label>Lembrar-me</label>
			</div>
			<button type="button" class="button button-primary">Fazer Login</button>
		</div>
		<div class="rodape">
			<div class="l">Lembrar-me</div>
			<div class="r">Voltar</div>
		</div>
	</div>
	<div class="half half-sucess">
		<?php
			if( isset( $sucesso ) ){
				echo "<div class='updated' style='margin-left: 0px;'><p>{$sucesso}</p></div>";
			}
		?>
	</div>
</div>
