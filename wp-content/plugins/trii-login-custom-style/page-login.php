<?php
/*
Plugin Name: Trii - Login Custom Style
Plugin URI: 
Description: Edition of the logo and the background of the login screen.
Author: Reinaldo Amorim
Version: 1.0
Author URI: http://reinaldoamorim.com.br

RELEASE NOTES:
  • 1.0 - 06/11/2016 - Versão Inicial
*/

define( 'PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'PLUGIN_URL', plugin_dir_url( __FILE__ ) );

require_once PLUGIN_DIR . 'inc/incs.php';
