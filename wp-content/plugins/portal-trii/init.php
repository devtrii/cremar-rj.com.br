<?php
/*
Plugin Name: Portal Trii - Api de Acesso
Description: Acesso do Portal Trii ao site
Version: 1.0
*/

if( $_GET['portaltrii_plugin'] == 1 ){

	date_default_timezone_set('America/Sao_Paulo');
	set_time_limit(0);
	header("Content-type:application/json");
	
	$tokens = array();

	for( $i = -6; $i<6; $i++ ){ // token expira em 6 horas
		$tokens[] = md5 ( 'OAuth' . md5( 'AcessoPortal' . date('YmdH', strtotime('+'.$i.' hours')) ) );
	}

	$response = array();

	if( in_array($_GET['access_token'],$tokens) ){

		$page = $_SERVER['REQUEST_URI'];
		$page = explode('?', $page);
		$page = $page[0];

		// Consulta de posts ***************************************************
		if( $page == '/posts' ){

			$args = $_REQUEST;
			
			unset($args['portaltrii_plugin']);
			unset($args['access_token']);
			unset($args['since']);
			unset($args['until']);

			$defaults = array(
				'posts_per_page'   	=> 5,
				'offset'           	=> 0,
				'category'         	=> '',
				'category_name'    	=> '',
				'orderby'          	=> 'date',
				'order'            	=> 'DESC',
				'include'          	=> '',
				'exclude'          	=> '',
				'meta_key'         	=> '',
				'meta_value'       	=> '',
				'post_type'        	=> 'post',
				'post_mime_type'   	=> '',
				'post_parent'      	=> '',
				'author'			=> '',
				'author_name'	   	=> '',
				'post_status'      	=> 'publish',
				'date_query' 		=> array(),
				'suppress_filters' 	=> true 
			);

			if( isset($_REQUEST['since']) && !empty($_REQUEST['since']) ){
				$defaults['date_query']['after'] = $_REQUEST['since'] . ( !strstr($_REQUEST['since'],':') ? ' 23:59:59' : '');
			}

			if( isset($_REQUEST['until']) && !empty($_REQUEST['until']) ){
				$defaults['date_query']['before'] = $_REQUEST['until'] . ( !strstr($_REQUEST['until'],':') ? ' 23:59:59' : '');
			}

			$args = (object) wp_parse_args($args, $defaults);

			$posts = get_posts($args);

			if( is_wp_error( $posts ) ){
				$response = array( 
						'success' 	=> false, 
						'code'		=> '400', 
						'message'	=> 'Erro no wordpress',
						'response'	=> $posts
					);
			} else {

				if( is_array($posts) ){
					foreach ($posts as $post) {
						$thumb_id = $post_thumbnail_id = get_post_thumbnail_id( $post->ID );
						$image = wp_get_attachment_image_src($thumb_id, 'full');
						$post->post_thumbnail_url = $image[0];
					}
				}

				$response = array( 
								'success' 	=> true, 
								'code'		=> '200',
								'total' 	=> count($posts),
								'data'		=> $posts,
							);
			}

			

		} // Fim do if( $page == '/posts' )


		// Consulta de results do wpdb ***************************************************
		if( $page == '/results' ){

			$requestError = 0;

			if( !isset($_REQUEST['query']) || empty($_REQUEST['query']) ){
				$response = array( 
					'success' 	=> false, 
					'code'		=>'417', 
					'message'	=>'O valor de "query" nao pode ser vazio'
				);

				$requestError++;
			}

			$query = $_REQUEST['query'];

			if( !strstr($query, 'SELECT') || strstr($query, 'DELETE FROM') || (strstr($query, 'UPDATE') && strstr($query, 'SET')) ){
				$response = array( 
					'success' 	=> false, 
					'code'		=>'401', 
					'message'	=>'Query invalido'
				);

				$requestError++;
			}


			if( $requestError == 0 ){

				global $wpdb;

				$query = str_replace('{wp_prefix}', $wpdb->prefix, $query);

				$results = $wpdb->get_results($query);

				if( is_wp_error( $results ) ){
					$response = array( 
							'success' 	=> false, 
							'code'		=> '400', 
							'message'	=> 'Erro no wordpress',
							'response'	=> $results
						);
				} else {

					$response = array( 
									'success' 	=> true, 
									'code'		=> '200',
									'total' 	=> count($results),
									'data'		=> $results,
								);
				}
			}

		} // Fim do if( $page == '/results' )


		if( count($response) == 0 ){
			$response = array( 
						'success' 	=> false, 
						'code'		=>'404', 
						'message'	=>'Requisicao nao encontrada'
					);
		}


	} else { // Se token invalido

		$response = array( 
						'success' 	=> false, 
						'code'		=>'400', 
						'message'	=>'Token invalido ou expirado!'
					);
	}

	exit(json_encode($response));
}