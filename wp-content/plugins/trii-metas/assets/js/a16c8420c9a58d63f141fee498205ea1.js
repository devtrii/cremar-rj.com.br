
				if( el.is('a') && el.attr('href').startsWith('tel:') )
				{
					$( el ).on('click', function (argument) {
						_click('telephone', el);
					});
					continue;
				}
				if( el.is('a') && el.attr('href').startsWith('https://api.whatsapp.com/send') )
				{
					$( el ).on('click', function (argument) {
						_click('whatsapp', el);
					});
					continue;
				}
				if( el.is('a') && el.attr('href').startsWith('mailto:') )
				{
					$( el ).on('click', function (argument) {
						_click('email', el);
					});
					continue;
				}
