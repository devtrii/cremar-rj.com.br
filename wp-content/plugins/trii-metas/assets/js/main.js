(function($) {

	$( document ).ready(function(){
		$('.ctrl-toggle-a').on('change', function(){
			if( !this.checked ){
				$('.chat-config.a').slideUp();
			}else{
				$('.chat-config.a').slideDown();
			}
		});

		$('p.submit input').on('click', function(){
			if( $('.ctrl-toggle-a').attr('checked') && $('.chat-config.a input').val() === '' ){

				$('.row.custom details').attr('open', '');
				
				alert('Preencha o seletor jQuery!');
				
				return false;
			}
		});
	});

}(jQuery));