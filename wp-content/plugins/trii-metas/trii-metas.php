<?php
/*
Plugin Name: Trii - Metas
Description: Configurar metas do analytics customizadas
Version: 1.2
Author: Pedro Augusto
Author URI: https://pedrooaugusto.github.io/

RELEASE NOTES:
  • 1.0 - 21/06/2018 - Versão Inicial
  • 1.1 - 08/03/2019 - Remoção total do uso de Jquery para melhor combatibilidade com o page speed insights
  • 1.2 - 14/08/2019 - Resolvendo problema de compatibilidade com o novo plugin de whatsapp
*/

define( 'TMTS__VERSION', '1.2' );
define( 'TMTS__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'TMTS__PLUGIN_URL', plugin_dir_url( __FILE__ ) );

require_once TMTS__PLUGIN_DIR . 'inc/load.php';