<?php 
	$options = TMTS_Admin::tmts_get_data();
	if( isset($_GET['debugmetas']) ){
		echo '<pre>' . print_r($_POST, true) . '</pre>';
		echo '<pre>' . print_r($options, true) . '</pre>';
	}
?>



<div class="container">
	<div class="row">
		<div class="full header">
			<h1>Trii Metas</h1>
			<h4>
				<i>— Um Plugin para adicionar outras taxas de conversão 
				além da convencional visita e contato sucesso!</i>
			</h4>
		</div>
	</div>
	<hr>
	<br/>

	<form method="POST" name="atualizar_metas">

		<div class = "row time">
			<div class="full">
				<div class="meta telefone">
					<div class="meta__info">
						<div class="meta__info__name">
							<div>Intervalo</div>
						</div>
						<div class="meta__info__description">
							<div>
								Informe o intervalo em minutos até que outro click
								possa ser computado pelo<br>analytics. (1min = 1min, 0.5min = 30sec, 1440min = 1dia)
							</div>
						</div>
					</div>
					<div class="meta__value input">
						<input 
							type = "number" 
							name = "tmts[time]"
							min="0.1"
							step="0.1"
							value = "<?php echo $options["time"] ? $options["time"] : "1440" ?>">
					</div>
				</div>
			</div>
		</div>

		<hr/>
		
		<div class="row metas">
			<div class="half">
				<div class="meta whatsapp">
					<div class="meta__info">
						<div class="meta__info__name">
							<div>Whatsapp</div>
						</div>
						<div class="meta__info__description">
							<div>
								Ao clicar em um número de Whatsapp no site, 
								é disparado um evento para o analytics.
							</div>
						</div>
					</div>
					<div class="meta__value checkbox">
						<label>
							<input 
								type = "checkbox" 
								name = "tmts[whatsapp]" 
								<?php echo $options["whatsapp"] ? "checked" : "" ?>>
							<span class="checkbox__feedback"></span>
						</label>
					</div>
				</div>
			</div>

			<div class="half">
				<div class="meta telefone">
					<div class="meta__info">
						<div class="meta__info__name">
							<div>Telefone</div>
						</div>
						<div class="meta__info__description">
							<div>
								Ao clicar em um número de Telefone no site, 
								é disparado um evento para o analytics.
							</div>
						</div>
					</div>
					<div class="meta__value checkbox">
						<label>
							<input 
								type = "checkbox" 
								name = "tmts[telefone]"
								<?php echo $options["telefone"] ? "checked" : "" ?>>
							<span class="checkbox__feedback"></span>
						</label>
					</div>
				</div>
			</div>

			<div class="half">
				<div class="meta email">
					<div class="meta__info">
						<div class="meta__info__name">
							<div>Email</div>
						</div>
						<div class="meta__info__description">
							<div>
								Ao clicar em um endereço de e-mail no site, 
								é disparado um evento para o analytics.
							</div>
						</div>
					</div>
					<div class="meta__value checkbox">
						<label>
							<input 
								type = "checkbox" 
								name = "tmts[email]"
								<?php echo $options["email"] ? "checked" : "" ?>>
							<span class="checkbox__feedback"></span>
						</label>
					</div>
				</div>
			</div>

		</div>
		
		<div class="row custom">
			<div class="full">
				<details  <?php echo $options["chat"] ? "open" : "" ?> >
					<summary class = "link-txt">
						<span>Avançado</span>
					</summary>
					<div>
						<h4>Developers only!</h4>

						<div class="half">
							<div class="meta chat">
								<div class="meta__info">
									<div class="meta__info__name">
										<div>Chat</div>
									</div>
									<div class="meta__info__description">
										<div>
											Ao clicar em uma aba de Chat no site, 
											é disparado um evento para o analytics.
										</div>
									</div>
								</div>
								<div class="meta__value checkbox">
									<label>
										<input 
											type = "checkbox" 
											name = "tmts[chat]"
											class = "ctrl-toggle-a"
											<?php echo $options["chat"] ? "checked" : "" ?>>
										<span class="checkbox__feedback"></span>
									</label>
								</div>
								<div class = "chat-config a" style="display: <?php echo $options["chat"] ? "block" : "none"; ?>;">
									<hr/>
									<span>
										JQuery Selector: 
										<input 
											type = "text"
											name = "tmts[chat_selector]"
											placeholder = "iframe.chatZopim"
											<?php echo $options["chat_selector"] != "" ? "value='".$options["chat_selector"]."'" : ""; ?>
											/>
									</span>
								</div>
							</div>
						</div>

					</div>
				</details>
			</div>
		</div>

		<hr/>

		<div class="row btn">
			<div class="full">
				<?php submit_button('Salvar', 'button-primary', 'tmts_submit_data'); ?>
			</div>
		</div>
	</form>
</div>