<?php
	$options = TSTP_Admin::tstp_get_data();
?>

<script type="text/javascript">

	<?php 
		foreach ($options['html'] as $key => $value) {

			$method = strstr( $key, 'inicio' ) ? 'insertBefore' : 'appendChild';

			$location = str_replace('inicio_', '', $key);
			$location = str_replace('fim_', '', $location);

			echo "var script = document.createElement( 'script' );\n";
			echo "script.type = 'text/javascript';\n";
			echo "script.src = '".TSTP__PLUGIN_URL."assets/js/".$key.".js';\n";
			if($location == "footer"){
				echo "var el = document.getElementById('".$location."');\n";
			} else {
				echo "var el = document.getElementsByTagName('".$location."')[0];\n";
			}
			echo "el.".$method."( script".($method == 'insertBefore' ? ', el.firstChild':'').");\n";

			echo "var content = document.createElement( 'span' );\n";
			echo "content.innerHTML = '".preg_replace( "/\r|\n/", "", $value)."';\n";
			echo "el.".$method."( content".($method == 'insertBefore' ? ', el.firstChild':'').");\n";	
		}

	?>

</script>