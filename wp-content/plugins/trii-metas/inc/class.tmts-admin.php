<?php
	
	class TMTS_Admin 
	{

		public static function OPTS(){
			
			return array(
					"telefone" => "a[href^='tel:']", 
					"whatsapp" => "a[href^='https://api.whatsapp.com/send']", 
					"email"    => "a[href^='mailto:']", 
					"chat"	   => self::tmts_get_data()['chat_selector']
				);
		}

		public function __construct()
		{
			add_action("admin_menu", array($this, "register_menu"));
			add_action("admin_init", array($this, "tmts_save_data"));

			add_action( 'wp_footer', array( $this, 'tmts_load_codes_page' ) );
		}

		public function register_menu()
		{
			add_options_page("Trii Metas", "Trii Metas", "manage_options", 
				"tmts-settings", array($this, "tmts_menu_view"));
		}

		public function tmts_menu_view()
		{
			//CSS and HTML and JS for the plugin settings page

			wp_enqueue_style( "tmts_css", TMTS__PLUGIN_URL . "assets/css/style.css");

			wp_enqueue_script( "tmts_js", TMTS__PLUGIN_URL . "assets/js/main.js", array("jquery") );

			require_once TMTS__PLUGIN_DIR . "view/settings.php";
		}

		public function tmts_save_data()
		{
			/*
				Para Cada Check Marcado Pegar um .JS correspondente.
				No final Juntar todos os .JS que foram marcado em um só.
			*/

			// echo "<pre>" . print_r($_GET, true) . "</pre>";
			// echo "<pre>" . print_r($_POST, true) . "</pre>";

			$is_submit = isset($_POST["tmts_submit_data"]);
			
			if ($is_submit && $_GET["page"] == "tmts-settings")
			{

				$form_data = isset($_POST["tmts"]) ? $_POST["tmts"] : array();

				$options = array(
					"telefone" => array_key_exists("telefone", $form_data),
					"whatsapp" => array_key_exists("whatsapp", $form_data),
					"email" => array_key_exists("email", $form_data),
					"chat" => array_key_exists("chat", $form_data),
					"chat_selector" => array_key_exists("chat_selector", $form_data) ? $form_data['chat_selector'] : "",
					"time" => array_key_exists("time", $form_data) ? $form_data['time'] : "1440",
				);
				
				$files = array("telefone", "whatsapp", "email", "chat");

				$main_file_content = "";

				foreach ($files as $param) 
				{
					$exist_param = array_key_exists($param, $form_data);

					if($exist_param && @file_exists(TMTS__PLUGIN_DIR."assets/js/".$param."_analytics.js"))
					{
						$main_file_content = $main_file_content . file_get_contents(TMTS__PLUGIN_DIR."assets/js/".$param."_analytics.js");
					}
				}

				file_put_contents(TMTS__PLUGIN_DIR."assets/js/a16c8420c9a58d63f141fee498205ea1.js",  
					$main_file_content);

				update_option("tmts_options", $options);
			}
		}

		public static function tmts_get_data()
		{
			return get_option('tmts_options', false);
		}

		public function tmts_load_codes_page()
		{
			if( !is_admin() )
			{
				wp_enqueue_script( 'jquery' );
				require_once TMTS__PLUGIN_DIR . 'inc/load_codes_page.php';
			}
		}
	}

	new TMTS_Admin();
?>