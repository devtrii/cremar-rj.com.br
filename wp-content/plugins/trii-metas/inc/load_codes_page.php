<?php 
	$available_options 		= TMTS_Admin::OPTS();
	$options 				= TMTS_Admin::tmts_get_data();

	foreach ($options as $key => $value)

		if( $value && array_key_exists($key, $available_options) )

			$selector .= $available_options[ $key ] . ', ';

	$selector = substr($selector, 0, count($selector) - 3);

	if( isset($_GET['debugmetas']) ){

		echo "<pre>" . print_r(TMTS_Admin::tmts_get_data(), true) . "</pre>";

		echo "<pre>" . print_r(TMTS_Admin::OPTS(), true) . "</pre>";

		echo "<pre>" . $selector . "</pre>";
	}

?>

<script type="text/javascript">
	/*  Trii Metas  */
	

		var EXPIRATION_TIME = 1000 * 60 * <?php echo $options['time'] ? $options['time'] : 1440 ; ?>;
		
		function _extract(what, link)
		{

			function whatsapp(s)
			{
			
				//Link = https://api.whatsapp.com/send?1=PT_br&phone=5521100200500

				if(s.indexOf('?') == -1)
					return null;

				var param_name = 'phone=';

				var urlencoded = s.split('?')[1];/* link.substring(link.indexOf('?')) */

				var params     = urlencoded.split('&');

				var numero     = null;

				for (var i = 0; i < params.length; i++) {
					if(params[i].startsWith(param_name)){
						numero = params[i].substring(param_name.length);
						break;
					}
				}

				return numero;
			}

			function generic(s)
			{
				return s.split(':')[1];
			}

			if( what === 'whatsapp' )
				return whatsapp(link.getAttribute('href'));
			else
				return generic(link.getAttribute('href'));

			return undefined;
		}

		function _click(type, link){

			if(type === 'whatsapp' && checkExpirationDate(type)){
				ga('send', 'event', 'whatsapp', 'enviar-mensagem', _extract('whatsapp', link));
			}
			else if(type === 'telephone' && checkExpirationDate(type)){
				ga('send', 'event', 'telefone', 'clique-telefone', _extract('telephone', link));
			}
			else if(type === 'email' && checkExpirationDate(type)){
				ga('send', 'event', 'email', 'clique-email', _extract('email', link));
			}
			else if(type === 'chat' && checkExpirationDate(type)){
				ga('send', 'event', 'chat', 'clique-chat', undefined);	
			}else{
				// console.log('not yet.');
			}
		}

		function checkExpirationDate(type){
			
			function save(obj){
				
				obj[type] = new Date();
						
				var s = JSON.stringify(obj);
						
				localStorage.setItem("trii-metas-"+location.hostname, s);

				return true;
			}

			var k = localStorage.getItem("trii-metas-"+location.hostname);

			if(k)
			{
				var o = JSON.parse(k);

				if(o[type] !== "null")
				{
					var d = new Date(), 
						d0 = new Date(o[type]);

					if(d - d0 > EXPIRATION_TIME)
						return save(o);
					else
						return false;
				}
				else
					return save(o);
			}
			else
			{
				var o = {
					whatsapp: 'null',
					email: 'null',
					telephone: 'null',
					chat: 'null'
				};

				return save(o);
			}
		}

		function ready(e){

			try{
	
				var LINKS = document.querySelectorAll("A");
				
				if( LINKS && LINKS.length > 0 ){

					for(const el of LINKS){
						if(el.href == '') continue;

						if( el.tagName == "A" && el.getAttribute('href').startsWith('tel:') )
						{
							el.addEventListener('click', function() {
								_click('telephone', el);
							});
							continue;
						}
						if( el.tagName == "A" && el.getAttribute('href').startsWith('https://api.whatsapp.com/send') ||  el.classList.contains('qlwapp-toggle'))
						{
							
							el.addEventListener('click', function() {
								_click('whatsapp', el);
							});
							continue;
						}
						if( el.tagName == "A" && el.getAttribute('href').startsWith('mailto:') )
						{
							el.addEventListener('click', function() {
								_click('email', el);
							});
							continue;
						}
					}
				}

			}catch(err){
				console.log(err);
			}

		}

		// Iniciar
		setTimeout(ready, 300);

	
</script>