(function($, window) {
	$(document).on({
		'ready': function(e) {

			// TinyMCE
			/*tinymce.init({
				selector: "#modalText",
				setup : function(ed) {
					ed.on("change", function(e){
						$('#modalText').html(tinymce.activeEditor.getContent());
					});
					ed.on("keyup", function(){
						$('#modalText').html(tinymce.activeEditor.getContent());
					});
			   }
			});*/

			// Active
			showOptions();

			$( document ).on('click', 'input[name=active]', function(e) {
				showOptions();
			});

			function showOptions() {
				var $check = $('input[name=active]'),
					$options = $('.showOptions');

				if ( $check.attr('checked') == 'checked' ) {
					$options.fadeIn(300);
				} else {
					$options.fadeOut(300);
				}
			}

			// Tabs
			$( document ).on('click', '.nav-tab', function(e) {
				e.preventDefault();

				var link = $(this).attr('href'),
					tab = link.substr( link.search('&tab=') + 5 ),
					$nav = $('.nav-tab-wrapper');

				$('.form-table tbody').removeClass('active').fadeOut(0);
				$('.form-table tbody[tab="'+ tab +'"]').addClass('active').fadeIn(300)

				// change tab
				$nav.find('a').removeClass('nav-tab-active');
				$(this).addClass('nav-tab-active');

				// change url
				history.pushState(null, null, 'options-general.php?page=trii-modal&tab=' + tab);
			});

			// PREVIEW
			$( document ).on('click', '.preview', function(e) {
				e.preventDefault();

				var $preview = $('#preview'),
					$iframe = $preview.find('.iframe'),
					url = $iframe.attr('data-src');

				// TinyMCE
				$('#modalText').val(tinymce.activeEditor.getContent());

				// ajax
				$.ajax({
					url: plugin_ajax,
					type: 'POST',
					dataType: 'JSON',
					data: {
						action: 'savePreview',
						form: $('.modal-form').serializeArray()
					},
					beforeSend: function( xhr ) {
						$('.loader').text('Carregando...');
					},
					success: function( success ) {
						$('.loader').text('');
						$iframe.append('<iframe src="'+ url +'" width="100%"></iframe>');
						$preview.fadeIn(600);
					},
					error: function ( error ) {
						console.log( error );
					}
				});
			});

			function closePreview() {
				var $preview = $('#preview'),
					$iframe = $preview.find('.iframe');

				$preview.fadeOut(500);
				$iframe.html('');
			}

			$( document ).on('click', '#preview .close', function(e) {
				closePreview();
			});

			$( document ).keyup(function(e) {
				if (e.keyCode == 27) { closePreview(); }
			});

			// --------------------------

			if ( $('#customContent').val().length > 0 ) {
				$('.default').hide();
			}

			$( document ).on('keyup change', '#customContent', function(e) {
				if ( $(this).val().length > 0 ) {
					$('.default').hide();
				} else {
					$('.default').show();
				}
			});

			jQuery( '.btn-select-bg' ).on( 'click', function( event ){
				event.preventDefault();

				// Uploading files
				var file_frame;
				var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
				var set_to_post_id = 1; // Set this
				
				if( $( '.rowIMG' ).length > 11 ){
					bootbox.alert( 'Você só pode colocar até 12 imagens.' );
					return false;
				}
				 // If the media frame already exists, reopen it.
					if ( file_frame != undefined && file_frame ) {
					  file_frame.open();
					  return;
					}
				 
					// Create the media frame.
					file_frame = wp.media.frames.file_frame = wp.media({
					  title: jQuery( this ).data( 'uploader_title' ),
					  button: {
						text: jQuery( this ).data( 'uploader_button_text' ),
					  },
					  multiple: false  // Set to true to allow multiple files to be selected
					});
				 
					// When an image is selected, run a callback.
					file_frame.on( 'select', function() {
					  // We set multiple to false so only get one image from the uploader
					  attachment = file_frame.state().get('selection').first().toJSON();
						//$('.foto_loader').show();
															 
						if( attachment.subtype != 'jpeg' && attachment.subtype != 'jpg' && attachment.subtype != 'png' && attachment.subtype != 'gif' ){
							alert( 'Extensão de arquivo não permitida. \n \n Extensões Permitidas: JPEG, JPG, PNG, GIF' );
						}else{

							$( '#bgImage' ).val( attachment.url );
							$( '#bgImage_width' ).val( attachment.width );
							$( '#bgImage_height' ).val( attachment.height );

							//$( '.logowp' ).attr( 'src', attachment.url );
							$( '.btn-select-bg' ).text( 'Imagem Carregada' );
							$('.removeBg').removeClass('hidden');

						}
				 
					});
				 
					file_frame.open();
			});

			jQuery( '.btn-select-image' ).on( 'click', function( event ){
				event.preventDefault();

				// Uploading files
				var file_frame;
				var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
				var set_to_post_id = 1; // Set this
				
				if( $( '.rowIMG' ).length > 11 ){
					bootbox.alert( 'Você só pode colocar até 12 imagens.' );
					return false;
				}
				 // If the media frame already exists, reopen it.
					if ( file_frame != undefined && file_frame ) {
					  file_frame.open();
					  return;
					}
				 
					// Create the media frame.
					file_frame = wp.media.frames.file_frame = wp.media({
					  title: jQuery( this ).data( 'uploader_title' ),
					  button: {
						text: jQuery( this ).data( 'uploader_button_text' ),
					  },
					  multiple: false  // Set to true to allow multiple files to be selected
					});
				 
					// When an image is selected, run a callback.
					file_frame.on( 'select', function() {
					  // We set multiple to false so only get one image from the uploader
					  attachment = file_frame.state().get('selection').first().toJSON();
						//$('.foto_loader').show();
															 
						if( attachment.subtype != 'jpeg' && attachment.subtype != 'jpg' && attachment.subtype != 'png' && attachment.subtype != 'gif' ){
							alert( 'Extensão de arquivo não permitida. \n \n Extensões Permitidas: JPEG, JPG, PNG, GIF' );
						}else{

							$( '#image' ).val( attachment.url );
							$( '#image_width' ).val( attachment.width );
							$( '#image_height' ).val( attachment.height );

							//$( '.logowp' ).attr( 'src', attachment.url );
							$( '.btn-select-image' ).text( 'Imagem Carregada' );
							$('.removeImg').removeClass('hidden');

						}
				 
					});
				 
					file_frame.open();
			});

			jQuery( '.btn-select-closeBtn' ).on( 'click', function( event ){
				event.preventDefault();

				// Uploading files
				var file_frame;
				var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
				var set_to_post_id = 1; // Set this
				
				if( $( '.rowIMG' ).length > 11 ){
					bootbox.alert( 'Você só pode colocar até 12 imagens.' );
					return false;
				}
				 // If the media frame already exists, reopen it.
					if ( file_frame != undefined && file_frame ) {
					  file_frame.open();
					  return;
					}
				 
					// Create the media frame.
					file_frame = wp.media.frames.file_frame = wp.media({
					  title: jQuery( this ).data( 'uploader_title' ),
					  button: {
						text: jQuery( this ).data( 'uploader_button_text' ),
					  },
					  multiple: false  // Set to true to allow multiple files to be selected
					});
				 
					// When an image is selected, run a callback.
					file_frame.on( 'select', function() {
					  // We set multiple to false so only get one image from the uploader
					  attachment = file_frame.state().get('selection').first().toJSON();
						//$('.foto_loader').show();
															 
						if( attachment.subtype != 'jpeg' && attachment.subtype != 'jpg' && attachment.subtype != 'png' ){
							alert( 'Extensão de arquivo não permitida. \n \n Extensões Permitidas: JPEG, JPG, PNG' );
						}else{
							$( '#closeBtn' ).val( attachment.url );
							$( '#closeBtn_width' ).val( attachment.width );
							$( '#closeBtn_height' ).val( attachment.height );

							$( '.btn-select-closeBtn' ).text( 'Imagem Carregada' );
							$('.removeClose').removeClass('hidden');
						}
				 
					});
				 
					file_frame.open();
			});

			// Restore the main ID when the add media button is pressed
			jQuery('a.add_media').on('click', function() {
				wp.media.model.settings.post.id = wp_media_post_id;
			});

			var $removes = $('.removeImg');

			$.each($removes, function(e) {
				if ( $(this).siblings('.url').val().length == 0 ) {
					$(this).addClass('hidden');
				}
			});

			$( document ).on('click', '.removeImg', function(e) {
				e.preventDefault();

				$(this).siblings('input').val('');
				$(this).siblings('button[type=button]').text('Selecionar imagem');
				$(this).addClass('hidden');
			});

		}
	});
})(jQuery);
