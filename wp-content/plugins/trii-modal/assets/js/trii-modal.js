jQuery( document ).ready(function($) {
	
	// vars
	var sessao = window.name,
		$fadeBg = $('.triiModal-fade'),
		customContent = $fadeBg.attr('custom-content'),
		delay = $('.triiModal-fade').attr('delay') + '000',
		mobile = $('.triiModal-fade').attr('mobile'),
		limit = 767;	

	if ( $(window).width() > 767 ) {
		if ( sessao != 'modalOK' ) {
			setTimeout( function(){
				openModal();
			}, delay);
		}
	} else {
		if ( mobile == 'yes' ) {
			if ( sessao != 'modalOK' ) {
				setTimeout( function(){
					openModal();
				}, delay);
			}
		}
	}

	function openModal() {
		var $modal = $('.triiModal-fade, .triiModal-content');
		$modal.fadeIn(400);
	}

	function closeModal() {
		var $modal = $('.triiModal-fade, .triiModal-content');
		window.name = 'modalOK';
		$modal.fadeOut(400);
	}

	// conteudo personalizado
	if ( customContent.length > 0 ) {
		var $content = $('.triiModal-content');
		$content.addClass('noStyle');
		$content.find('.btn-close').remove();
		$content.find('.title-modal').remove();
		$(customContent).addClass('hidden');
		$content.find('.text-modal').html('').append( $(customContent).get(0).outerHTML );
		$content.find(customContent).removeClass('hidden');
	}

	// close modal
	$( document ).on('click', '.triiModal-content .btn-close', function(e) {
		e.preventDefault();
		closeModal();
	});

	$( document ).keyup(function(e) {
		if (e.keyCode == 27) { closeModal(); }
	});

	$( document ).on('click', '.triiModal-fade', function(e) {
		closeModal();
	});

});