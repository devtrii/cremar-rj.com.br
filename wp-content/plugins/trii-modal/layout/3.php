<?php /* LAYOUT 3 */ ?>

	<div class="layout layout-3">
		<span class="title-modal" style="<?php echo $titleColor; ?>"><?php echo $options->title; ?></span>
		<span class="text-modal"><?php echo stripslashes($options->modalText); ?></span>
        <div class="block">
            <img src="<?php echo $options->image['url']; ?>" alt="<?php echo $alt; ?>" class="image-modal">
        </div>
	</div>