<?php /* LAYOUT 4 */ ?>

	<div class="layout layout-4">
		<span class="title-modal" style="<?php echo $titleColor; ?>"><?php echo $options->title; ?></span>
		<div class="block">
			<div class="left">
				<img src="<?php echo $options->image['url']; ?>" alt="<?php echo $alt; ?>" class="image-modal">
			</div>
			<div class="right">
				<span class="text-modal"><?php echo stripslashes($options->modalText); ?></span>
			</div>
		</div>
	</div>