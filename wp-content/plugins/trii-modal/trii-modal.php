<?php
/*
Plugin Name: Trii - Modal
Plugin URI: 
Description: Adicione modais customizáveis para avisos.
Author: Gabriel Araujo
Author URI: http://gabrielquefez.com
Version: 2.1

RELEASE NOTES:
  • 1.0 - 23/01/2017 - Versão Inicial
  • 1.1 - 08/02/2017 - incluir js apenas na página do plugin
  • 2.0 - 07/06/2017 - Reestruturação no layout e adição de modelos
  • 2.1 - 15/03/2018 - Permitir GIF
*/

define( 'TRIIMODAL_DIR', plugin_dir_path( __FILE__ ) );
define( 'TRIIMODAL_URL', plugin_dir_url( __FILE__ ) );

require_once TRIIMODAL_DIR . 'inc/incs.php';
