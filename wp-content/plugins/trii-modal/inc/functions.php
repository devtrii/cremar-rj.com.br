<?php

	$hasOption = get_option( 'trii_modal_settings' );
	if( ! $hasOption ) {
		$optionOBJ = array(
				'active' 		=> false,
				'mobile' 		=> false,
				'delay' 		=> 3,
				'layout'		=> 1,
				'width'			=> 835,
				'bgImage' 		=> array(
					'url' 	 => '',
					'width'  => 835,
					'height' => 300
				),
				'image' 		=> array(
					'url' 	 => '',
					'width'  => 150,
					'height' => 150
				),
				'bgColor' 		=> 'FFFFFF',
				'closeBtn' 		=> array(
					'url' 	 => TRIIMODAL_URL . 'assets/image/closeBtn.png',
					'width'  => 27,
					'height' => 27
				),
				'title'			=> '',
				'titleColor'	=> '000000',
				'modalText'		=> '',
				'customContent'	=> ''
			);

		update_option( 'trii_modal_settings', $optionOBJ );
	}
	unset( $hasOption );


	function trii_modal_addPage() {
		add_options_page( 'Trii Modal', 'Trii Modal', 'manage_options', 'trii-modal', 'trii_modal_viewPage' );
	}
	add_action('admin_menu', 'trii_modal_addPage');


	function trii_modal_viewPage() {
		require_once TRIIMODAL_DIR . 'view/options.php';
	}

	function trii_modal_assets() {
		if ( $_GET['page'] <> 'trii-modal' ) return;
		wp_enqueue_style( 'jquery-ui' );
		wp_enqueue_script( 'modal_script', TRIIMODAL_URL . 'assets/js/script.js', array( 'jquery' ), true );
		wp_enqueue_script( 'modal_jscolor', TRIIMODAL_URL . 'assets/js/jscolor.min.js', array( 'jquery' ), true );
		wp_enqueue_style( 'modal_style', TRIIMODAL_URL . 'assets/css/modal_style.css' );
	}
	add_action( 'admin_head', 'trii_modal_assets' );


	function trii_modal_frontEnd() {
		$options = get_option( 'trii_modal_settings' );

		if ( $options['active'] || isset( $_GET['debugModal'] ) ) {
			wp_enqueue_style( 'trii-modal', TRIIMODAL_URL . 'assets/css/trii-modal.min.css', array(), false );
			wp_enqueue_script( 'trii-modal', TRIIMODAL_URL . 'assets/js/trii-modal.min.js', array('jquery'), false, true );
		}
	}
	add_action( 'wp_enqueue_scripts', 'trii_modal_frontEnd' );


	function trii_modal_content() {
		$options = get_option( 'trii_modal_settings' );

		if ( isset($_GET['debugModal']) ) {
			$options = get_option('trii_modal_preview');
		}

		if( $options ) $options = ( Object ) $options;

		if ( $options->active || isset( $_GET['debugModal'] ) ) {
			$mobile = ( $options->mobile ) ? 'yes' : 'no';
			$bgColor = ( $options->bgColor ) ? 'background-color: #'. $options->bgColor .';' : null;
			$bgImg = ( $options->bgImage ) ? 'background-image: url( \''. $options->bgImage['url'] .'\' );' : null;
			$btnClose = ( $options->closeBtn['url'] ) ? $options->closeBtn['url'] : TRIIMODAL_URL . 'assets/image/closeBtn.png';
			$titleColor = ( $options->titleColor ) ? 'color: #' . $options->titleColor . ';' : null;
			$customContentClass = ( $options->customContent ) ? $options->customContent : false;
			$maxWidth = ( $options->width ) ? ' max-width: ' . $options->width . 'px;' : ' max-width: 835px;';

			// remove delay on preview
			if ( isset( $_GET['debugModal'] ) )
				$options->delay = 0;

			// alt
			global $wpdb;
			$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $options->image['url'] ));
			$alt = get_post_meta( $attachment[0] , '_wp_attachment_image_alt', true );
	?>
		<div class="triiModal-fade" delay="<?php echo $options->delay; ?>" mobile="<?php echo $mobile; ?>" custom-content="<?php echo $customContentClass; ?>"></div>
		<div class="triiModal-content" layout="<?php echo $options->layout; ?>" style="<?php echo $bgColor . $bgImg . $maxWidth; ?>">
			<img src="<?php echo $btnClose; ?>" alt="Fechar" class="btn-close">
			<?php
				// Layout
				require_once TRIIMODAL_DIR . 'layout/'. $options->layout .'.php';
			?>
		</div>
	<?php
		}
	}
	add_action( 'wp_footer', 'trii_modal_content' );


	add_action('wp_ajax_savePreview', 'savePreview');
	function savePreview() {
		$rs = true;

		$options = array();
		$atts = ( Object ) $_POST['form'];

		foreach ($atts as $key => $option) {
			if ( strpos($option['name'], '[') ) {
				$parent = substr($option['name'], 0, strpos($option['name'], '['));
				$child = substr( substr($option['name'], strpos($option['name'], '[')), 1, -1 );

				$options[$parent][$child] = $option['value'];
			} else {
				$options[$option['name']] = $option['value'];
			}
		}

		update_option( 'trii_modal_preview', $options );
	
		exit( json_encode( $rs ) );
	}