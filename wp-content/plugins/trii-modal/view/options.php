<?php
	wp_enqueue_script('media-upload');
	wp_enqueue_media();
	
	if( isset( $_POST['trii_modal_saveSettings'] ) ){

		$option = array();

		foreach ( $_POST as $key => $post ) {
			if( $key != 'trii_modal_saveSettings' ){
				$option[$key] = $post;
			}
		}

		update_option( 'trii_modal_settings', $option );

		$sucesso = "Modal <b>atualizado!</b>";
	}

	$trii_modal_settings = get_option( 'trii_modal_settings' );
	if( $trii_modal_settings ) $trii_modal_settings = ( Object ) $trii_modal_settings;

	if ( isset($_GET['debug']) ) {
		echo '<pre>' . print_r( $trii_modal_settings , true ) . '</pre>';
	}

	if( isset( $_GET[ 'tab' ] ) ) {
		$active_tab = $_GET[ 'tab' ];
	} else {
		// default tab
		$active_tab = 'config';
	}

?>

<script>
	window.plugin_ajax = '<?php echo admin_url("admin-ajax.php"); ?>';
</script>

<div class="wrap">
	<form method="POST" class="modal-form">
		<div class="title">
			<h1>Trii Modal</h1>

			<div class="buttons">
				<span class="loader"></span>
				<a href="" class="button preview">Pré-visualizar</a>
				<?php submit_button( 'Atualizar', 'primary', 'trii_modal_saveSettings', true ); ?>
			</div>
		</div>
		<?php settings_errors(); ?>

		<div class="half-sucess">
			<?php
				if( isset( $sucesso ) ){
					echo "<div id='message' class='updated notice notice-success is-dismissible'><p>{$sucesso}</p><button type='button' class='notice-dismiss'><span class='screen-reader-text'>Dispensar este aviso.</span></button></div>";
				}
			?>
		</div>
	
		<div class="habilitarModal">
			<span>Habilitar Modal</span>
			<input type="checkbox" name="active" id="active" value="1" class="switch" <?php echo $trii_modal_settings->active || !is_object( $trii_modal_settings ) ? 'checked' : ''; ?> /> <label for="active"> </label>
		</div>

		<h2 class="nav-tab-wrapper showOptions">
			<a href="?page=trii-modal&tab=config" class="nav-tab <?php echo ( $active_tab == 'config' ) ? 'nav-tab-active' : '' ?>">Configurações</a>
			<a href="?page=trii-modal&tab=layout" class="nav-tab <?php echo ( $active_tab == 'layout' ) ? 'nav-tab-active' : '' ?>">Aparência</a>
		</h2>

		<table class="form-table showOptions">
			<!-- CONFIG -->
			<tbody tab="config" class="<?php echo ( $active_tab == 'config' ) ? 'active' : 'hidden' ?>">
				<tr>
					<td>Mostrar Modal no Mobile</td>
					<td>
						<input type="checkbox" name="mobile" id="mobile" value="1" class="switch" <?php echo $trii_modal_settings->mobile || !is_object( $trii_modal_settings ) ? 'checked' : ''; ?> /> <label for="mobile"> </label>
					</td>
				</tr>
				<tr>
					<td>Tempo de exibição</td>
					<td>
						<input type="number" name="delay" id="delay" value="<?php echo $trii_modal_settings->delay; ?>" <?php echo $trii_modal_settings->delay || !is_object( $trii_modal_settings ) ? 'checked' : ''; ?> /> <label for="delay"> segundo(s)</label><br>
						<i>Quantos segundos depois do site carregado o aviso será exibido</i>
					</td>
				</tr>
				<tr>
					<td class="title-part"><h2>Opções de Desenvolvedor</h2></td>
				</tr>
				<tr>
					<td>Modal Personalizado</td>
					<td>
						<input type="text" name="customContent" id="customContent" value="<?php echo $trii_modal_settings->customContent ? $trii_modal_settings->customContent : false; ?>"> 
						<i>Nome da classe para o modal personalizado. (Ex: .conteudoModal)</i>
					</td>
				</tr>
			</tbody>

			<!-- LAYOUT -->
			<tbody tab="layout" class="<?php echo ( $active_tab == 'layout' ) ? 'active' : 'hidden'?>">
				<tr class="default">
					<td>Modelo</td>
					<td>
						<ul class="layouts">
							<?php
								$count = 0;
								$dir = TRIIMODAL_DIR . 'layout';
								if ($handle = opendir($dir)) {
									while (($file = readdir($handle)) !== false){
										if (!in_array($file, array('.', '..')) && !is_dir($dir.$file)) 
											$count++;
									}
								}

								$trii_modal_settings->layout = ( $trii_modal_settings->layout ) ? $trii_modal_settings->layout : 1;

								$files = glob( $dir . '/*.php', GLOB_BRACE);
								foreach ($files as $file) {
									$file = end(explode('/', $file));
									$format = substr($file, -3);
									if ( $format == 'php' ) {
										$filename = substr( $file, 0, -4 );
										$checked = ( $trii_modal_settings->layout == $filename ) ? 'checked' : '';
							?>
							<li>
								<input type="radio" name="layout" id="layout<?php echo $filename; ?>" value="<?php echo $filename; ?>" <?php echo $checked; ?> />
								<label for="layout<?php echo $filename; ?>" style="background-image: url( <?php echo TRIIMODAL_URL . 'layout' ?>/<?php echo $filename; ?>.png );">Layout <?php echo $filename; ?></label>
							</li>
							<?php
									}
								}
							?>
						</ul>
					</td>
				</tr>
				<tr class="default">
					<td>Largura</td>
					<td>
						<input type="number" name="width" id="width" min="300" max="1200" value="<?php echo $trii_modal_settings->width; ?>" <?php echo $trii_modal_settings->width || !is_object( $trii_modal_settings ) ? 'checked' : ''; ?> />px
						<i>Largura total do modal.</i>
					</td>
				</tr>
				<tr class="default">
					<td>Título</td>
					<td>
						<input type="text" name="title" id="title" value="<?php echo $trii_modal_settings->title ? $trii_modal_settings->title : '' ?>" />
						<input class="jscolor" name="titleColor" id="titleColor" value="<?php echo $trii_modal_settings->titleColor ? $trii_modal_settings->titleColor : '000000'; ?>">
					</td>
				</tr>
				<tr class="default">
					<td>Background</td>
					<td>
						<input class="jscolor" name="bgColor" id="bgColor" value="<?php echo $trii_modal_settings->bgColor ? $trii_modal_settings->bgColor : 'FFFFFF'; ?>">
						<button type="button" class="btn-select-bg"><?php echo ( $trii_modal_settings->bgImage['url'] != '' ) ? 'Imagem Carregada' : 'Selecionar Imagem'; ?></button>
						<input type="hidden" name="bgImage[url]" id="bgImage" class="url" value="<?php echo $trii_modal_settings->bgImage['url'] ? $trii_modal_settings->bgImage['url'] : '' ?>" />
						<input type="hidden" name="bgImage[width]" id="bgImage_width" value="<?php echo $trii_modal_settings->bgImage['width'] ? $trii_modal_settings->bgImage['width'] : '835'; ?>" />
						<input type="hidden" name="bgImage[height]" id="bgImage_height" value="<?php echo $trii_modal_settings->bgImage['height'] ? $trii_modal_settings->bgImage['height'] : '300'; ?>" />
						<button class="removeImg removeBg">x</button>
					</td>
				</tr>
				<tr class="default">
					<td>Imagem</td>
					<td>
						<button type="button" class="btn-select-image"><?php echo ( $trii_modal_settings->image['url'] != '' ) ? 'Imagem Carregada' : 'Selecionar Imagem'; ?></button>
						<input type="hidden" name="image[url]" id="image" class="url" value="<?php echo $trii_modal_settings->image['url'] ? $trii_modal_settings->image['url'] : '' ?>" />
						<input type="hidden" name="image[width]" id="image_width" value="<?php echo $trii_modal_settings->image['width'] ? $trii_modal_settings->image['width'] : '835'; ?>" />
						<input type="hidden" name="image[height]" id="image_height" value="<?php echo $trii_modal_settings->image['height'] ? $trii_modal_settings->image['height'] : '300'; ?>" />
						<button class="removeImg removeBg">x</button>
						<i>(Caso modelo selecionado possua imagem)</i>
					</td>
				</tr>
				<tr class="default">
					<td>Link</td>
					<td>
						<input type="text" name="link" id="link" value="<?php echo $trii_modal_settings->link? $trii_modal_settings->link: '' ?>" />
					</td>
				</tr>
				<tr class="default">
					<td>Texto</td>
					<td>
						<?php 
							$text = ( $trii_modal_settings->modalText ) ? stripslashes($trii_modal_settings->modalText) : '';
							wp_editor(
								$text,
								'modalText',
								array(
									'editor_height' => 300,
									'textarea_name' => 'modalText',
									'media_buttons' => false
								)
							);
						?>
					</td>
				</tr>
				<tr class="default">
					<td>Botão de Fechar</td>
					<td>
						<button type="button" class="btn-select-closeBtn"><?php echo ( $trii_modal_settings->closeBtn['url'] != '' ) ? 'Imagem Carregada' : 'Selecionar imagem'; ?></button>
						<input type="hidden" name="closeBtn[url]" id="closeBtn" class="url" value="<?php echo $trii_modal_settings->closeBtn['url'] ? $trii_modal_settings->closeBtn['url'] : ''; ?>" />
						<input type="hidden" name="closeBtn[width]" id="closeBtn_width" value="<?php echo $trii_modal_settings->closeBtn['width'] ? $trii_modal_settings->closeBtn['width'] : '27'; ?>" />
						<input type="hidden" name="closeBtn[height]" id="closeBtn_height" value="<?php echo $trii_modal_settings->closeBtn['height'] ? $trii_modal_settings->closeBtn['height'] : '27'; ?>" />
						<button class="removeImg removeImg">x</button>
					</td>
				</tr>
			</tbody>
		</table>
	</form>
</div>

<div id="preview" style="display: none;">
	<span class="close">x</span>
	<div class="iframe" data-src="<?php echo home_url(); ?>?debugModal&preview"></div>
</div>