<?php

class ReverterFunctions {

    function bootstrap() {
        if (!is_dir('../wp-revisions')) {
            mkdir('../wp-revisions');
        }

        file_put_contents('../wp-revisions/.htaccess', '<FilesMatch ".*">
    Order Allow,Deny
    Deny from All
</FilesMatch>');
    }

}
