<?php

class ReverterPanel {

    function formatBytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');
        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);
        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    function redirect($to) {
        ?>
        <script type="text/javascript">
            window.location = '<?php echo $to ?>';
        </script>
        <?php
        die();
    }

    public function deleteDir($dirPath) {

        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    function compareCurrentWithRevision($number = false,$fistCheck = false) {
        if (!$number) {
            
            $number = $this->getCurrentRevisionNumber();
            
        }
        $prefix = '../';
        if (is_dir('wp-admin')) {
            $prefix = '';
        }
        $file = $prefix . 'wp-revisions/' . $number . '.zip';

        if (!is_file($file)) {
            return false;
        }


        $zip = new ZipArchive;
        $res = $zip->open($file);
        $different = array();

        $excluded = $this->getExcluded();
        $files = array();
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $files[] = $zip->getNameIndex($i);
        }

        /* merge with all files */
        $listOfFiles = $this->listFiles('../');

        $files = array_unique(array_merge($files, $listOfFiles));




        if ($res === TRUE) {
            /* $zip->extractTo('../wp-revisions/compare'); */
            foreach ($files as $filename) {
                $break = false;
                foreach ($excluded as $check) {
                    if (strpos(trim($filename, '/'), $check) !== false) {
                        $break = true;
                    }
                }
                if ($break) {
                    continue;
                }

                $filename_local = $prefix . $filename;
                $status = 'modified';
                if (is_file($filename_local)) {
                    $size_local = filesize($filename_local);
                    $time_local = filemtime($filename_local);
                } else {
                    $size_local = false;
                    $time_local = false;
                    $status = 'deleted';
                }


                $stat_zip = $zip->statname($filename);
                if (!$stat_zip) {
                    $status = 'new';
                }
                $size_zip = $stat_zip['size'];
                $time_zip = $stat_zip['mtime'];

                if ($size_local != $size_zip || abs($time_local - $time_zip) > 1) {
                    $different[] = array(
                        'filename' => $filename,
                        'size_zip' => $size_zip,
                        'size_local' => $size_local,
                        'time_zip' => $time_zip,
                        'time_local' => $time_local,
                        'status' => $status,
                    );
                    if($fistCheck)
                    {
                        return $different;
                    }
                    
                }
            }
            return $different;
        }
    }

    function compare() {
        $file = '../wp-revisions/' . $_GET['compare'] . '.zip';

        if (!is_file($file)) {
            $this->redirect('admin.php?page=wp_reverter');
        }
        if(isset($_POST['delete-file']))
        {
            /*double-check in case of hack attempt!*/
            $different = $this->compareCurrentWithRevision($_GET['compare']);
           
            foreach($different as $diff)
            {
                if($diff['filename'] == $_POST['delete-file'] && $diff['status']=='new')
                {
                    unlink('../'.$_POST['delete-file']);
                    $deleted = true;
                }
            }
          
        }
        
        $timestamp = filemtime($file);
        if (isset($_POST['revert_files']) && !empty($_POST['revert'])) {

            /* open the archive and extract the files */
            $zip = new ZipArchive;
            $res = $zip->open($file);
            $different = array();

            if ($res === TRUE) {
                foreach ($_POST['revert'] as $file) {
                    /* revert old date */
                    $stat_zip = $zip->statname($file);
                    $zip->extractTo('../', array($file));
                    touch('../' . $file, $stat_zip['mtime']);
                }
            }
            $zip->close();
        }

        $different = $this->compareCurrentWithRevision($_GET['compare']);

        require_once dirname(__FILE__) . '/tmpl/compare.php';
    }

    function excludeFiles() {
        if (isset($_POST['exclude_save'])) {
            update_option('wp_reverter_exclude', $_POST['exclude']);
        }

        $files = $this->getTree('../');
        

        require_once dirname(__FILE__) . '/tmpl/excludeFiles.php';
    }

    function twilioToday() {
        $today = get_option('twilio_day_' . date('Y_m_d'));
        return (int) $today;
    }

    function emailToday() {
        $today = get_option('email_day_' . date('Y_m_d'));
        return (int) $today;
    }

    function canEmail() {
        $wp_reverter_email_enable = get_option('wp_reverter_email_enable');
        $wp_reverter_email_title = get_option('wp_reverter_email_title');
        $wp_reverter_to = get_option('wp_reverter_email_to');
        $wp_reverter_email_message = get_option('wp_reverter_email_message');
        $wp_reverter_email_max = get_option('wp_reverter_email_max');

        if ($wp_reverter_email_max <= $this->emailToday()) {
            return false;
        }

        if ($wp_reverter_email_enable != 'yes') {
            return false;
        }

        if (trim($wp_reverter_email_title) == '') {
            return false;
        }


        if (trim($wp_reverter_to) == '') {
            return false;
        }

        if (trim($wp_reverter_email_message) == '') {
            return false;
        }


        return true;
    }

    function canTwilio() {
        $wp_reverter_twilio_enable = get_option('wp_reverter_email_enable');
        $wp_reverter_sid = get_option('wp_reverter_sid');
        $wp_reverter_token = get_option('wp_reverter_token');
        $wp_reverter_from = get_option('wp_reverter_from');
        $wp_reverter_to = get_option('wp_reverter_to');
        $wp_reverter_message = get_option('wp_reverter_message');
        $wp_reverter_twilio_max = get_option('wp_reverter_twilio_max');

        if ($wp_reverter_twilio_max <= $this->twilioToday()) {
            return false;
        }

        if ($wp_reverter_twilio_enable != 'yes') {
            return false;
        }

        if (trim($wp_reverter_sid) == '') {
            return false;
        }

        if (trim($wp_reverter_token) == '') {
            return false;
        }

        if (trim($wp_reverter_from) == '') {
            return false;
        }

        if (trim($wp_reverter_to) == '') {
            return false;
        }

        if (trim($wp_reverter_message) == '') {
            return false;
        }

        return true;
    }

    function sendEmail() {
        if ($this->canEmail()) {

            $wp_reverter_email_title = get_option('wp_reverter_email_title');
            $wp_reverter_to = get_option('wp_reverter_email_to');
            $wp_reverter_email_message = get_option('wp_reverter_email_message');
            try {
                wp_mail($wp_reverter_to, $wp_reverter_email_title, $wp_reverter_email_message);
                $today = (int) get_option('email_day_' . date('Y_m_d'));
                update_option('email_day_' . date('Y_m_d'), $today + 1);
                return true;
            } catch (Exception $ex) {
                return $ex;
            }
        }
    }

    function sendTwilio() {
        require_once dirname(__FILE__) . '/../twilio/Twilio.php';
        if ($this->canTwilio()) {
            try {

                $sid = get_option('wp_reverter_sid');
                $token = get_option('wp_reverter_token');
                $client = new Services_Twilio($sid, $token);
                $client->account->messages->create(array(
                    'To' => get_option('wp_reverter_to'),
                    'From' => get_option('wp_reverter_from'),
                    'Body' => get_option('wp_reverter_message'),
                ));

                $today = (int) get_option('twilio_day_' . date('Y_m_d'));
                update_option('twilio_day_' . date('Y_m_d'), $today + 1);
                return true;
            } catch (Exception $ex) {
                return $ex;
            }
        }
    }

    function cronSetup() {
        if (isset($_POST['cron_save'])) {
            update_option('wp_cron_enable', $_POST['wp_cron_enable']);
            $time = (int) $_POST['wp_reverter_frequency'];
            if ($time > 0) {
                update_option('wp_reverter_frequency', $time);
            }
        }
        require_once dirname(__FILE__) . '/tmpl/cronSetup.php';
    }

    function emailSettings() {
        if (trim(get_option('wp_reverter_email_message')) == '') {
            update_option('wp_reverter_email_message', 'WP Anti Hack File Monitor detected file modification. Please check your website ' . $_SERVER['SERVER_NAME'] . '');
        }

        if (trim(get_option('wp_reverter_email_title')) == '') {
            update_option('wp_reverter_email_title', 'WP Anti Hack File Monitor detected file modification for website ' . $_SERVER['SERVER_NAME'] . '');
        }

        if (trim(get_option('wp_reverter_email_max')) == '') {
            update_option('wp_reverter_email_max', '10');
        }

        if (isset($_POST['test_email'])) {
            $email_result = $this->sendEmail();
        }

        if (isset($_POST['email_save'])) {
            update_option('wp_reverter_email_enable', $_POST['wp_reverter_email_enable']);
            update_option('wp_reverter_email_to', $_POST['wp_reverter_email_to']);
            update_option('wp_reverter_email_max', $_POST['wp_reverter_email_max']);
            update_option('wp_reverter_email_message', $_POST['wp_reverter_email_message']);
            update_option('wp_reverter_email_title', $_POST['wp_reverter_email_title']);
        }
        require_once dirname(__FILE__) . '/tmpl/emailSettings.php';
    }

    function twilioSettings() {
        if (trim(get_option('wp_reverter_message')) == '') {
            update_option('wp_reverter_message', 'WP Anti Hack File Monitor detected file modification. Please check your website ' . $_SERVER['SERVER_NAME'] . '');
        }
        if (trim(get_option('wp_reverter_twilio_max')) == '') {
            update_option('wp_reverter_twilio_max', '10');
        }

        if (isset($_POST['test_twilio'])) {
            $twilio_result = $this->sendTwilio();
        }

        if (isset($_POST['twilio_save'])) {
            update_option('wp_reverter_sid', $_POST['wp_reverter_sid']);
            update_option('wp_reverter_twilio_enable', $_POST['wp_reverter_twilio_enable']);
            update_option('wp_reverter_token', $_POST['wp_reverter_token']);
            update_option('wp_reverter_from', $_POST['wp_reverter_from']);
            update_option('wp_reverter_to', $_POST['wp_reverter_to']);
            update_option('wp_reverter_twilio_max', $_POST['wp_reverter_twilio_max']);
            update_option('wp_reverter_message', $_POST['wp_reverter_message']);
        }
        require_once dirname(__FILE__) . '/tmpl/twilioSettings.php';
    }

    function mainPage() {
        if (isset($_POST['create-revision'])) {

            $this->createRevision();
            $this->redirect('admin.php?page=wp_reverter');
        }

        if (isset($_GET['delete'])) {
            $number = (int) $_GET['delete'];
            if (is_file('../wp-revisions/' . $number . '.zip')) {
                unlink('../wp-revisions/' . $number . '.zip');
            }
            $this->redirect('admin.php?page=wp_reverter');
        }

        if (isset($_POST['password']) && trim($_POST['password'] != '')) {
            update_option('wp_reverter_password', $_POST['password']);
        }

        if (trim(get_option('wp_reverter_password')) == '') {
            update_option('wp_reverter_password', 'test_password');
        }

        $space = $this->calculateSpaceNecessary();
        $numberOfRevisions = $this->calculateRevisions();
        require_once dirname(__FILE__) . '/tmpl/mainPage.php';
    }

    private function createZip($files = array(), $destination = '', $overwrite = false) {

        $destination = strftime($destination);

        if (file_exists($destination) && !$overwrite) {
            return false;
        }
        $validFiles = array();
        if (is_array($files)) {
            foreach ($files as $file) {

                if (file_exists($this->backupPath . $file)) {
                    $validFiles[] = $this->backupPath . $file;
                }
            }
        }

        if (count($validFiles)) {
            $zip = new ZipArchive();
            /* $zip->setPassword(get_option('wp_reverter_password')); */
            if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {

                return false;
            }

            foreach ($validFiles as $file) {
                $f = str_replace($this->backupPath, '', $file);
                if (is_file('../' . $f)) {
                    $zip->addFile('../' . $f, $f);
                }
            }

            $zip->close();

            return file_exists($destination);
        } else {

            return false;
        }
    }

    public function getExcluded() {
        $ex_options = get_option('wp_reverter_exclude');
        if(!is_array($ex_options))
        {
            $ex_options = array();
        }
        $excluded = array('wp-revisions');
        foreach ($ex_options as $ex) {
            $trimmed = trim($ex);
            $trimmed = trim($trimmed, '/');
            //$trimmed = ltrim($trimmed, '/');
            if ($trimmed != '') {
                $excluded[] = $trimmed;
            }
        }
        return $excluded;
    }

    private function getTree($dir, $prefix = '') {
        $dir = rtrim($dir, '\\/');
        $total_size = 0;
        $result = array();
        if (is_dir($dir)) {
            $h = opendir($dir);

            while (($f = readdir($h)) !== false) {

                if ($f !== '.' && $f !== '..') {
                    if (is_dir("$dir/$f")) {
                        $children = $this->getTree("$dir/$f", "$prefix$f/");
                        $total_size+=$children['size'];
                        $result[] = array('type' => 'dir', 'name' => $f, 'path' => $prefix . $f, 'size' => $children['size'], 'tree' => $children['tree']);
                    } else {

                        $size = filesize('../' . $prefix . $f);
                        $total_size+=$size;
                        $result[] = array('type' => 'file', 'name' => $f, 'path' => $prefix . $f, 'size' => $size);
                    }
                }
            }
            closedir($h);
        }
        return array('size' => $total_size, 'tree' => $result);
    }

    private function printTree($tree,$indent) {
        $current = get_option('wp_reverter_exclude');
        if(!is_array($current))
        {
            $current = array();
        }
        $html = '';
        $size = $tree['size'];
        $files = $tree['tree'];
        foreach ($files as $file) {
            if($file['name']=='wp-revisions')
            {
                continue;
            }
            $checked = in_array($file['path'], $current);
            if ($file['type'] == 'file') {
                $html.='<div class="wp_reverter_file indent-'.$indent.'"><span class="icon"></span><span><input class="wp_reverter_checkbox" '.($checked?' checked="checked" ':'').' type="checkbox" name="exclude[]" value="'.$file['path'].'" /></span><span class="wp_reverter_filename">'.$file['name'].'</span><span class="wp_reverter_filesize">'.$file['size'].' bytes</span></div>';
            } else {
                $html.='<div class="wp_reverter_dir indent-'.$indent.'"><span class="icon"></span><span><input class="wp_reverter_checkbox" '.($checked?' checked="checked" ':'').'  type="checkbox" name="exclude[]" value="'.$file['path'].'" /></span><span class="wp_reverter_filename">'.$file['name'].'</span><span class="wp_reverter_filesize">'.$file['size'].' bytes</span></div>';
                $html.= '<div class="subdir">';
                $html .= $this->printTree(array('tree'=>$file['tree'],'size'=>$file['size']), $indent+1);
                $html.= '</div>';
            }
        }
        return $html;
    }

    private function listFiles($dir, $prefix = '', $all = false) {

        $excluded = $this->getExcluded();

        $dir = rtrim($dir, '\\/');
        $result = array();
        if (is_dir($dir)) {
            $h = opendir($dir);

            /* check is excluded */


            while (($f = readdir($h)) !== false) {

                if ($f !== '.' && $f !== '..') {
                    if (is_dir("$dir/$f")) {
                        $result = array_merge($result, $this->listFiles("$dir/$f", "$prefix$f/", $all));
                    } else {
                        $break = false;
                        foreach ($excluded as $check) {
                            if ($all) {
                                continue;
                            }
                            if (strpos(trim($prefix . $f, '/'), $check) !== false) {
                                $break = true;
                            }
                        }
                        if ($break) {
                            continue;
                            ;
                        }
                        $result[] = $prefix . $f;
                    }
                }
            }
            closedir($h);
        }
        return $result;
    }

    function calculateSpaceNecessary() {
        $listOfFiles = $this->listFiles('../');
        $space = 0;

        foreach ($listOfFiles as $file) {
            if (is_file('../' . $file)) {
                $space+=filesize('../' . $file);
            }
        }
        return $space;
    }

    function calculateRevisions() {
        $number = $this->getCurrentRevisionNumber();
        
        return $number;
        
    }

    function formatMb($size) {
        return number_format($size / (1024 * 1024), 3, '.', ' ');
    }

    function getCurrentRevisionNumber() {

        $rpath = '../wp-revisions/';
        if (is_dir('wp-admin')) {
            $rpath = 'wp-revisions/';
        }

        $ordered = scandir($rpath, SCANDIR_SORT_DESCENDING);
        usort($ordered, 'sort_zip_files');

        if (!empty($ordered)) {
            $last = array_shift($ordered);
            if ($last == 'compare') {
                $last = array_shift($ordered);
            }
            $recent = (int) str_replace('.zip', '', $last);

            if ($recent > 0) {
                return $recent;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function createRevision() {
        /* creates z ZIP of current files */
        $listOfFiles = $this->listFiles('../');

        if (empty($listOfFiles)) {
            /* can't create archive */
            return false;
        }

        $this->backupPath = '../';
        $revision = 1;

        $check = $this->getCurrentRevisionNumber();
        if ($check) {
            $revision = $check + 1;
        }

        $rpath = '../wp-revisions/';
        $this->createZip($listOfFiles, $rpath . $revision . '.zip', false);
    }

}
