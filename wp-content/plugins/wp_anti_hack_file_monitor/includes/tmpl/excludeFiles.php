<div id="wp-reverter-panel">
    <h3>Exclude Files</h3>
    <?php if (isset($_POST['exclude_save'])): ?>
        <p class="wp-reverter-success">The settings has been saved</p>
    <?php endif ?>
    <p class="p-margin">Here you can set path to files/catalogs you <b>don't want to</b> check. The selected files won't be archived in repositories and modifications of these files won't be notified via email/sms.</p>
    <p class="p-margin">Click the folder icon to see it's content</p>
    <p class="p-margin"><b>Excluding entire folder will exclude all files inside this folder</b></p>
    <p class="p-margin">
        <b>
            Remember. If you exclude the path, create the revision, and then include this path back - it might show the local files as "new". Please create new revision after reinclusion of any path.
        </b>
    </p>
    <form action="" class="wp-reverter-form" method="post">
        <div class="one-form-block">
            <label for="wp_reverter_exclude">Exluded Paths</label>
            <div class='wp_reverter_all_files'>
            <?php echo $this->printTree($files,0)?>
            </div>
            
        </div>
        <div class="one-form-block">
            <p>&nbsp;</p>
            <input type="submit" name="exclude_save" value="Save" class="button"/>
        </div>
    </form>
</div>
<style type="text/css">
<?php for($i=0;$i<15;$i++):?>
.indent-<?php echo $i ?>
{
    padding-left: <?php echo $i*40?>px;
}

.indent-<?php echo $i ?> .wp_reverter_filesize
{
    padding-left: <?php echo ((15-$i)*40)-300?>px;
}
<?php endfor; ?>
</style>
