<div id="wp-reverter-panel">
    <h3>Twilio Settings</h3>
    <p>Don't forget to put country prefix in your phone numbers. For example <b>48799449414</b> if the county is Poland and phone number <b>799449414</b></p>
    
  <p>Today were sent <b><?php echo $this->twilioToday()?></b> messages.</p>
    
    <?php if (isset($_POST['twilio_save'])): ?>
        <p class="wp-reverter-success">The settings has been saved</p>
    <?php endif ?>

    <?php if (isset($_POST['test_twilio']) && $twilio_result===true): ?>
        <p class="wp-reverter-success">The SMS has been sent via Twilio API.</p>
    <?php endif ?>
        
    <?php if (isset($_POST['test_twilio']) && $twilio_result!==true): ?>
        <p class="wp-reverter-error">An error occured! Please double-check the credentials.</p>
    <?php endif ?>

        
    <form action="" class="wp-reverter-form" method="post">
        <div class="one-form-block">
            <label for="wp_reverter_twilio_enable">Enable Twilio Notifications</label>
            <p>
                <select name="wp_reverter_twilio_enable">
                    <option <?php if (get_option('wp_reverter_twilio_enable') == 'no'): ?>selected="selected"<?php endif ?> value="no">No</option>
                    <option <?php if (get_option('wp_reverter_twilio_enable') == 'yes'): ?>selected="selected"<?php endif ?> value="yes">Yes</option>
                </select>                
            </p>
        </div>
        <div class="one-form-block">
            <label for="wp_reverter_sid">Sid</label>
            <p><input id="wp_reverter_sid"  type="text" name="wp_reverter_sid" value="<?php echo get_option('wp_reverter_sid') ?>" /></p>
        </div>
        <div class="one-form-block">
            <label for="wp_reverter_token">Token</label>
            <p><input id="wp_reverter_token"  type="text" name="wp_reverter_token" value="<?php echo get_option('wp_reverter_token') ?>" /></p>
        </div>
        <div class="one-form-block">
            <label for="wp_reverter_from">From (phone number)</label>
            <p><input id="wp_reverter_from" t type="text" name="wp_reverter_from" value="<?php echo get_option('wp_reverter_from') ?>" /></p>
        </div>
        <div class="one-form-block">
            <label for="wp_reverter_to">To (phone number)</label>
            <p><input id="wp_reverter_to"  type="text" name="wp_reverter_to" value="<?php echo get_option('wp_reverter_to') ?>" /></p>
        </div>
        <div class="one-form-block">
            <label for="wp_reverter_message">Message</label>
            <p>
                <textarea id="wp_reverter_message" name="wp_reverter_message"><?php echo get_option('wp_reverter_message') ?></textarea>                
            </p>
        </div>
        <div class="one-form-block">
            <label for="wp_reverter_twilio_max">Max messages per day</label>
            <p>
                <input id="wp_reverter_twilio_max"  type="text" name="wp_reverter_twilio_max" value="<?php echo get_option('wp_reverter_twilio_max') ?>" />                
            </p>
        </div>
        <div class="one-form-block">
            <input type="submit" name="twilio_save" value="Save" class="button"/>
        </div>


    </form>
    <?php if ($this->canTwilio()): ?>
        <form action='' method='post' class="test_twilio">
            <input type='submit' class="button" name='test_twilio' value='Send test SMS' />
        </form>
    <?php else: ?>
        <br />
        <b>You cannot use the API now. Lack of credentials or messages per day quota exceeded.</b>
    <?php endif ?>
</div>