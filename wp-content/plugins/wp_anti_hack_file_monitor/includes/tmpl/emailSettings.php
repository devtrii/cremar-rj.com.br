<div id="wp-reverter-panel">
    <h3>Email Settings</h3>
    
    
  <p>Today were sent <b><?php echo $this->emailToday()?></b> messages.</p>
    
    <?php if (isset($_POST['email_save'])): ?>
        <p class="wp-reverter-success">The settings has been saved</p>
    <?php endif ?>

    <?php if (isset($_POST['test_email']) && $email_result===true): ?>
        <p class="wp-reverter-success">The Emas has been sent.</p>
    <?php endif ?>
        
    <?php if (isset($_POST['test_email']) && $email_result!==true): ?>
        <p class="wp-reverter-error">An error occured! Please double-check the credentials.</p>
    <?php endif ?>

        
    <form action="" class="wp-reverter-form" method="post">
        <div class="one-form-block">
            <label for="wp_reverter_email_enable">Enable Email Notifications</label>
            <p>
                <select name="wp_reverter_email_enable">
                    <option <?php if (get_option('wp_reverter_email_enable') == 'no'): ?>selected="selected"<?php endif ?> value="no">No</option>
                    <option <?php if (get_option('wp_reverter_email_enable') == 'yes'): ?>selected="selected"<?php endif ?> value="yes">Yes</option>
                </select>                
            </p>
        </div>
        
        <div class="one-form-block">
            <label for="wp_reverter_email_to">Email address</label>
            <p><input id="wp_reverter_email_to"  type="text" name="wp_reverter_email_to" value="<?php echo get_option('wp_reverter_email_to') ?>" /></p>
        </div>
        
        <div class="one-form-block">
            <label for="wp_reverter_email_title">Title</label>
            <p><input id="wp_reverter_email_title"  type="text" name="wp_reverter_email_title" value="<?php echo get_option('wp_reverter_email_title') ?>" /></p>
        </div>
        
        <div class="one-form-block">
            <label for="wp_reverter_email_message">Message</label>
            <p>
                <textarea id="wp_reverter_email_message" name="wp_reverter_email_message"><?php echo get_option('wp_reverter_email_message') ?></textarea>                
            </p>
        </div>
        <div class="one-form-block">
            <label for="wp_reverter_email_max">Max messages per day</label>
            <p>
                <input id="wp_reverter_email_max"  type="text" name="wp_reverter_email_max" value="<?php echo get_option('wp_reverter_email_max') ?>" />                
            </p>
        </div>
        <div class="one-form-block">
            <input type="submit" name="email_save" value="Save" class="button"/>
        </div>


    </form>
    <?php if ($this->canEmail()): ?>
        <form action='' method='post' class="test_email">
            <input type='submit' class="button" name='test_email' value='Send test Email' />
        </form>
    <?php else: ?>
        <br />
        <b>You cannot use the Email notifications now. Lack of credentials or messages per day quota exceeded.</b>
    <?php endif ?>
</div>