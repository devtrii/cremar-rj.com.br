<div id="wp-reverter-panel">
    <h3>Cron Setup</h3>
    <?php if (isset($_POST['cron_save'])): ?>
        <p class="wp-reverter-success">The settings has been saved</p>
    <?php endif ?>
    <p class='p-margin'>You can setup the plugin to check the website status periodically. In case of file(s) modification, you'll be notified via e-mail or sms.</p>
    <p class='p-margin'>The real Cron is a program that you can setup in your hosting panel.</p>
    <p class='p-margin'>
        <a href="http://en.wikipedia.org/wiki/Cron" target="_blank">Read about Cron on Wikipedia</a>
    </p>
    <p class='p-margin'>
        <a href="https://documentation.cpanel.net/display/ALD/Cron+Jobs" target="_blank">Read about Cron setup for cPanel</a>
    </p>
    <p class='p-margin'>
        You need to add following url to be executed as a cron job:
    </p>
    <p class='p-margin'>
        <a href="<?php echo get_home_url() ?>/?wp_reverter_check=true"><?php echo get_home_url() ?>/?wp_reverter_check=true</a>
    </p>
    <?php if(!$this->canEmail() && !$this->canTwilio()):?>
    <p class='p-margin p-error'>
        <b>Both Email and Twilio methods are not configured! Please setup at least one of these.</b>
    </p>
    <p>&nbsp;</p>
    <?php endif ?>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
    <p class='p-margin'>
        <b>If you don't know how to setup cron for your website, you can use the following pseudo-cron feature</b>
    </p>
    
    
    <p class='p-margin'>
        Pseudo-cron is executed by one of your visitors, therefore you need to have one to receive notifications. 
    </p>
    <form action="" class="wp-reverter-form" method="post">
        <div class="one-form-block">
            <label for="wp_cron_enable">Enable Pseudo-cron</label>
            <p>
                <select name="wp_cron_enable">
                    <option <?php if (get_option('wp_cron_enable') == 'no'): ?>selected="selected"<?php endif ?> value="no">No</option>
                    <option <?php if (get_option('wp_cron_enable') == 'yes'): ?>selected="selected"<?php endif ?> value="yes">Yes</option>
                </select>                
            </p>
        </div>

        <div class="one-form-block">
            <label for="wp_reverter_frequency">Frequency (minutes, more then 0)</label>
            <p><input id="wp_reverter_frequency"  type="text" name="wp_reverter_frequency" value="<?php echo get_option('wp_reverter_frequency') ?>" /></p>
        </div>
        <div class="one-form-block">
            <input type="submit" name="cron_save" value="Save" class="button"/>
        </div>
        <p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><b>Notifications methods status:</b></p>

        <?php if($this->canTwilio()):?>
        <p>You have configured the Twilio API credentials.</p>
        <?php else: ?>
        <p>You haven't configured the Twilio API credentials. <a href='admin.php?page=twilio_settings'>Click here</a> to setup.</p>
        <?php endif ?>
        
        <?php if($this->canEmail()):?>
        <p>You have configured the Email settings.</p>
        <?php else: ?>
        <p>You haven't configured the Email settings. <a href='admin.php?page=email_settings'>Click here</a> to setup.</p>
        <?php endif ?>
    </form>
</div>