<div id="wp-reverter-panel">
    <?php
    /*     * WILL BE IN USE FROM PHP 5.6* */
    /*

      <h3>Zip Files Password</h3>
      <p>This password is used in zipped revisions and protects against automated attacks. Almost all malicious scripts are made to search unzipped PHP files, so this password doesn't need to be strong - any password is enough.</p>
      <p>Please remember that this form won't change the passwords of previously created revisions.</p>

      <?php if(isset($_POST['password'])):?>
      <p class="wp-reverter-success">The password has been changed</p>
      <?php endif ?>

      <form action="" method="post">
      <input type="text" name="password" value="<?php echo get_option('wp_reverter_password') ?>" />
      <input type="submit" name="save-password" class="button" value="Save" />
      </form>
     */
    ?>
    <h3>Create New Revision</h3>
    <p>Revision is a backup of your files, thanks to this you can revert your website in case of hack attempt. 
        <br /><b>You need at least one revision to make the plugin functional!</b> 
    <p>Each revisions is stored as password-protected zip file, so viruses and hackers won't be able to modify the files inside.</p>
    <br />You can create as many revisions as you like, but keep in mind that it takes your hosting quota.</p>
<p>Your <b>uncompressed</b> files takes <b><?php echo $this->formatMb($space) ?> MB</b></p>
<form id="create-revision" action="" method="post">
    <input type="submit" name="create-revision" class="button" value="Create Revision Now" />
</form>
<?php if ($numberOfRevisions > 0): ?>
    <h3>Current Revisons</h3>
    <p>The plugin will compare the current files with most recent revision, however you can store more then one revision, just to have the files backuped.</p>
    <p>
        <span class="most-recent"></span> - most recent revision.
    </p>
    <table class="wp-list-table widefat plugins first-row" id="rev-table">
        <thead>
            <tr>
                <th>Revision no.</th>
                <th>Date</th>
                <th>Zip size</th>
                <th>Action</th>

            </tr>
        </thead>
        <tbody>
            <?php 
            $files = scandir('../wp-revisions/',SCANDIR_SORT_DESCENDING);
            usort($files,'sort_zip_files');
            
            foreach($files as $i=>$file):
                $number = str_replace('.zip','',$file);
                if($file=='.' || $file=='..' || $file=='.htaccess' || !is_file('../wp-revisions/'.$number.'.zip'))
                {
                    continue;
                }
                ?>
                <tr class="<?php if($i%2==0):?>alternate<?php endif ?>">
                    <th><?php echo $number ?></th>
                    <th>
                        <?php
                        
                        $timestamp = filemtime('../wp-revisions/'.$number.'.zip');                        
                        echo date(get_option('date_format').' '.get_option('time_format'),$timestamp);
                        ?>
                    </th>
                    <th>
                        <?php echo $this->formatMb(filesize('../wp-revisions/'.$number.'.zip'))?> MB
                    </th>
                    <th class="text-right">
            <p><a target="_blank" href="index.php?download_revision=<?php echo $number ?>">Download</a></p>
            <p><a href="admin.php?page=wp_reverter&compare=<?php echo $number ?>">Compare with current files</a></p>
            <p><a class="delete" href="admin.php?page=wp_reverter&delete=<?php echo $number ?>">Delete</a></p>
            </th>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
<?php endif ?>
</div>