<div id="wp-reverter-panel">
    <h3>Compare Revisions</h3>
    <p><b class="revision-stats">Revision Number:</b> <?php echo $_GET['compare'] ?></p>
    <p><b class="revision-stats">Date created: </b><?php
        echo date(get_option('date_format') . ' ' . get_option('time_format'), $timestamp);
        ?>
    </p>

    <div>
        <br />
        <br />
        <?php if (isset($_POST['revert_files'])): ?>
            <p class="wp-reverter-success">The selected files has been reverted!</p>
        <?php endif ?>
            <?php if (isset($deleted)): ?>
            <p class="wp-reverter-success">The selected file has been deleted!</p>
        <?php endif ?>
        <?php if (count($different) == 0): ?>
            <p class="no-diff"><b>No differences!</b></p>
        <?php else: ?>
            <p>There are <b><?php echo count($different) ?></b> files different;

            <h3 class="margin-top-details">Click to see details</h3>

            <?php
            $file = '../wp-revisions/' . ((int) $_GET['compare']) . '.zip';
            $zip = new ZipArchive;
            $res = $zip->open($file);
            ?>
            <?php if (!empty($different)): ?>
                <form action="" id="revert-files-form" method="post">
                    <table class="wp-list-table widefat plugins full-width compare" id="rev-table">
                        <thead>
                            <tr>
                                <th>Select to revert</th>
                                <th>Status</th>
                                <th>File</th>
                                <th>Current file size</th>
                                <th>Old file size</th>
                                <th>Current file modification time</th>
                                <th>Old file modification time</th>
                            </tr>
                        </thead>
                        <?php foreach ($different as $i => $d): ?>
                            <tr class="<?php if ($i % 2 == 0): ?>alternate<?php endif ?> trigger-row <?php echo $d['status']?>">
                                <th>
                                    <?php if($d['status']!='new'):?>
                                    <input type="checkbox" name="revert[]" value="<?php echo $d['filename'] ?>" />
                                    <?php else: ?>
                                    <a href="#" rel="<?php echo urldecode($d['filename']) ?>" class="confirm-delete">delete</a>
                                    <?php endif ?>
                                </th>
                                <th class="status-<?php echo $d['status'] ?>"><?php echo $d['status'] ?></th>
                                <th><?php echo $d['filename'] ?></th>
                                <th><?php if ($d['size_local']): echo $d['size_local'] ?> bytes<?php endif ?></th>
                                <th><?php if ($d['size_zip']): echo $d['size_zip'] ?> bytes<?php endif ?></th>
                                <th><?php if ($d['time_local']) echo date(get_option('date_format') . ' ' . get_option('time_format'), $d['time_local']) ?></th>
                                <th><?php if ($d['time_zip']) echo date(get_option('date_format') . ' ' . get_option('time_format'), $d['time_zip']) ?></th>
                            </tr>
                            
                            <tr class="<?php if ($i % 2 == 0): ?>alternate<?php endif ?> details">
                                <th colspan="7">
                                    <?php
                                    if(is_file('../' . $d['filename']))
                                    {
                                        $is_picture = exif_imagetype('../' . $d['filename']);
                                    }
                                    else
                                    {
                                        /*check ext*/
                                        $ext = strtolower(array_pop(explode('.',$d['filename'])));
                                        $is_picture = in_array($ext, array('png','jpg','jpeg','gif','bmp'));
                                    }
                                    

                                    if (!$is_picture && $d['size_local'] < 51200 && $d['size_zip'] < 51200):
                                        if(is_file('../' . $d['filename']))
                                        {
                                            $current = file_get_contents('../' . $d['filename']);
                                        }
                                        else
                                        {
                                            $current='';
                                        }
                                        
                                        $from_zip = $zip->getFromName($d['filename']);

                                        $diff = DiffWpActiHack::toTable(DiffWpActiHack::compare($from_zip, $current), '', '');
                                        if (strpos($diff, 'diffInserted') || strpos($diff, 'diffDeleted')) {
                                            ?>
                                    <p class="diffInserted square wp-legend">
                                        <span></span> - new code
                                    </p>
                                    <p class="diffDeleted square last-legend wp-legend">
                                        <span></span> - removed or modified code
                                    </p>
                                   
                                    
                                    <table class="above-compare">
                                        <tr>
                                            <td><b>Old file</b></td>
                                            <td><b>Current file</b></td>
                                        </tr>
                                    </table>
                                    <?php
                                   
                                    echo $diff;
                                } else {
                                    ?>
                                    <p><b>No difference in content were found, but both of the files has different date of modification. Maybe the changes were reverted by person who modified this file.</b></p>
                                    <?php
                                } elseif ($is_picture && $d['size_local'] < 524288 && $d['size_zip'] < 524288):
                                ?>
                                <table class="above-compare">
                                    <tr>
                                        <td>
                                            <b>Old picture</b>
                                            <?php $from_zip = $zip->getFromName($d['filename']);
                                            if(trim($from_zip)!=''):
                                            ?>
                                            <p>
                                                <img src="data:image/png;base64,<?php echo base64_encode($from_zip) ?>" alt="" />
                                            </p>
                                            <?php endif ?>
                                        </td>
                                        <td>
                                            
                                            <b>Current picture</b>
                                            <?php if(is_file('../' . $d['filename'])):?>
                                            <p>
                                                <img src="<?php echo '../' . $d['filename'] ?>" alt="" />
                                            </p>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                </table>
                                <?php
                            else:
                                ?>
                                <p><b>At least one of the files is bigger then 50kb, you cannot compare files that big.</b></p>
                            <?php endif ?>
                            </th>
                            </tr>
                           
                        <?php endforeach ?>
                    </table>
                    <input type="submit" id="revert-button" class="button" name="revert_files" value="Revert selected files" />
                </form>
            <?php endif ?>
        <?php endif ?>
    </div>

    <p>
        <a href="admin.php?page=wp_reverter">&laquo; back</a>
    </p>
</div>