(function ($)
{
    $(document).ready(function ()
    {
        
        $('.confirm-delete').click(function(e)
        {
            e.stopPropagation();
            e.preventDefault();
            if(confirm('Are you sure you want to delete this file?'))
            {
                var form = $('<form action="" method="post"></form>').append('<input type="hidden" name="delete-file" value="'+$(this).attr('rel')+'"></input>');
                $('body').append(form);
                form.submit();
            }
        });
        
        $('.wp_reverter_dir .icon').click(function()
        {
            $(this).parent().next().toggle();
        });
        
        $('.wp_reverter_checkbox:checked').each(function(k,v)
        {
           $(this).parents('.subdir').css('display','block'); 
        });
        
        $('#create-revision').submit(function ()
        {
            return confirm('Are you sure you want to create the revision?');
        });

        $('.test_twilio').submit(function()
        {
            return confirm('Are you sure you want to send test SMS? Your Twilio account will be charged.');
        })

        $('.delete').click(function (e)
        {
            return confirm("Are you sure you want to delete this revisions?");
        });
        
        $('#revert-files-form').submit(function(e)
        {
            
            if($('#revert-files-form input[type="checkbox"]').is(':checked'))
            {
                return confirm('Are you sure you want to revert selected files ? The local files will be overwritten by older one from the selected repository.');
            }
            else
            {
                e.preventDefault();
            }
            
            
        });

        $('#rev-table input[type="checkbox"]').click(function(e)
        {
            e.stopPropagation();
        });

        $('.trigger-row').click(function ()
        {
            $('.details').not($(this).next()).css('display', 'none');
            $(this).next().toggle();
        });

    });
})(jQuery);