<?php
/*
  Plugin Name: WP Anti Hack File Monitor
  Description: WP Anti Hack File Monitor detects attempts of modification/deletion/creation files in your WP and will send you a notification via SMS/E-mail.
  Author: Cezary Siwa
  Version: 1.0
  Plugin URI: http://cezary-siwa.pl
  Author URI: http://cezary-siwa.pl
 */

require_once dirname(__FILE__) . '/includes/ReverterPanel.php';
require_once dirname(__FILE__) . '/includes/ReverterFunctions.php';
require_once dirname(__FILE__) . '/includes/diff.php';



function sort_zip_files($a, $b) {

    $explodeda = explode('.zip', $a);
    $explodedb = explode('.zip', $b);
    $a = (int) (array_shift($explodeda));
    $b = (int) (array_shift($explodedb));
    return $a < $b;
}

class WPReverter {

    function __construct() {
        $this->panel = new ReverterPanel;
        $functions = new ReverterFunctions;
        $functions->bootstrap();
    }

    function settings() {
        if (isset($_GET['compare'])) {
            $this->panel->compare();
        } else {
            $this->panel->mainPage();
        }
    }

    function twilioSettings() {
        $this->panel->twilioSettings();
    }

    function emailSettings() {
        $this->panel->emailSettings();
    }

    function cronSetup() {
        $this->panel->cronSetup();
    }

    function excludeFiles() {
        $this->panel->excludeFiles();
    }

    function adminMenu() {
        add_menu_page('WP Anti Hack File Monitor', 'WP Anti Hack File Monitor', 'manage_options', 'wp_reverter', array($this, 'settings'));
        add_submenu_page('wp_reverter', 'Exclude Files', 'Exclude Files', 'manage_options', 'exclude_files', array($this, 'excludeFiles'));
        add_submenu_page('wp_reverter', 'Twilio Notifications', 'Twilio Notifications', 'manage_options', 'twilio_settings', array($this, 'twilioSettings'));
        add_submenu_page('wp_reverter', 'Email Notifications', 'Email Notifications', 'manage_options', 'email_settings', array($this, 'emailSettings'));
        add_submenu_page('wp_reverter', 'Cron Setup', 'Cron Setup', 'manage_options', 'cron_setup', array($this, 'cronSetup'));
    }

    function notice() {
        ?>
        <div class="updated">
            <p><b>WP Anti Hack File Monitor</b> requires at least one revision created to be fully functional. <a href="admin.php?page=wp_reverter">Click here</a> to create one.</p>
        </div>
        <?php
    }

    function init() {

        add_action('admin_menu', array($this, 'adminMenu'));

        if ($this->panel->calculateRevisions() == 0) {
            add_action('admin_notices', array($this, 'notice'));
        }

        $pseudocron = false;
        if (get_option('wp_cron_enable') == 'yes') {
            $frequency = get_option('wp_reverter_frequency');
            $last = get_option('wp_reverter_last_pseudocron');


            if (time() - $last >= ($frequency * 60)) {
                $pseudocron = true;
                update_option('wp_reverter_last_pseudocron', time());
            }
        }

        if (isset($_GET['wp_reverter_check']) || $pseudocron) {
            $check = $this->panel->compareCurrentWithRevision(false, true);

            if (!empty($check)) {
                /* check if last message wasn't about this set of files */

                $code = serialize($check);
                $last_check = get_option('wp_revision_last_check');

                if ($last_check != $code) {
                    update_option('wp_revision_last_check', $code);
                    $this->panel->sendTwilio();
                    $this->panel->sendEmail();
                }

                /* send via e-mail */

                /* send via twillio */
            }

            if (!$pseudocron) {
                die();
            }
        }

        if (isset($_GET['download_revision'])) {
            if (is_admin() && current_user_can('manage_options')) {
                $filename = $_GET['download_revision'];
                $file = '../wp-revisions/' . $filename . '.zip';
                if (is_file($file)) {


                    $size = filesize($file);

                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename=' . $_GET['download_revision'] . '.zip');
                    header('Content-Transfer-Encoding: binary');
                    header('Connection: Keep-Alive');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . $size);
                    set_time_limit(3600);
                    readfile($file);
                    //echo file_get_contents($file);
                } else {
                    die('Wrong filename!');
                }
            } else {
                die('Not an admin!');
            }
            die();
        }
    }

    function prepareAdmin() {
        wp_enqueue_script('hp_admin_js', plugin_dir_url(__FILE__) . 'js/hp_admin.js', array(), rand(1, 1000));
        wp_enqueue_style('hp_admin_css', plugin_dir_url(__FILE__) . 'css/hp_admin.css', array(), rand(1, 1000));
    }

}

$WpReverter = new WPReverter();
add_action('init', array($WpReverter, 'init'));
add_action('admin_init', array($WpReverter, 'prepareAdmin'));
