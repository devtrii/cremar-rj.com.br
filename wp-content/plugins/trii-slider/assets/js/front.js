(function($, window) {

	// Cria o autoplay caso tenha a classe autoplay
	if($(".autoplay").length>0){
		$(document).on({
			"ready": function(e) {

				setInterval( function(){
					if ( !$(".triislide").is(":hover") ){
						var $this 	= $( ".triislide.isSlide .bullets li.active" ).next( "li" );
						if( $this.length <= 0 ){
							$this 	= $( ".triislide .bullets li" ).first();	
						}

						$this.click();
					}

				}, 5000);

			}
		});
	}


	function getClassItem() {
		var w = jQuery(document).width(),
			c = '';

		if ( w >= 769 )
			c = 'desktop';
		if ( w <= 768 ) 
			c = 'tablet';
		if ( w <= 475 )
			c = 'mobile';

		return c;
	 }

	 $( window ).on( 'resize', function(){

		var $this 	= $( '.triislide.isSlide .bullets li.active' );

		$( '.triislide.isSlide .container-triislide > li' ).hide();
		$( '.triislide.isSlide .'+ getClassItem() +'.slide' + $this.data( 'slide' ) ).show( );

	});

	$(document).on({
		'ready': function(e) {

			$( document ).on( 'click', '.triislide.isSlide .bullets li', function(e){
				e.preventDefault();

				var $this 	= $( this);

				console.log( getClassItem() );

				$( '.triislide .bullets li' ).removeClass( 'active' );
				$this.addClass( 'active' );

				$( '.triislide .container-triislide > li' ).hide();
				$( '.triislide .'+ getClassItem() +'.slide' + $this.data( 'slide' ) ).fadeIn( 1000 );

			});

		} // Document Ready
	});

})(jQuery, window);
