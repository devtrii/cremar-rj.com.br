(function($, window) {
	$(document).on({
		'ready': function(e) {

			// Uploading files
			var file_frame;
			var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
			var set_to_post_id = 1; // Set this

			$( document ).on( 'click', '.btn-select-img', function(event){
				event.preventDefault();
					
					$button = $( this );

			     // If the media frame already exists, reopen it.
				    if ( file_frame != undefined && file_frame ) {
				      file_frame.open();
				      return;
				    }
				 
				    // Create the media frame.
				    file_frame = wp.media.frames.file_frame = wp.media({
				      title: jQuery( this ).data( 'uploader_title' ),
				      button: {
				        text: jQuery( this ).data( 'uploader_button_text' ),
				      },
				      multiple: false  // Set to true to allow multiple files to be selected
				    });
				 
				    // When an image is selected, run a callback.
				    file_frame.on( 'select', function() {
				      // We set multiple to false so only get one image from the uploader
				      attachment = file_frame.state().get('selection').first().toJSON();
						//$('.foto_loader').show();
							
						console.log( attachment );

			      		if( attachment.width != $button.data( 'width' ) ){
					    	alert( 'Largura inválida, tamanho permitido: ' + $button.data( 'width' ) + 'px.' );
					    }else if( attachment.subtype != 'jpeg' && attachment.subtype != 'jpg' && attachment.subtype != 'png' ){
				      		alert( 'Extensão de arquivo não permitida. \n \n Extensões Permitidas: JPEG, JPG, PNG' );
				      	}else{

							$button.next( '.urlField' ).val( attachment.url ).change();
							$button.closest( '.row' ).find( '.prev' ).attr( 'src', attachment.sizes.thumbnail.url );
							$button.closest( '.row' ).find( '.box-prev' ).fadeIn( 400 );
							$button.text( 'Imagem Carregada' ).addClass( 'has' );

				      	}
				 
				    });
				 
				    file_frame.open();
			});

			// Restore the main ID when the add media button is pressed
			jQuery('a.add_media').on('click', function() {
				wp.media.model.settings.post.id = wp_media_post_id;
			});

			$( document ).on( 'click', '.addNewSlide', function(e){
				e.preventDefault();

				var $tabs 	= $( '.tabs' );
					count 	= $tabs.find( '.tab' ).last().data( 'count' ) + 1,
					$boxs 	= $( '.box-tabs' );

					$tabs.append( '<li id="boxslide'+ count +'" class="tab" data-count="'+ count +'">Slide '+ count +'</li>' );

				var htmlBox = '<div class="box-op boxslide'+ count +'" style="none">'+
								'<div class="box-link">'+
						        	'<label>Link do Slide</label>'+
						        	'<input type="text" name="slide['+ count +'][link]" placeholder="Informe o link com HTTPS://" />'+
						        '</div>'+
						        '<div class="row">'+
						        	'<div class="title">Slide - Versão Desktop ( <b>1920x</b> )</div>'+
						        	'<button type="button" class="btn-select-img" data-width="1920">Selecione a Imagem</button>'+
						        	'<input type="hidden" name="slide['+ count +'][desktop]" class="urlField urlDesk" />'+
						        	'<img class="prev" />'+
						        '</div>'+

						        '<div class="row">'+
						        	'<div class="title">Slide - Versão Tablet ( <b>768x</b> )</div>'+
						        	'<button type="button" class="btn-select-img" data-width="768">Selecione a Imagem</button>'+
						        	'<input type="hidden" name="slide['+ count +'][tablet]" class="urlField" />'+
						        	'<img class="prev" />'+
						        '</div>'+

						        '<div class="row">'+
						        	'<div class="title">Slide - Versão Mobile ( <b>475x</b> )</div>'+
						        	'<button type="button" class="btn-select-img" data-width="475">Selecione a Imagem</button>'+
						        	'<input type="hidden" name="slide['+ count +'][mobile]" class="urlField" />'+
						        	'<img class="prev" />'+
						        '</div>'+
							'</div>';

					$tabs.find( '.tab' ).removeClass( 'active' );
					$tabs.find( '#boxslide'+ count ).addClass( 'active' );

					$boxs.find( '.box-op' ).hide();
					$boxs.append( htmlBox );
					$boxs.find( '.boxslide'+ count ).fadeIn( 400 );
					

			});

			$( document ).on( 'click', '.tabs li.tab', function(e){

				var $this 	= $( this ),
					$tabs 	= $( '.tabs' ),
					$boxs 	= $( '.box-tabs' );

					$tabs.find( '.tab' ).removeClass( 'active' );
					$this.addClass( 'active' );

					$boxs.find( '.box-op' ).hide();
					$boxs.find( '.' + $this.attr( 'id' ) ).fadeIn( 400 );

			});

			$( document ).on( 'click', '.removeNewSlide', function(e){
				e.preventDefault();

				var $active = $( '.tabs li.active' ),
					$boxs 	= $( '.box-tabs' ),
					$tabs 	= $( '.tabs' );

				if( confirm( 'Você realmente deseja remover o ' + $active.html() + '?' ) ){
					
					$boxs.find( '.' + $active.attr( 'id' ) ).remove();
					$active.remove();

					var count 		= $tabs.find( '.tab' ).length;

						if( count > 0 ){
							var $nextTab 	= $tabs.find( '.tab' ).last();

							$tabs.find( '.tab' ).removeClass( 'active' );
							$nextTab.addClass( 'active' );
							$boxs.find( '.' + $nextTab.attr( 'id' ) ).fadeIn( 400 );
						}else{
							$( '.addNewSlide' ).click();
						}
				}

			});

			$( document ).on( 'click', '#publish', function(e){

				if( $( '#validSlide' ).val().length <= 0 ){
					e.preventDefault();

					alert( 'Você precisa adicionar ao menos 1 slide para publicar.' );
				}

			});

			$( document ).on( 'change', '.urlDesk', function(e){

				var val = false;
				$( '.urlDesk' ).each( function(){
					if( $( this ).val().length > 0 ){
						val = true;
					}
				});

				if( val ){
					$( '#validSlide' ).val( 'ok' );
				}else{
					$( '#validSlide' ).val( null );
				}

			});

			$( document ).on( 'click', '.box-prev .remove', function(e){
				e.preventDefault();

				var $this 	= $( this );

				if( confirm( 'Você realmente deseja remover essa imagem?' ) ){

					$this.closest( '.box-prev' ).fadeOut( 400 );
					$this.closest( '.box-prev' ).prev( '.urlField' ).val( null ).change();
					$this.closest( '.box-prev' ).prev( '.urlField' ).prev( 'button' ).removeClass( 'has' ).text( 'Selecione a imagem' );

				}

			});	

		} // Document Ready
	});
})(jQuery);
