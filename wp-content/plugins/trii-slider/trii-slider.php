<?php
/*
Plugin Name: Trii - Slider
Plugin URI: 
Description: Criar sliders no padrão trii.
Author: Reinaldo Amorim
Version: 3.1
Author URI: http://reinaldoamorim.com.br

RELEASE NOTES:
  • 1.0 - 13/06/2017 - Versão Inicial
  • 2.0 - 06/09/2017 - Adicionado opção de link e corrigido bug de 1 slide no shortcode
  • 3.0 - 28/02/2019 - Adicionado opção de ordem dos slides - Crescente ou Decrescente
  • 3.1 - 14/03/2019 - Remoção do uso de jQuery nos scripts inline para melhor combatibilidade com o page speed insights
  • 3.2 - 28/01/2020 - Ajuste dos slides aparecendos cortados no responsivo quando o possuia mais de um slide
  • 3.3 - 09/03/2020 - troca do background da li para div para melhor otimização no pagespeed
*/

define( 'TRII_SLIDER_DIR', plugin_dir_path( __FILE__ ) );
define( 'TRII_SLIDER_URL', plugin_dir_url( __FILE__ ) );

require_once TRII_SLIDER_DIR . 'inc/incs.php';
