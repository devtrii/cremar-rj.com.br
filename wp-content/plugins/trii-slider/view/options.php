<?php
	global $post;

	$arSliders = array(
		1 => array(
			'desktop'	=> '',
			'tablet' 	=> '',
			'mobile' 	=> '',
			'link'		=> ''
		)
	);

	$ordem = "";

	if( $post->ID && $post->post_status != 'auto-draft' ){
		$arSliders = get_post_meta( $post->ID, 'trii_slide', true );
		$arSliders = unserialize( $arSliders );
		$ordem = get_post_meta( $post->ID, 'trii_slider_ordem', true );
	}
?>
<br>
	<label><b>Ordem do Slide</b></label>
	<select id="ordem_select" name="ordem">
		<option value="Crescente" <?php echo $ordem == 'Crescente'?'selected':'';?> >Crescente</option>
		<option value="Decrescente" <?php echo $ordem == 'Decrescente'?'selected':'';?> >Decrescente</option>
	</select>

<ul class="tabs">
	<?php
		$active = 'active';
		foreach ( $arSliders as $key => $slide ) {	?>	
		<li id="boxslide<?php echo $key; ?>" class="<?php echo $active; ?> tab" data-count="<?php echo $key; ?>">Slide <?php echo $key; ?></li>
	<?php if( !empty( $active ) ) $active = ''; }	?>
	<li class="addNewSlide">Adicionar</li>
	<li class="removeNewSlide">Remover</li>
</ul>
<input type="hidden" id="validSlide" value="<?php echo $post->ID ? 'ok' : ''; ?>" />
<div class="box-tabs">
	<?php 
		$active = 'style="display:block"';
		foreach ( $arSliders as $key => $slide ) { ?>
		<div class="box-op boxslide<?php echo $key; ?>" <?php echo $active; ?>>
	        <div class="box-link">
	        	<label>Link do Slide</label>
	        	<?php
					$value 	= !empty( $slide['link'] ) ? $slide['link'] : '';
	        	?>
	        	<input type="text" name="slide[<?php echo $key; ?>][link]" placeholder="Informe o link com HTTPS://" value="<?php echo $value ?>"/>
		    </div>
			
	        <div class="row">
	        	<div class="title">Slide - Versão Desktop ( <b>1920x</b> )</div>
	        	<?php
	        		$value 	= !empty( $slide['desktop'] ) ? $slide['desktop'] : '';
	        		$class 	= !empty( $value ) ? 'has' : '';
					$txt 	= !empty( $value ) ? 'Imagem Carregada' : 'Selecione a Imagem';
	        	?>
	        	<button type="button" class="btn-select-img <?php echo $class ?>" data-width="1920"><?php echo $txt; ?></button>
	        	<input type="hidden" name="slide[<?php echo $key; ?>][desktop]" class="urlField urlDesk" value="<?php echo $value ?>" />
	        	<div class="box-prev" <?php echo !empty( $value ) ? 'style="display:block"' : ''; ?>>
	        		<div class="remove" title="Remover Imagem">X</div>
	        		<?php
	        			if( !empty( $value ) ){
	        				$src = end( explode( '.', $value ) );
	        				$src = substr( $value, 0, -4 ) . '-150x150.' . $src;
	        			}else{
	        				$src = '';
	        			}
	        		?>
	        		<img class="prev" src="<?php echo $src; ?>" />
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="title">Slide - Versão Tablet ( <b>768x</b> )</div>
	        	<?php
	        		$value 	= !empty( $slide['tablet'] ) ? $slide['tablet'] : '';
	        		$class 	= !empty( $value ) ? 'has' : '';
	        		$txt 	= !empty( $value ) ? 'Imagem Carregada' : 'Selecione a Imagem';
	        	?>
	        	<button type="button" class="btn-select-img <?php echo $class ?>" data-width="768"><?php echo $txt; ?></button>
	        	<input type="hidden" name="slide[<?php echo $key; ?>][tablet]" class="urlField" value="<?php echo $value ?>" />
	        	<div class="box-prev" <?php echo !empty( $value ) ? 'style="display:block"' : ''; ?>>
	        		<div class="remove" title="Remover Imagem">X</div>
	        		<?php
	        			if( !empty( $value ) ){
	        				$src = end( explode( '.', $value ) );
	        				$src = substr( $value, 0, -4 ) . '-150x150.' . $src;
	        			}else{
	        				$src = '';
	        			}
	        		?>
	        		<img class="prev" src="<?php echo $src; ?>" />
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="title">Slide - Versão Mobile ( <b>475x</b> )</div>
	        	<?php
	        		$value 	= !empty( $slide['mobile'] ) ? $slide['mobile'] : '';
	        		$class 	= !empty( $value ) ? 'has' : '';
	        		$txt 	= !empty( $value ) ? 'Imagem Carregada' : 'Selecione a Imagem';
	        	?>
	        	<button type="button" class="btn-select-img <?php echo $class ?>" data-width="475"><?php echo $txt; ?></button>
	        	<input type="hidden" name="slide[<?php echo $key; ?>][mobile]" class="urlField" value="<?php echo $value ?>" />
	        	<div class="box-prev" <?php echo !empty( $value ) ? 'style="display:block"' : ''; ?>>
	        		<div class="remove" title="Remover Imagem">X</div>
	        		<?php
	        			if( !empty( $value ) ){
	        				$src = end( explode( '.', $value ) );
	        				$src = substr( $value, 0, -4 ) . '-150x150.' . $src;
	        			}else{
	        				$src = '';
	        			}
	        		?>
	        		<img class="prev" src="<?php echo $src; ?>" />
	        	</div>
	        </div>

		</div>
	<?php if( !empty( $active ) ) $active = ''; }	?>
</div>