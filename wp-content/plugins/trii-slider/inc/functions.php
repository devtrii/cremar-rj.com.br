<?php
	
	// Register Custom Post Type - Trii Slider
	function cpt_triislider() {

		$labels = array(
			'name'                => _x( 'Trii Slider', 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( 'Trii Slider', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( 'Trii Slider', 'text_domain' ),
			'name_admin_bar'      => __( 'Trii Slider', 'text_domain' ),
			'parent_item_colon'   => __( 'Slider pai:', 'text_domain' ),
			'all_items'           => __( 'Todos os sliders', 'text_domain' ),
			'add_new_item'        => __( 'Novo Slider', 'text_domain' ),
			'add_new'             => __( 'Adicionar Slider', 'text_domain' ),
			'new_item'            => __( 'Novo Slider', 'text_domain' ),
			'edit_item'           => __( 'Editar Slider', 'text_domain' ),
			'update_item'         => __( 'Atualizar Slider', 'text_domain' ),
			'view_item'           => __( 'Ver Slider', 'text_domain' ),
			'search_items'        => __( 'Buscar Slider', 'text_domain' ),
			'not_found'           => __( 'Não encontrado', 'text_domain' ),
			'not_found_in_trash'  => __( 'Não encontrado na Lixeira', 'text_domain' ),
		);
		$args = array(
			'label'               => __( 'Trii Slider', 'text_domain' ),
			'description'         => __( 'Trii Slider', 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array( 'title' ),
			'taxonomies'          => array(),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 50,
			'show_in_admin_bar'   => true,
			'menu_icon'			  => 'dashicons-desktop',
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,		
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);
		register_post_type( 'triislider', $args );

	}
	add_action( 'init', 'cpt_triislider', 0 );
	
	//Remove metabox do add post no cpt trii slider
	add_action( 'add_meta_boxes', 'triislide_remove_post_custom_fields', 100000  );
	function triislide_remove_post_custom_fields() {
		remove_meta_box( 'wpseo_meta', 'triislider', 'normal');
	}

	add_action( 'edit_form_after_title', 'triislider_add_fields' );
	function triislider_add_fields( $post ) {
		if ( $post->post_type != 'triislider' ) return;

		require_once TRII_SLIDER_DIR . 'view/options.php';
	}

	function triislide_register_scripsts_head() {
		global $typenow, $pagenow;
		if( !in_array($typenow, array('triislider') ) ) return;

		wp_deregister_script( 'wpseo-admin-global-script' ); 
    	wp_deregister_script( 'wpseo-admin-media' ); 
		wp_deregister_script( 'yoast-seo' );
		wp_deregister_script( 'wp-seo-metabox' );
		wp_deregister_script( 'wp-seo-post-scraper' ); 
		wp_deregister_script( 'wp-seo-replacevar-plugin' ); 
		wp_deregister_script( 'wp-seo-shortcode-plugin' ); 
		wp_deregister_script( 'yoast-seo-post-scraper' );
		wp_deregister_script( 'yoast-seo-term-scraper' );
		wp_deregister_script( 'yoast-seo-featured-image' );
		wp_deregister_script( 'wpseo-admin-media' );
		wp_deregister_script( 'll_main-js' );
		wp_deregister_script( 'll_jscolor' );

		wp_enqueue_script( 'triislide_main-js', TRII_SLIDER_URL . 'assets/js/painel.js', array( 'jquery' ), true );

		wp_enqueue_style( 'triislide_style', TRII_SLIDER_URL . 'assets/css/painel.css' );
	}
	add_action( 'admin_head', 'triislide_register_scripsts_head' );


	//Salvando no banco
	function triislide_save_post( $post_id, $post ) {
		global $typenow, $pagenow;
		if( !in_array($typenow, array('triislider') ) ) return;

		if (isset($post->post_status) && 'auto-draft' == $post->post_status) {
			return;
		}

		if( empty( $_POST['slide'] ) ){
			return;
		}

		$arSliders = array();
		$i = 1;
		foreach ( $_POST['slide'] as $slide ) {
			if( !empty( $slide['desktop'] ) ){
				$arSliders[$i] = $slide;
			}
			$i++;
		}

		update_post_meta( $post_id, 'trii_slide', serialize($arSliders) );
		update_post_meta( $post_id, 'trii_slider_ordem', $_POST['ordem'] );

	}
	add_action( 'save_post', 'triislide_save_post', 1, 2);

	function triislide_display_column( $column, $post_id ) {
	    if ($column == 'shortcode'){
	        echo '[triislide slide="'. $post_id .'" autoplay="Não"]';
	    }
	}

	function triislide_add_sticky_column( $columns ) {
	    return array_merge( $columns, 
	        array( 'shortcode' => 'Sortcode' ) );
	}

	add_action( 'admin_head', 'triislide_actions' );
	function triislide_actions(){
		global $typenow, $pagenow;
		if( in_array($typenow, array('triislider') ) ){
			add_action( 'manage_posts_custom_column' , 'triislide_display_column', 10, 2 );
			add_filter( 'manage_posts_columns' , 'triislide_add_sticky_column' );
		}
	}

	// SHORTCODE
    //========================================
        function triislide_shortcode( $atts ) {
            $rs = extract( shortcode_atts( array(
                'slide'         => '',
                'bullets'		=> '#000',
                'autoplay'		=> 'Não',
            ), $atts ));

            if( empty( $slide ) ){
            	global $wpdb;
            	$slide = $wpdb->get_var( "SELECT ID FROM wp_posts WHERE post_type = 'triislider' AND post_status = 'publish'" );
            }

            $css_class = ( function_exists('vc_map') ) 
                        ? apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts ) 
                        : false;

            $slides 	= unserialize( get_post_meta( $slide, 'trii_slide', true ) );
			$isSlide 	= sizeof( $slides ) > 1 ? 'isSlide' : '';

			$tamanhoD 	= getimagesize($slides[1]['desktop']);
			if( $slides[1]['tablet'] )
				$tamanhoT 	= getimagesize($slides[1]['tablet']);

			if( $slides[1]['mobile'] )
				$tamanhoM 	= getimagesize($slides[1]['mobile']);

			if( $autoplay == 'Sim' && !empty( $isSlide ) ){
				$html .= ' <div class="autoplay"></div>';//apenas cria a div para uso no js
			}

			$slider_ordem 	= get_post_meta( $slide, 'trii_slider_ordem', true );

			$html .= '<style type="text/css">
						.triislide .bullets li{
							border: 1px solid '. $bullets .';
						}

						.triislide .bullets li.active,
						.triislide .bullets li:hover{
							background: '.$bullets.'; 
						}

						.triislide,
						.triislide .container-triislide > li .box-bg{
							height: 100%;
						}

						.triislide,
						.triislide .container-triislide > li {
							height: '. $tamanhoD[1] .'px;
						}

						@media (max-width: 1200px){
							.triislide,
							.triislide .container-triislide > li{
								height: '. ( $tamanhoD[1] - 100 ) .'px;
							}
						}

						@media (max-width: 991px){
							.triislide,
							.triislide .container-triislide > li{
								height: '. ( $tamanhoD[1] - 200 ) .'px;
							}
						}

						@media (max-width: 768px){
							.triislide,
							.triislide .container-triislide > li{
								height: '. ( $tamanhoT[1] ) .'px;
							}
						}

						@media (max-width: 415px){
							.triislide,
							.triislide .container-triislide > li{
								height: '. ( $tamanhoM[1] ) .'px;
							}
						}
					</style>';
			$html .= '<div class="triislide '. $isSlide .'">
						<ul class="container-triislide">';

							if($slider_ordem == "Decrescente")
								krsort($slides);

							$slides_array = array_values($slides);

							foreach ( $slides_array as $n => $item )
							{
								$n++;

								$linkSlide = !empty( $item['link'] ) ? "<a href='". http_verifica( $item['link'] ) ."' target='_blank'></a>" : '';

								$html .= "<li class='slide{$n} slide desktop' ><div class='box-bg' style='background: url( {$item['desktop']} ) no-repeat top center;'> </div>{$linkSlide}</li>";

								if( !empty( $item['tablet'] ) ){
									$html .= "<li class='slide{$n} slide tablet' ><div class='box-bg' style='background: url( {$item['tablet']} ) no-repeat top center;'> </div>{$linkSlide}</li>";
								}

								if( !empty( $item['mobile'] ) ){
									$html .= "<li class='slide{$n} slide mobile'><div class='box-bg' style='background: url( {$item['mobile']} ) no-repeat top center;'> </div>{$linkSlide}</li>";
								}
							}

							//Bulets
							if( !empty( $isSlide ) ){
								$html .= '<ul class="bullets">';
								$active = 'active';
								foreach( range( 1, sizeof( $slides ) ) as $n ) {
									$html .= "<li class='{$active}' data-slide='{$n}'></li>";
									if( !empty( $active ) ) $active = '';
								}
								$html .= '</ul>';
							}

			$html .=	'</ul>
					  </div>';

            return $html;
        }
        add_shortcode( 'triislide', 'triislide_shortcode' );

    // SHORTCODE
    //========================================
        function vc_custom_triislide() {
            if ( function_exists('vc_map') ) {

            	global $wpdb;

                $atts = array(
                    'category' 	=> 'Custom',
                    'group' 	=> 'Configurações Gerais',
                );

                $sliders = $wpdb->get_results( "SELECT ID, post_title FROM wp_posts WHERE post_type = 'triislider' AND post_status = 'publish'" );

                $arValues = array();
                foreach( $sliders as $slide ) {
                	$arValues[$slide->post_title] = $slide->ID;
                }

                vc_map( array(
                    'name'          => 'Trii Slider',
                    'base'          => 'triislide',
                    'description'   => 'Slide configurado no menu Trii Slider',
                    'category'      => $atts['category'],
                    'params'        => array(
                        array(
                            'type'          => 'dropdown',
                            'heading'       => 'Slide',
                            'param_name'    => 'slide',
                            'value'         => $arValues,
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                        array(
                            'type'          => 'dropdown',
                            'heading'       => 'AutoPlay?',
                            'param_name'    => 'autoplay',
                            'value'         => array( 'Não', 'Sim' ),
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                        array(
                            'type'          => 'colorpicker',
                            'heading'       => 'Cor dos bollets ativos',
                            'param_name'    => 'bullets',
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                    )
                ) );
            }
        }
        add_action( 'vc_before_init', 'vc_custom_triislide' );

    add_action('wp_enqueue_scripts', 'triislide_enqueue_styles_scripts');
    function triislide_enqueue_styles_scripts() {
		wp_enqueue_style( 'triislide_front', TRII_SLIDER_URL . 'assets/css/front.css' );
		wp_enqueue_script( 'triislide_front-js', TRII_SLIDER_URL . 'assets/js/front.js', array( 'jquery' ), true );
    }

    function http_verifica( $link ){

		$link = trim( $link );
		$exp_hTTP = explode('/', $link);

		if( ( $link == '') || (empty($exp_hTTP[0])) )
		return '#';
		elseif( ($exp_hTTP[0] <> 'http:') && ($exp_hTTP[0] <> 'https:') ) 
		return 'http://' . $link;
			else
		return $link;

	}