<?php
/*
    *    Plugin Name: Clean Twice
    *    Author: Maia
    *    Description: Limpa os dados das tabelas que tem mais de 2 meses        
*/

// don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}


// Cria o intervalo de tempo
add_filter( 'cron_schedules', 'add_every_month' );
function add_every_month( $schedules ) {
    $schedules['every_month'] = array(
            'interval' => 2635200,
            'display'   => __( 'Every Month', 'textdomain' )
    );
    return $schedules;
}
// Agenda caso não esteja agendado
if ( ! wp_next_scheduled( 'add_every_month' ) ) {
    wp_schedule_event( time(), 'every_month', 'add_every_month' );
}

// Agenda a ação
add_action( 'add_every_month', 'every_month_event_func' );
function every_month_event_func() {
    global $wpdb;
    $day =  time();
    $myTable = $wpdb->prefix . "clean_records";
    $cf7Table = $wpdb->prefix . "cf7dbplugin_submits";
    $wsalTable = $wpdb->prefix . "wsal_metadata";
    $wsalIdsTable = $wpdb->prefix . "wsal_occurrences";
    $flamingoTable = $wpdb->prefix ."posts";
    $less2 =  strtotime(date("Y-m-d", $day) .' -2 months');

    
    // Se n tiver a tabela cria
    if ($wpdb->get_var('SHOW TABLES LIKE '.$myTable) != $myTable) {
        $sql = 'CREATE TABLE '.$myTable.'(
            id INT,
            data_ultima DECIMAL,
            PRIMARY KEY  (id))';
        if(!function_exists('dbDelta')) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        }
        dbDelta($sql);
    }    

    $ultimaLimpeza = $wpdb->get_results( "SELECT LAST (data_ultima) AS last_clean FROM $myTable"); 
    if(date("m", $day) > date("m", $ultimaLimpeza)){
        //Limpeza Audit    
        if($wpdb->get_var("SHOW TABLES LIKE '$wsalTable'") == $wsalTable){
            $results =  $wpdb->get_results( "SELECT id FROM  $wsalIdsTable WHERE created_on <  $less2") ; 
            $dates =  array_values ($results);
            $idsDatas = array();
            foreach($dates as $result){
                array_push($idsDatas, $result);
            }
            $filtroIds = implode(',', array_map('intval', $idsDatas));
            $resultsFiltred =  $wpdb->get_results( "SELECT * FROM  $wsalTable WHERE occurrence_id IN (".$filtroIds.")" ) ; 
            $toArr =  array_values ($resultsFiltred);
            $text = array();
            for ($i=0; $i < count($toArr) ; $i++) {   
                $text[$i] = $toArr[$i];
            }
            $content = "<pre>".print_r($text,true)."</pre>";
            $fp = fopen("audit".date("Y-m-d", $day).".txt","wb");
            fwrite($fp,$content);
            fclose($fp);   
            $wpdb->query("DELETE FROM $wsalTable WHERE occurrence_id IN (".$filtroIds.")" );
            $cleanWSAL = true;
            
        }    
        // Limpeza CF7
        if($wpdb->get_var("SHOW TABLES LIKE '$cf7Table'") == $cf7Table){
            $results =  $wpdb->get_results( "SELECT * FROM  $cf7Table WHERE  submit_time <  $less2" , ARRAY_N);
            $dates =  array_values ($results);
            $sla = array();
            for ($i=0; $i < count($dates) ; $i++) {   
                $sla[$i] =   $dates[$i];
            }
            $content = "<pre>".print_r($sla, true)."</pre>";
            $fp = fopen("cf7submitTime".date("Y-m-d", $day).".txt","wb");
            fwrite($fp,$content);
            fclose($fp);   
            $wpdb->query("DELETE FROM $cf7Table WHERE submit_time <  $less2" );
            $cleanCF7 =  true;
            
        } 
        
        // Limpeza Flamingo
        if($wpdb->get_var("SHOW TABLES LIKE '$flamingoTable'") == $flamingoTable){
            $results =  $wpdb->get_results( "SELECT * FROM  $flamingoTable WHERE post_type='flamingo_inbound' AND UNIX_TIMESTAMP(post_date) < $less2", ARRAY_N);
            $dates =  array_values ($results);
            $sla = array();
            for ($i=0; $i < count($dates) ; $i++) {   
                $sla[$i] =  $dates[$i];
            }
            $content = "<pre>".print_r($sla, true)."</pre>";
            $fp = fopen("flamingo".date("Y-m-d", $day).".txt","wb");
            fwrite($fp,$content);
            fclose($fp);   
            $wpdb->query("DELETE FROM $flamingoTable WHERE post_type = 'flamingo_inbound' AND UNIX_TIMESTAMP(post_date) < $less2" );
            $cleanFlamingo =  true;
            
        } 
        // Manda o email
        if($cleanCF7 ||  $cleanWSAL ||  $cleanFlamingo){
            $wpdb->query("INSERT INTO  $myTable(data_ultima) VALUES ($day)");
            $to = 'dev@trii.com.br';
            $subject = 'Sanitize DB - '.get_bloginfo( 'name' , 'raw' ).' - '.date("Y-m-d", $day).'';
            $message  = $cleanCF7 ? "<p>A tabela $cf7Table foi limpa.<br></p>" :  "<p>A tabela $cf7Table não foi limpa, ou ela não existe ou não foram encontrados nela dados com mais de 2 meses.<br></p>";
            $message  .= $cleanWSAL ? "<p>A tabela $wsalTable foi limpa.<br></p>" :  "<p>A tabela $wsalTable não foi limpa, ou ela não existe ou não foram encontrados nela dados com mais de 2 meses.<br></p>";
            $message  .= $cleanFlamingo ? "<p>A tabela $flamingoTable foi limpa.<br></p>" :  "<p>A tabela $flamingoTable não foi limpa, ou ela não existe ou não foram encontrados nela dados com mais de 2 meses.<br></p>";
            $message  .= "<p>Em anexo os logs que foram excluidos.<br></p>";
            $headers = "Content-Type: text/html; charset=UTF-8
            Content-Transfer-Encoding: 8bit";
            $attachments = array("audit".date("Y-m-d", $day).".txt","cf7submitTime".date("Y-m-d", $day).".txt" ,"flamingo".date("Y-m-d", $day).".txt");
            wp_mail( $to, $subject, $message, $headers, $attachments );
            unlink("audit".date("Y-m-d", $day).".txt");
            unlink("cf7submitTime".date("Y-m-d", $day).".txt");
            unlink("flamingo".date("Y-m-d", $day).".txt");
        }
    }

}


?>