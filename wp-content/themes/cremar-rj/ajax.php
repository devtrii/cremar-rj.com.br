<?php
add_action('wp_ajax_infinite_scroll', 'infinite_scroll');           
add_action('wp_ajax_nopriv_infinite_scroll', 'infinite_scroll');    

function infinite_scroll(){


    $args = array(
        'post_type' 		=> 'post',
        'posts_per_page'	=> 8,
        'paged' 			=> $_POST['paged'],
        'order'				=> 'id',
        'order_by'			=> 'ASC',
        'tag'               => $_POST['taxonomy']['tag'],
        'category_name'     => $_POST['taxonomy']['category']
    );

    $posts = new WP_Query( $args );
    $postObj ;


    if( $posts->have_posts() ){ 
        $response=[];
        while ( $posts->have_posts() ) { 
            $posts->the_post();
            
            $postOb= array(
                "img" => get_the_post_thumbnail_url(),
                "title" => get_the_title(),
                "excerpt" =>  get_excerpt( 156 ),
                "link" =>  get_permalink(),
            );
            array_push($response,$postOb);   
        }

        echo json_encode($response);

    }
    
    wp_die();
}