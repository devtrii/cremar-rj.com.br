<div class="sidebarBlog col-lg-3 col-xs-12">
    <span class="titulo recents-post-title">Posts Recentes</span>
	<ul class="recents-post">
		<?php
		$args = array(
            'post_type' 		=> 'post',
            'posts_per_page'	=> 3
		);

        $posts = new WP_Query( $args );
                
		if( $posts->have_posts() ){
			while ( $posts->have_posts() ) { 
                $posts->the_post(); ?>
				<li>
					<div class="txt">
						<span class="arTitulo"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></span>
					</div>
				</li>
			<?php }
		}
		?>
	</ul>

    <span class="titulo recents-post-title">Categorias</span>
	<ul class="recents-post">
		<?php
            $categorias = get_categories(array ('number'  => 5,  'orderby' => 'count', 'order'   => 'DESC'));
            
            foreach($categorias as $categoria) { ?>
				<li>
					<div class="txt">
						<span class="arTitulo"><a href="<?php echo get_category_link($categoria->term_id); ?>"><?php echo $categoria->name; ?></a></span>
					</div>
				</li>
			<?php }
        ?>
	</ul>

	<span class="titulo tags-title">Tags</span>
	<ul class="tags">
		<?php 
		$tags = get_tags(array ('number'  => 10,  'orderby' => 'count', 'order'   => 'DESC'));
        foreach ($tags as $tag) 
        {
            ?>
                <li><a href="<?php echo get_category_link( $tag->term_id ); ?>"><?php echo $tag->name; ?></a></li>
            <?php
        }
		?>
	</ul>
</div>