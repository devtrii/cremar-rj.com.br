<?php
	get_header();

	the_post();
?>

<div class="container">
    <div class="row">
        <?php $post_id = get_the_id();?>

        <div class="mainBlog col-lg-8 col-xs-12" id="row-single">
            <div class="blog-box single-post">
                <div class="item">
                    <div class="thumb"><?php echo get_the_post_thumbnail($post->ID, array( 770, 402) ); ?></div>

                    <div class="infos">
                        <div class="date">Publicado em<span> <?php the_date('d \d\e F \d\e Y'); ?><span></div>
                        <h1 class="title"><?php the_title(); ?></h1>
                        <?php if(get_field('ativar_sumario')){
                            echo do_shortcode("[sumario]");
                        } ?>
                        <div class="content"><?php the_content(); ?></div>
                    </div>

                </div>
            </div>
        </div>

        <div class="sidebarBlog col-lg-4 col-xs-12">
            <?php get_sidebar('blog'); ?>
        </div>
    </div>

    <section id="ultimas-novidades-blog" class="col-xs-12">


        <div class="container">
            <div class="title-box">
                <h3 class="title-style">
                    Aproveite para conferir mais artigos como este em nosso blog.
                </h3>
            </div>

            <div id="ultimas_novidades_container">
                <div class="sec_padrao" id="mesmas-categorias">
                    <?php
                           
				$args = array(
                    'category__in'   => wp_get_post_categories( $post_id ),
					'posts_per_page'	=> 12,
                    'post__not_in'   => array( $post_id)
				);

				$posts = new WP_Query($args);

				if($posts->have_posts()){

				?>
                    <div class="container_artigos owl-carousel owl-theme">

                        <?php
					$dados_artigos = array();
					$artigo_conjunto = array();

					while ($posts->have_posts()){ 

						$posts->the_post();
						$tag_categoria = get_the_category();

						//Buscando categoria
						foreach($tag_categoria as $categorias){
							$cat = $categorias->name;

						}

						array_push($dados_artigos, array(
									'img' => get_the_post_thumbnail_url(),
									'link' => get_permalink(),
									'categoria' => $cat,
									'titulo' => get_the_title(),
									'descricao' => get_excerpt(156)
						));
					}

					$artigo_conjunto = array_chunk($dados_artigos, 6);

					foreach($artigo_conjunto as $artigos){

						if($artigo_conjunto){
						?>

                        <div class="item grid">
                            <?php
								foreach($artigos as $artigo){
								?>

                            <a href="<?php echo $artigo['link']; ?>" class="bloco">

                                <img src="<?php echo $artigo['img']; ?>" alt="<?php echo $artigo['titulo']; ?>"
                                    class="ajustar_cover">

                                <div class="box_descricao">
                                    <span class="tag tag_geral"><?php echo $artigo['categoria']; ?></span>

                                    <span class="titulo_artigo"><?php echo $artigo['titulo']; ?></span>
                                </div>

                                <div class="box_hover_artigo box_geral">
                                    <h2 class="titulo_hover"><?php echo $artigo['titulo']; ?></h2>
                                    <p><?php echo $artigo['descricao']; ?></p>
                                </div>

                            </a>

                            <?php } ?>
                        </div>

                        <?php	}
					}?>
                    </div>
                    <?php
				}
				?>
                </div>
            </div>



        </div>

    </section>

    <a class="backToBlog" href="/blog"> <i class="fas fa-chevron-left"></i>Volte para a página inicial do blog</a>

</div>


<?php get_footer(); ?>