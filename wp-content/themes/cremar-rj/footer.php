		
		<div id="phoneBottom">
		<a href="tel:02141484107"><svg xmlns="http://www.w3.org/2000/svg" width="215.6" height="33">
                                      <g id="telefone" transform="translate(-20.203 8)">
                                        <text id="_21_4148-4107" data-name="(21) 4148-4107" transform="translate(58.804 18)" fill="#ffffff" font-size="25" font-family="Roboto-Bold, Roboto" font-weight="700"><tspan x="0" y="0">(21) 4148-4107</tspan></text>
                                        <g id="phone-call_2_" data-name="phone-call (2)" transform="translate(20.203 -4)">
                                          <g id="Grupo_161" data-name="Grupo 161" transform="translate(0 0)">
                                            <path id="Caminho_556" data-name="Caminho 556" d="M27.845,20.622,23.923,16.7a2.608,2.608,0,0,0-4.342.98A2.669,2.669,0,0,1,16.5,19.362c-2.8-.7-6.582-4.342-7.283-7.283A2.538,2.538,0,0,1,10.9,9a2.608,2.608,0,0,0,.98-4.342L7.958.735a2.8,2.8,0,0,0-3.781,0L1.515,3.4C-1.146,6.2,1.8,13.62,8.378,20.2s14.005,9.664,16.806,6.862L27.845,24.4A2.8,2.8,0,0,0,27.845,20.622Z" transform="translate(-0.539 0)" fill="#ffffff"/>
                                          </g>
                                        </g>
                                      </g>
                                    </svg></a>
		</div>

		<div id="footer">
		
			<div class="container">
				<div class="row">
					<div class="box box1 col-md-3 col-xs-12">
						<?php dynamic_sidebar( 'footer1' ); ?>
					</div>

					<div class="box box2 col-md-4 col-xs-12">
						<?php dynamic_sidebar( 'footer2' ); ?>
					</div>

					<div class="box box3 col-md-4 col-xs-12">
						<?php dynamic_sidebar( 'footer3' ); ?>
					</div>

				</div>	
			</div>

			<div class="copy">
				<div class="container">
					<div class="lado">
						Cremar © Todos os direitos reservados.
						<a href="https://trii.com.br" target="_blank"><img src="<?php echo get_template_directory_uri();?>/assets/images/logo-trii.png" title="Agência Digital no RJ" alt="Agência Digital no RJ"/></a>
					</div>
				</div>
			</div>
		</div>

		<div class="wp_footer">
			<?php wp_footer(); ?>
		</div>
		<!-- Colocar Fonte Aqui -->
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css2?family=Marcellus&display=swap" rel="stylesheet">
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

	</body>
</html>