<?php
	//Template Name: Blog
	get_header();
?>

<?php

$blogServices = new BlogServices();


?>
<main class="container" id="blog-archive">
    <section id="pesquisa-blog-categorias">
        <div class="title-box">
            <h2 class="title-style">Encontre artigos especiais para você</h2>
        </div>

        <div class="form-pesquisa-categoria">

            <form>
                <div class="select">
                    <select name="categories">
                        <option value="" disabled selected>Selecione uma categoria</option>
                        <?php echo $blogServices->get_blog_categories() ?>
                    </select>
                    <i class="fas fa-chevron-down"></i>
                </div>
                <div class="button">
                    <a href="#pesquisa-blog-categorias">Pesquisar</a>
                </div>
            </form>

        </div>
    </section>

    <section id="ultimas-novidades-blog">
        <div class="title-box">
            <h2 class="title-style">
                Confira as últimas novidades do nosso blog:
            </h2>
        </div>


        <div class="container">
            <div id="ultimas_novidades_container">
                <?php
				$paged = get_query_var('paged') ? get_query_var('paged') : 1;

				$args = array(
					'post_type' 		=> 'post',
					'posts_per_page'	=> 12,
					'paged' 			=> $paged
				);

				$posts = new WP_Query($args);

				if($posts->have_posts()){ ?>

                <div class="container_artigos owl-carousel owl-theme">

                    <?php
					$dados_artigos = array();
					$artigo_conjunto = array();

					while ($posts->have_posts()){ 

						$posts->the_post();
						$tag_categoria = get_the_category();

						//Buscando categoria
						foreach($tag_categoria as $categorias){
							$cat = $categorias->name;
						}
						array_push($dados_artigos, array(
							'img' => get_the_post_thumbnail_url(),
							'link' => get_permalink(),
							'categoria' => $cat,
							'titulo' => get_the_title(),
							'descricao' => get_excerpt(156)
						));
					}

					$artigo_conjunto = array_chunk($dados_artigos, 6);

					foreach($artigo_conjunto as $artigos){

						if($artigo_conjunto){?>

                    <div class="item grid">
                        <?php foreach($artigos as $artigo){ ?>

                        <a href="<?php echo $artigo['link']; ?>" class="bloco">

                            <img src="<?php echo $artigo['img']; ?>" alt="<?php echo $artigo['titulo']; ?>"
                                class="ajustar_cover">

                            <div class="box_descricao">
                                <span class="tag tag_geral"><?php echo $artigo['categoria']; ?></span>

                                <span class="titulo_artigo"><?php echo $artigo['titulo']; ?></span>
                            </div>

                            <div class="box_hover_artigo box_geral">
                                <h2 class="titulo_hover"><?php echo $artigo['titulo']; ?></h2>
                                <p><?php echo $artigo['descricao']; ?></p>
                            </div>

                        </a>

                        <?php } ?>
                    </div>

                    <?php	}
					}?>
                </div>
                <?php
				}
				?>
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>