	<!-- SIDEBAR -->
	<?php
	$blogServices = new BlogServices();
	
	?>
	<section id="pesquisa-blog-single-categorias">

	    <div class="form-pesquisa-categoria">

	        <form>
	            <div class="select">
	                <select name="categories">
	                    <option value="" disabled selected>Selecione uma categoria</option>
	                    <?php echo $blogServices->get_blog_categories() ?>
	                </select>
	                <i class="fas fa-chevron-down"></i>
	            </div>
	            <div class="button">
	                <a href="#pesquisa-blog-categorias">Pesquisar</a>
	            </div>
	        </form>

	    </div>
	</section>

	<section class="veja-tambem">
	    <span class="titulo">Veja também:</span>
	    <ul class="recents-post">
	        <?php
			$args = array(
				'post_type' 		=> 'post',
				'posts_per_page'	=> 3
			);

			$posts = new WP_Query( $args );
			if( $posts->have_posts() ){
				while ( $posts->have_posts() ) { $posts->the_post(); ?>
	        <li>
	            <a href="<?php echo get_permalink(); ?>">
	                <span class="thumb"><?php echo get_the_post_thumbnail($post->ID, array(66, 66)); ?></span>
	                <span class="title"><?php the_title(); ?></span>
	            </a>
	        </li>
	        <?php }
			}
		?>
	    </ul>

	</section>

	<section class="tags">

	    <span>Tags</span>
	    <ul>
	        <?php 
		$tag_options = array ('number'  => 10,  'orderby' => 'count', 'order'   => 'DESC');		
		$tags = get_tags($tag_options);

		foreach ($tags as $tag) {?>

	        <li><a href="<?php echo get_category_link( $tag->term_id ); ?>"><?php echo $tag->name; ?></a></li>

	        <?php }
		?>
	    </ul>

	</section>