(function ($) {
  $(document).on("click", ".faq .title", function (e) {
    var $this = $(this),
      $faqs = $(".faq"),
      $faq = $this.parent(),
      $content = $faq.find(".content");
    $(".active").removeClass("active");

    if (!$faq.hasClass("active")) {
      $faq.addClass("active");
    }
  });

  $(document).ready(function () {
    var item = $(".faq:first");
    console.log(item);
    item.addClass("active");
  });
})(jQuery, this);
