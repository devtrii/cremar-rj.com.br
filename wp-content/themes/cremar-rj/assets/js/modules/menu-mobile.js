(function ($, window) {
  jQuery(document).ready(function () {
    const menuMobile = $(".menu-mobile");
    const fecharMenu = $(".box-fechar");
    const boxMobile = $(".box-mobile");

    function abrirMenu() {
      menuMobile.on("click", function () {
        $("hmtl, body").css({
          height: $(window).height() + "px",
          width: $(window).width() + "px",
          overflow: "hidden",
        });

        boxMobile.show();
        boxMobile.animate(
          {
            height: "100vh",
            opacity: "0.95",
          },
          350
        );

        function removeTop() {
          console.log(boxMobile.find("li:not(.active)").length);

          if (boxMobile.find("li:not(.active)").length > 0) {
            boxMobile.find("li:not(.active)").first().animate(
              {
                marginTop: "0",
                opacity: "1",
              },
              700
            );
            boxMobile.find("li:not(.active)").first().addClass("active");
            setTimeout(removeTop, 100);
          } //if
        } //removeTop
        setTimeout(removeTop, 200);
      });
    } //abrir menu

    function fechaMenu() {
      fecharMenu.on("click", function () {
        $("hmtl, body").css({
          height: "auto",
          width: "auto",
          overflow: "auto",
        });
        boxMobile.animate(
          {
            height: "0",
            opacity: "0",
          },
          500,
          () => {
            boxMobile.hide();
            boxMobile.find("li.active").removeClass("active").css({
              opacity: "0",
              marginTop: "-80px",
            });
          }
        );
      });
    }

    $(".menu-item-has-children ").on("click", function () {
      if ($(this).find(".sub-menu").hasClass("active")) {
        $(this).find(".sub-menu").removeClass("active");
        $(this).find(".sub-menu").hide(300);
      } else {
        $(this).find(".sub-menu").addClass("active");
        $(this).find(".sub-menu").show(300);
      }
    });

    abrirMenu();
    fechaMenu();


    
  }); //jquery ready

  $(document).scroll(function() { 
    if ($(this).scrollTop() > 80) {
        $(".cta-header").addClass("fixo");
    } else {
        $(".cta-header").removeClass("fixo");
    }
});
})(jQuery, this);
