(function ($, window) {
  $(document).on("click", ".goToByScroll", function (e) {
    e.preventDefault();
    var id = $(this).attr("href");

    if (!id) {
      var id = $(this).find("a").attr("href");
    }

    if (id.startsWith("#")) id = id.replace("#", "");

    goToByScroll(id);
  });

  function goToByScroll(id) {
    $("html,body").animate({ scrollTop: $("#" + id).offset().top }, "slow");
  }
})(jQuery, this);
