(function ($, window) {
  jQuery(document).ready(function () {
    function handleWhatsPlugin() {
      let whats_plugin = $("#qlwapp .qlwapp-toggle");

      whats_plugin.addClass("animated_plugin bounce_plugin");
    }
    handleWhatsPlugin();

    var resolucao = screen.availWidth;

    //Se dar foco em qualquer input o zap irá sumir - Apenas mobile
    $(document).on("focus", "input, select, textarea", function () {
      if (resolucao <= 991) {
        $("#qlwapp .qlwapp-toggle").hide();
      }
    });

    //Mostrar zap ao perder foco
    $(document).on("blur", "input, select, textarea", function () {
      if (resolucao <= 991) {
        $("#qlwapp .qlwapp-toggle").show();
      }
    });
  });
})(jQuery, this);
