(function ($) {
  $(document).on("change", 'input[name="curriculo"]', function (e) {
    var $this = jQuery(this);

    if ($this.val() == "" || $this.val() == undefined) {
      jQuery(".beforeCurriculo").remove();
    } else {
      var tipo = $this.val().split(".").pop(),
        tamanho = this.files[0].size;

      if (tipo != "doc" && tipo != "docx" && tipo != "pdf") {
        alert("Tipo de arquivo inválido.");
        $this.val(null);
        $("form .curriculo label").html(
          "<i class='fa fa-times' aria-hidden='true'></i>Arquivo inválido!"
        );
        $("form .curriculo label").addClass("hasError");
      } else if (tamanho > 10000000) {
        alert("Você só pode anexar um arquivo com no máximo 10MB.");
        $this.val(null);

        $("form .curriculo label").html(
          "<i class='fa fa-times' aria-hidden='true'></i>Arquivo muito grande!"
        );
        $("form .curriculo label").addClass("hasError");
      } else {
        $("form .curriculo label").removeClass("hasError");
        $("form .curriculo label").html(
          "<i class='fa fa-check' aria-hidden='true'></i> Arquivo anexado!"
        );
        $("form .curriculo label").addClass("anexado");
      }
    }
  });

  /**
   *
   * Função responsável por fazer labels flutuantes
   *
   */

  $(document).on("ready", function () {
    //Label flutua
    $(".field input, .field textarea, .field select").focusin(function () {
      var $this = $(this),
        $field = $this.parents(".field");

      if (!$field.hasClass("active")) {
        $field.addClass("active");
      }
    });

    $(".field input, .field textarea, .field select").focusout(function () {
      var $this = $(this),
        $field = $this.parents(".field");

      if ($this.val().length == 0) {
        $field.removeClass("active");
      }
    });
  });

  //responsável por mostrar evento de sucesso no form

  document.addEventListener(
    "wpcf7mailsent",
    function (event) {
      Swal.fire(
        "Seu contato foi enviado com sucesso!",
        "Responderemos sua mensagem em breve",
        "success"
      );
      // Impedir que tenhar meta se enviar apartir do trabalhe conosco
      const buttonCurriculo = $('input[name="curriculo"]');
      if (buttonCurriculo.length === 0) {
        ga("send", "event", "formulario", "contato-sucesso");
        fbq("trackCustom", "contatoSucesso");
      }
    },
    false
  );

  document.addEventListener(
    "wpcf7mailfailed",
    function (event) {
      Swal.fire({
        title: "Erro!",
        text:
          "Não foi possível enviar sua mensagem, tente novamente em alguns instantes",
        icon: "error",
        confirmButtonText: "Ok",
      });
    },
    false
  );
})(jQuery, this);
