(function ($) {
  function transformaLastPostInCarousel() {
    const resolucao = $(window).width();
    const sidebar = $(".sidebarBlog .veja-tambem ul ");

    if (resolucao <= 991) {
      sidebar.addClass("owl-carousel owl-theme");

      sidebar.owlCarousel({
        items: 1,
        loop: true,
        autoPlay: true,
      });
    }
  }

  $(document).on("ready", function () {
    transformaLastPostInCarousel();

    /**
     *
     * SELECIONANDO ELEMENTOS
     *
     */
    const selectCategoriasArchive = $(
      "#pesquisa-blog-categorias .form-pesquisa-categoria select"
    );

    const selectCategoriasSingle = $(
      "#pesquisa-blog-single-categorias .form-pesquisa-categoria select"
    );

    /**
     *
     * MUDAR O LINK BOTÃO QUANDO DER CHANGE NO INPUT ARCHIVE
     *
     */
    selectCategoriasArchive.on("change", function () {
      const linkCategoria = $(
        "#pesquisa-blog-categorias .form-pesquisa-categoria a"
      );

      const selectValue = selectCategoriasArchive.val();
      linkCategoria.attr("href", selectValue);
    });

    /**
     *
     * MUDAR O LINK BOTÃO QUANDO DER CHANGE NO INPUT SINGLE
     *
     */

    selectCategoriasSingle.on("change", function () {
      const linkCategoriaSingle = $(
        "#pesquisa-blog-single-categorias .form-pesquisa-categoria a"
      );

      const selectCategoriasValueSingle = selectCategoriasSingle.val();
      linkCategoriaSingle.attr("href", selectCategoriasValueSingle);
    });

    /**
     *
     *CHAMANDO O OWL GERAL NO CONTAINER ARTIGOS
     *
     */

    $("#ultimas_novidades_container .container_artigos").owlCarousel({
      loop: true,
      autoPlay: false,
      items: 1,
    });

    $("#ultimas-novidades-blog .owl-carousel").owlCarousel({
      loop: true,
      autoPlay: false,
      items: 1,
    });

    /**
     * Ser der resize chama a função
     */
    $(window).resize(function () {
      transformaLastPostInCarousel();
    });
  });

  let count = 2;
  let userCanScroll = true;
  var _top = $(window).scrollTop();
  var _direction;
  $(window).scroll(function () {
    var _cur_top = $(window).scrollTop();

    if (_top < _cur_top) {
      const footer = document.querySelector("#footer");
      const footerPosition = footer.getBoundingClientRect();
      if ($("body.page-template-template-blog")) {
        if (footerPosition.top <= 820) {
          if (userCanScroll) {
            loadArticle(count);
            count++;
          }
        }
      }

      function loadArticle(pageNumber) {
        $(".skeleton").css({
          display: "grid",
        });
        userCanScroll = false;

        let category;
        let tag;
        const taxonomy = window.location.pathname.split("/");

        if (taxonomy[1] === "category") {
          category = window.location.pathname.split("/")[2];
        }
        if (taxonomy[1] === "tag") {
          tag = window.location.pathname.split("/")[2];
        }

        console.log(category);
        console.log(tag);
        $.ajax({
          url: ajaxurl,
          type: "POST",
          data: {
            action: "infinite_scroll",
            paged: pageNumber,
            taxonomy: { tag, category },
          },
          success: function (response) {
            if (response.length > 1) {
              const postObjs = JSON.parse(response);

              let html = '<br/><div class="item grid">';

              postObjs.map((post) => {
                html += `
                <a href="${post.link}" class="bloco">
                  ${
                    post.img
                      ? `<img src="${post.img}" title="${post.title}" class="ajustar_cover"/>`
                      : `<div class="no-img"><i class="fas fa-picture-o" aria-hidden="true"></i></div>`
                  }        
                  <div class="box_descricao">
                    <span class="titulo_artigo">${post.title}</span>
                  </div>

                  <div class="box_hover_artigo box_geral">
                    <h2 class="titulo_hover">${post.title}</h2>
                    <p>${post.excerpt}</p>
                  </div>
                </a>
              `;
              });

              html += "</div>";
              $("#row_category .container_categ ").append(html);
              $("#row_tag .container_categ ").append(html);

              userCanScroll = true;
            } else {
              userCanScroll = false;
              // $(".mainBlog").append(
              //   "<div class='alert alert-info' role='alert'>Todos nosso posts foram carregados!</div>"
              // );
            }

            $(".skeleton").css({
              display: "none",
            });
          },
        });
        return false;
      }
    } else {
      // direction up
    }
    _top = _cur_top;
  });
})(jQuery, this);
