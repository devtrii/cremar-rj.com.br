<?php
	get_header();

	$tag_id = get_queried_object()->term_id;

?>

<div class="container">

    <div class="row">
        <div class="col-lg-8 col-xs-12" id="row_tag">
            <div class="container_categ">
                <div class="item grid">
                    <?php
					$paged = get_query_var('paged') ? get_query_var('paged') : 1;
					$args = array(
						'post_type' 		=> 'post',
						'posts_per_page'	=> 8,
						'paged' 			=> 1,
						'tag_id' 			=> $tag_id,
						'order'				=> 'id',
						'order_by'			=> 'ASC'
					);

					$posts = new WP_Query($args);

					if($posts->have_posts()){
						while ($posts->have_posts()){
							$posts->the_post();

							?>
                    <a href="<?php echo get_permalink(); ?>" class="bloco">
                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_the_title(); ?>"
                            class="ajustar_cover">

                        <div class="box_descricao">
                            <span class="titulo_artigo"><?php echo get_the_title(); ?></span>
                        </div>
                        <div class="box_hover_artigo box_geral">
                            <h2 class="titulo_hover"><?php echo get_the_title(); ?></h2>
                            <p><?php echo get_excerpt(156); ?></p>
                        </div>
                    </a>
                    <?php
							}
						}
					?>
                </div>


            </div>
            <div class="skeleton">
                <div class="loading">
                </div>
                <div class="loading">
                </div>
                <div class="loading">
                </div>
                <div class="loading">
                </div>
            </div>
        </div>

        <div class="sidebarBlog col-lg-4 col-xs-12">
            <?php get_sidebar('blog'); ?>

        </div>


    </div>
    <a class="backToBlog" href="/blog"> <i class="fas fa-chevron-left"></i>Volte para a página inicial do blog</a>


</div>

<?php get_footer(); ?>