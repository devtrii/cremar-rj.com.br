// const gulp = require("gulp");
// const sourcemaps = require("gulp-sourcemaps");
// const sass = require("gulp-sass");
// const autoPrefixer = require("gulp-autoprefixer");
// const concat = require("gulp-concat");
// const babel = require("gulp-babel");
// const uglify = require("gulp-uglify-es").default;
// const browserSync = require("browser-sync").create();

// function compilaSass() {
//   return gulp
//     .src("./assets/scss/*.scss")
//     .pipe(sourcemaps.init())
//     .pipe(
//       sass({
//         outputStyle: "compressed",
//       })
//     )
//     .pipe(
//       autoPrefixer({
//         browesers: ["last 2 versions"],
//         cascade: false,
//       })
//     )
//     .pipe(sourcemaps.write("./"))
//     .pipe(gulp.dest("./assets/css"));
//   // .pipe(browserSync.stream());
// }
// gulp.task("sass", compilaSass);

// function gulpJs() {
//   return gulp
//     .src("./assets/js/modules/*.js")
//     .pipe(concat("main.js"))
//     .pipe(
//       babel({
//         presets: ["@babel/env"],
//       })
//     )
//     .pipe(uglify())
//     .pipe(gulp.dest("./assets/js/"));
// }
// gulp.task("js", gulpJs);

// function browser() {
//   browserSync.init({
//     server: {
//       proxy: "http://pacotetrii.local.com.br",
//     },
//   });
// }
// gulp.task("browserSync", browser);

// function watch() {
//   gulp.watch("./assets/scss/*.scss", compilaSass);
//   gulp.watch("./assets/js/modules/*.js", gulpJs);
//   gulp.watch(["./*.php", "./**/*.php"]).on("change", browserSync.reload());
// }
// gulp.task("watch", watch);

// gulp.task("default", gulp.parallel("watch", "sass", "js"));

// Adiciona os modulos instalados
const gulp = require("gulp");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const browserSync = require("browser-sync").create();
const concat = require("gulp-concat");
const babel = require("gulp-babel");
const uglify = require("gulp-uglify-es").default;

// Funçao para compilar o SASS e adicionar os prefixos
function compilaSass() {
  return gulp
    .src("./assets/scss/*.scss")
    .pipe(
      sass({
        outputStyle: "compressed",
      })
    )
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 2 versions"],
        cascade: false,
      })
    )
    .pipe(gulp.dest("./assets/css/"))
    .pipe(browserSync.stream());
}

// Tarefa de gulp para a função de SASS
gulp.task("sass", function (done) {
  compilaSass();
  done();
});

// Função para juntar o JS
function gulpJS() {
  return gulp
    .src("./assets/js/modules/*.js")
    .pipe(concat("main.js"))
    .pipe(
      babel({
        presets: ["@babel/env"],
      })
    )
    .pipe(uglify())
    .pipe(gulp.dest("./assets/js/"))
    .pipe(browserSync.stream());
}

gulp.task("mainjs", gulpJS);

// JS Plugins
function pluginJS() {
  return gulp
    .src(["./assets/js/plugins/*.js"])
    .pipe(concat("plugins.js"))
    .pipe(gulp.dest("assets/js/"))
    .pipe(browserSync.stream());
}

gulp.task("pluginjs", pluginJS);

// Função para iniciar o browser
function browser() {
  browserSync.init({
    proxy: "cremar-rj.local.com.br",
  });
}

// Tarefa para iniciar o browser-sync
gulp.task("browser-sync", browser);

// Função de watch do Gulp
function watch() {
  gulp.watch("assets/scss/*.scss", compilaSass);
  gulp.watch("assets/js/modules/*.js", gulpJS);
  gulp.watch("assets/js/plugins/*.js", pluginJS);
  gulp.watch(["*.php", "./**/*.php"]).on("change", browserSync.reload);
}

// Inicia a tarefa de watch
gulp.task("watch", watch);

// Tarefa padrão do Gulp, que inicia o watch e o browser-sync
gulp.task(
  "default",
  gulp.parallel("watch", "browser-sync", "sass", "mainjs", "pluginjs")
);
