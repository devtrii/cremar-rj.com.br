<?php get_header(); ?>

	<main class="container">
		<?php 
			if ( have_posts() ) {
				while ( have_posts() ) : the_post();
					the_content();
				endwhile;
			} else {
				echo 'Nada encontrado';
			}
		?>
	</main>

<?php get_footer(); ?>