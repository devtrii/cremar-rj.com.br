<?php

    // SHORTCODE
    //========================================
        function faq_shortcode( $atts ) {
            $rs = extract( shortcode_atts( array(
                'question'      => 'Pergunta Exemplo',
                'answer'        => 'Resposta Exemplo',
                'class'         => 'faq-style',
                'css'           => ''
            ), $atts ));

            $css_class = ( function_exists('vc_map') ) 
                        ? apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts ) 
                        : false;

            $html = "<div itemscope itemprop='mainEntity' itemtype='https://schema.org/Question'>";

            $html .= "<div class='faq {$class}{$css_class}'>";
            $html .= "<div class='title' itemprop='name'>{$question}</div>";
            $html .= "<div itemscope itemprop='acceptedAnswer' itemtype='https://schema.org/Answer'>";
            $html .= "<div class='content' itemprop='text'><div class='title_resp' >{$question}</div>". rawurldecode( base64_decode( strip_tags( $answer ) ) ) ."</div>";
            
            $html .= "</div></div></div>";
            

            return $html;
        }
        add_shortcode( 'faq', 'faq_shortcode' );

    // MAP
    //========================================
        function vc_custom_faq() {
            if ( function_exists('vc_map') ) {
                $atts = array(
                    'category' => 'Custom',
                    'group' => 'Configurações Gerais',
                );

                vc_map( array(
                    'name'          => 'Faq',
                    'base'          => 'faq',
                    'description'   => 'Display questions and answers',
                    'category'      => $atts['category'],
                    'params'        => array(
                        array(
                            'type'          => 'textfield',
                            'heading'       => 'Pergunta',
                            'param_name'    => 'question',
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                        array(
                            'type'          => 'textarea_raw_html',
                            'heading'       => 'Resposta',
                            'param_name'    => 'answer',
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                        array(
                            'type'          => 'textfield',
                            'heading'       => 'Classe',
                            'param_name'    => 'class',
                            'value'         => 'faq-style',
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                        array(
                            'type'          => 'css_editor',
                            'heading'       => 'Css box',
                            'param_name'    => 'css',
                            'group'         => 'Opções de Design'
                        )
                    )
                ) );
            }
        }
        add_action( 'vc_before_init', 'vc_custom_faq' );