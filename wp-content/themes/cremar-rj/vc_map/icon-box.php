<?php

    // SHORTCODE
    //========================================
        function icon_box_shortcode( $atts ) {
            $rs = extract( shortcode_atts( array(
                'icon'          => '',
                'title'         => '',
                'link'          => '',
                'class'         => 'icon-box-style',
                'css'           => ''
            ), $atts ));

            $css_class = ( function_exists('vc_map') ) 
                        ? apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts ) 
                        : false;

            $tag = ( $link ) ? 'a' : 'div';
            $href = ( $link ) ? ' href="'. $link .'"' : '';

            $icon_src = wp_get_attachment_url( $icon );
            $icon_alt = get_post_meta( $icon, '_wp_attachment_image_alt', true);

            $html = "<{$tag} {$href} class='icon-box {$class}{$css_class}'>";
            $html .= "<div class='icon'><img src='" . $icon_src . "' alt='". $icon_alt ."'></div>";
            $html .= "<span class='title'>". rawurldecode( base64_decode( strip_tags( $title ) ) ) ."</span>";
            $html .= "</{$tag}>";

            return $html;
        }
        add_shortcode( 'icon_box', 'icon_box_shortcode' );

    // MAP
    //========================================
        function vc_custom_icon_box() {
            if ( function_exists('vc_map') ) {
                $atts = array(
                    'category' => 'Custom',
                    'group' => 'Configurações Gerais',
                );

                vc_map( array(
                    'name'          => 'Box Icon',
                    'base'          => 'icon_box',
                    'description'   => 'Display box with icons',
                    'category'      => $atts['category'],
                    'params'        => array(
                        array(
                            'type'          => 'attach_image',
                            'heading'       => 'Ícone',
                            'param_name'    => 'icon',
                            'description'   => 'Lembre-se de selecionar imagens com fundo transparente. (PNG)',
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                        array(
                            'type'          => 'textarea_raw_html',
                            'heading'       => 'Título',
                            'param_name'    => 'title',
                            'description'   => 'Adicionar título para o bloco',
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                        array(
                            'type'          => 'textfield',
                            'heading'       => 'Link',
                            'param_name'    => 'link',
                            'description'   => 'Caso deseja adicionar link, basta preencher este campo.',
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                        array(
                            'type'          => 'textfield',
                            'heading'       => 'Classe',
                            'param_name'    => 'class',
                            'value'         => 'icon-box-style',
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                        array(
                            'type'          => 'css_editor',
                            'heading'       => 'Css box',
                            'param_name'    => 'css',
                            'group'         => 'Opções de Design'
                        )
                    )
                ) );
            }
        }
        add_action( 'vc_before_init', 'vc_custom_icon_box' );