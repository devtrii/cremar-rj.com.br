<?php

	// SHORTCODE
	//========================================
		function blog_shortcode( $atts ) {
			$rs = extract( shortcode_atts( array(
				'post_type'		=> '',
				'post_qtd'		=> '',
				'post_size'		=> '',
				'thumb'			=> '',
				'title'			=> '',
				'date'			=> '',
				'description'	=> '',
				'read_more'		=> '',
				'class'			=> 'post-style',
				'css'			=> ''
			), $atts ));

			$css_class = ( function_exists('vc_map') ) 
						? apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts ) 
						: false;

			$img = wp_get_attachment_image_src($slides, 'full');

			$query = new WP_Query(array(
				'post_type'			=> $post_type,
				'posts_per_page'	=> $post_qtd
			));
			
			if ( $query->have_posts() ) {
				$html .= '<div class="blog-box">';

				while ( $query->have_posts() ) { $query->the_post();
					// link
					$html .= '<a href="'. get_permalink() .'" class="item">';

					// thumb
					if ( $thumb ) {
						if ( has_post_thumbnail() ) {
							$img = wpb_getImageBySize( array(
								'attach_id' => get_post_thumbnail_id( get_the_ID() ),
								'thumb_size' => $post_size,
								'class' => 'vc_single_image-img',
							) );
							$handle = curl_init($img['p_img_large'][0]);
								curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

								/* Get the HTML or whatever is linked in $url. */
								$response = curl_exec($handle);

								/* Check for 404 (file not found). */
								$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
								if($httpCode != 200) {
									$img['thumbnail'] = '<div class="no-img"><i class="fa fa-picture-o" aria-hidden="true"></i></div>';
								}

								curl_close($handle);

								/* Handle $response here. */
						} else {
							
							$img['thumbnail'] = '<div class="no-img"><i class="fa fa-picture-o" aria-hidden="true"></i></div>';
						}

						$html .= '<div class="thumb">'. $img['thumbnail'] .'</div>';
					}
					
					// title
					if ( $title ) {
						$html .= '<h3 class="title">'. get_the_title() .'</h3>';
					}

					// date
					if ( $date ) {
						$html .= '<div class="date">'. get_the_date("d M") .'</div>';
					}

					$html .= '<div class="infos">';

					// description
					if ( $description ) {
						$html .= '<div class="description">'. get_excerpt( 185 ) .'</div>';
					}

					// read more
					if ( $read_more ) {
						$html .= '<div class="button"><button>Ler mais</button></div>';
					}

					$html .= '</div>';

					// end link
					$html .= '</a>';
				}

				$html .= '</div>';
				wp_reset_postdata();
			}


			return $html;
		}
		add_shortcode( 'blog', 'blog_shortcode' );

	// MAP
	//========================================
		function vc_custom_post() {
			if ( function_exists('vc_map') ) {
				$atts = array(
					'category' => 'Custom',
					'group' => 'Configurações Gerais',
				);

				vc_map( array(
					'name'          => 'Blog',
					'base'          => 'blog',
					'description'   => 'Display posts',
					'category'      => $atts['category'],
					'params'        => array(
						array(
							'type'          => 'posttypes',
							'heading'       => 'Tipo de Post',
							'param_name'    => 'post_type',
							'group'         => $atts['group'],
							'admin_label'   => true
						),
						array(
							'type'          => 'textfield',
							'heading'       => 'Quantidade de Posts',
							'param_name'    => 'post_qtd',
							'group'         => $atts['group'],
							'admin_label'   => true
						),
						array(
							'type'          => 'textfield',
							'heading'       => 'Tamanho de corte do thumbnail',
							'param_name'    => 'post_size',
							'description'	=> 'Ex: 370x192',
							'group'         => $atts['group'],
							'admin_label'   => true
						),
						array(
							'type'          => 'checkbox',
							'heading'       => 'Mostrar Thumbnail',
							'param_name'    => 'thumb',
							'group'         => $atts['group'],
							'admin_label'   => true
						),
						array(
							'type'          => 'checkbox',
							'heading'       => 'Mostrar Título',
							'param_name'    => 'title',
							'group'         => $atts['group'],
							'admin_label'   => true
						),
						array(
							'type'          => 'checkbox',
							'heading'       => 'Mostrar Data',
							'param_name'    => 'date',
							'group'         => $atts['group'],
							'admin_label'   => true
						),
						array(
							'type'          => 'checkbox',
							'heading'       => 'Mostrar Descrição',
							'param_name'    => 'description',
							'group'         => $atts['group'],
							'admin_label'   => true
						),
						array(
							'type'          => 'checkbox',
							'heading'       => 'Mostrar Saiba Mais',
							'param_name'    => 'read_more',
							'group'         => $atts['group'],
							'admin_label'   => true
						),
						array(
							'type'          => 'textfield',
							'heading'       => 'Classe',
							'param_name'    => 'class',
							'value'         => 'post-style',
							'group'         => $atts['group'],
							'admin_label'   => true
						),
						array(
							'type'          => 'css_editor',
							'heading'       => 'Css box',
							'param_name'    => 'css',
							'group'         => 'Opções de Design'
						)
					)
				) );
			}
		}
		add_action( 'vc_before_init', 'vc_custom_post' );


	// EXCERPT
	//========================================
		function get_excerpt( $limit = 185 ) {
			$excerpt = ( get_the_excerpt() ) ? get_the_excerpt() : get_the_content();
			$excerpt = preg_replace(" ([.*?])",'',$excerpt);
			$excerpt = strip_shortcodes($excerpt);
			$excerpt = strip_tags($excerpt);
			$dots = ( mb_strlen($excerpt) > $limit ) ? '...' : '';
			$excerpt = mb_substr($excerpt, 0, $limit);
			//$excerpt = trim(preg_replace( '/s+/', ' ', $excerpt));
			$excerpt = $excerpt . $dots;
			return $excerpt;
		}