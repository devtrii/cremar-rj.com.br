<?php

    // SHORTCODE
    //========================================
        function title_shortcode( $atts ) {
            $rs = extract( shortcode_atts( array(
                'main_text'             => 'Título Exemplo',
                'tag'                   => 'h2',
                'class'                 => 'title-style',
                'id'                    => '',
                'css'                   => '',
                'secondary_text'        => '',
                'secondary_text_color'  => ''
            ), $atts ));

            $css_class = ( function_exists('vc_map') ) 
                        ? apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts ) 
                        : false;

            $secondary_text_html = '';
            
            if($secondary_text != '')
                $secondary_text_html = "<br><span class='secondary' style='color: {$secondary_text_color}'>{$secondary_text}</span>";

            $html = "<div id = '{$id}' class='title-box'>";
            $html .= "<{$tag} class='{$class}{$css_class}'>{$main_text}{$secondary_text_html}</{$tag}>";
            $html .= "</div>";

            return $html;
        }
        add_shortcode( 'title', 'title_shortcode' );

    // MAP
    //========================================
        function vc_custom_title() {
            if ( function_exists('vc_map') ) {
                $atts = array(
                    'category' => 'Custom',
                    'group' => 'Configurações Gerais',
                );

                vc_map( array(
                    'name'          => 'Título',
                    'base'          => 'title',
                    'description'   => 'Display titles',
                    'category'      => $atts['category'],
                    'params'        => array(
                        array(
                            'type'          => 'textfield',
                            'heading'       => 'Título Principal',
                            'param_name'    => 'main_text',
                            'group'         => $atts['group'],
                            'admin_label'   => true,
                            'description'   => 'Informe o título principal.'
                        ),
                        array(
                            'type'          => 'textfield',
                            'heading'       => 'Título Secondário',
                            'param_name'    => 'secondary_text',
                            'group'         => $atts['group'],
                            'admin_label'   => true,
                            'description'   => 'Informe o texto secondário, se não houver deixe em branco.'
                        ),
                        array(
                            'type'          => 'colorpicker',
                            'heading'       => 'Cor do Título Secondário',
                            'param_name'    => 'secondary_text_color',
                            'group'         => $atts['group'],
                            'admin_label'   => true,
                            'description'   => 'Informe a cor do título secondário.'
                        ),
                        array(
                            'type'          => 'dropdown',
                            'heading'       => 'Tag',
                            'param_name'    => 'tag',
                            'value'         => array( 'h2', 'h1', 'h3', 'h4', 'h5', 'h6', 'p', 'span', 'div' ),
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                        array(
                            'type'          => 'textfield',
                            'heading'       => 'Classe',
                            'param_name'    => 'class',
                            'value'         => 'title-style',
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                        array(
                            'type'          => 'textfield',
                            'heading'       => 'ID',
                            'param_name'    => 'id',
                            'value'         => '',
                            'group'         => $atts['group'],
                            'admin_label'   => true
                        ),
                        array(
                            'type'          => 'css_editor',
                            'heading'       => 'Css box',
                            'param_name'    => 'css',
                            'group'         => 'Opções de Design'
                        )
                    )
                ) );
            }
        }
        add_action( 'vc_before_init', 'vc_custom_title' );