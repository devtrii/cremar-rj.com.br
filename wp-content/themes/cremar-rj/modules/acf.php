<?php
	
	if( function_exists('acf_add_local_field_group') ):

		acf_add_local_field_group(array (
			'key' => 'group_5ebd379d65291',
			'title' => 'Sumário',
			'fields' => array (
				array (
					'key' => 'field_5f998d667f43f',
					'label' => 'Ativar Sumário',
					'name' => 'ativar_sumario',
					'type' => 'true_false',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => 'Deseja Ativar o sumário?',
					'default_value' => 0,
					'ui' => 1,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),
				array (
					'key' => 'field_5ebd37a6ebf9b',
					'label' => 'Número de H2s',
					'name' => 'numero_h2',
					'type' => 'number',
					'instructions' => 'Insira aqui a quantidade de H2s presentes no texto',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_5f998d667f43f',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => '',
					'max' => '',
					'step' => '',
				),
				array (
					'key' => 'field_5ebd3835ebf9c',
					'label' => 'Títulos',
					'name' => 'titulos',
					'type' => 'textarea',
					'instructions' => 'Insira aqui os títulos dos H2s todos separados por virgulas',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_5f998d667f43f',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => '',
					'new_lines' => '',
				),
				array (
					'key' => 'field_5ebd38cfebf9d',
					'label' => 'Texto opcional',
					'name' => 'adicional_titulo',
					'type' => 'text',
					'instructions' => 'Caso deseje escrever alguma frase junto ao sumário',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_5f998d667f43f',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'post',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'side',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
		
		endif;