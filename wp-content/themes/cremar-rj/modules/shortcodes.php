<?php
add_shortcode( 'sumario', 'montarSumario' );
function montarSumario(){
	$num=0;
	$html = '<div id="list_tits">';
	$html .= '<p>Sumário</p>';


	if(get_field('numero_h2') && get_field('titulos')){
		$num = get_field('numero_h2');
		$txt = explode(',', get_field('titulos'));
		$html .= '<script>';
		$html .=  'window.onload = function(){'."\n";
		$html .=  "  for (var i= 0; i< $num;i++){"."\n";
		$html .=  '   var tg = document.getElementsByTagName("h2")[i].setAttribute("id", "titulo"+i );'."\n";
		$html .=  '  }'."\n";
		$html .=  '};'."\n";
		$html .= '</script>'."\n";
		$html .=  '<span>'.get_field('adicional_titulo').'</span>';			
		$html .=  "<ul>";
		for($i = 0 ; $i < $num; $i++){
			$html .=  "<li><a class=goToByScroll href='#titulo".$i."'>".$txt[$i]."</a></li>";
		}
		$html .= "</ul>";
	}
	$html .= "</div>";

	return $html;
}