<?php


require 'admin-page/index.php';

global $wpdb;
$myTable = $wpdb->prefix . "instaLinks";

// Se n tiver a tabela cria
if ($wpdb->get_var('SHOW TABLES LIKE '.$myTable) != $myTable) {
    $sql = 'CREATE TABLE '.$myTable.'(
        id INT NOT NULL AUTO_INCREMENT,
        whatsNumber TEXT,
        name TEXT,
        count INT DEFAULT 0,
        PRIMARY KEY  (id))';

    if(!function_exists('dbDelta')) {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    }
    dbDelta($sql);
}  