<?php

require_once 'ajax.php';


add_action( 'admin_menu', 'instaLinks' );

function instaLinks() {
    add_menu_page  (
        'Links Instagram' ,
        'Links Instagram' ,
        'manage_options',
        sanitize_key('links-instagram'),
        'links_instagram_admin_page',
        'dashicons-instagram',
        3
    );
}

function load_custom_wp_admin_style($hook) {
    // Load only on ?page=mypluginname
	if( $hook != 'toplevel_page_links-instagram' ) {
		 return;
    }
    
    wp_enqueue_style('font_awesome_admin', get_template_directory_uri() . '/assets/font/fontawesome-free-5/css/all.min.css');

    wp_enqueue_style( 'link_insta_css', get_template_directory_uri() . '/modules/LinksInstagram/admin-page/assets/css/style.css');
    wp_enqueue_style( 'link_insta_bootstrap_css', get_template_directory_uri() . '/modules/LinksInstagram/admin-page/assets/css/bootstrap.min.css');

    wp_enqueue_script( 'link_insta_script', get_template_directory_uri() . '/modules/LinksInstagram/admin-page/assets/js/instaLink.js', array( 'jquery' ) );
    wp_enqueue_script( 'link_insta_bootstrap_script', get_template_directory_uri() . '/modules/LinksInstagram/admin-page/assets/js/bootstrap.min.js', array( 'jquery' ) );

}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

require 'page.php';