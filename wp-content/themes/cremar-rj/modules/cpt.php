<?php
	
	// CPT Produtos

		function cpt_produtos() {

			register_taxonomy(
				'categoria_produtos',
				'produto',
				array(
					'label' => __( 'Categorias' ),
					'rewrite' => array( 'slug' => 'categoria' ),
					'hierarchical'	=> true
				)
			);

			$labels = array(
				'name'                  => 'Produtos',
				'singular_name'         => 'Produto',
				'menu_name'             => 'Produtos',
				'name_admin_bar'        => 'Produtos',
				'archives'              => 'Item Archives',
				'attributes'            => 'Item Attributes',
				'parent_item_colon'     => 'Parent Item:',
				'all_items'             => 'Todos os Produtos',
				'add_new_item'          => 'Adicionar Novo Produto',
				'add_new'               => 'Adicionar Produto',
				'new_item'              => 'Novo Produto',
				'edit_item'             => 'Editar Produto',
				'update_item'           => 'Atualizar Produto',
				'view_item'             => 'Ver Produto',
				'view_items'            => 'Ver Produtos',
				'search_items'          => 'Buscar Produto',
				'not_found'             => 'Não encontrado',
				'not_found_in_trash'    => 'Não encontrado na lixeira',
				'featured_image'        => 'Imagem destacada',
				'set_featured_image'    => 'Definir imagem destacada',
				'remove_featured_image' => 'Remover imagem destacada',
				'use_featured_image'    => 'Usar como imagem destacada',
				'insert_into_item'      => 'Insert into item',
				'uploaded_to_this_item' => 'Uploaded to this item',
				'items_list'            => 'Items list',
				'items_list_navigation' => 'Items list navigation',
				'filter_items_list'     => 'Filter items list',
			);
			$args = array(
				'label'                 => 'Produtos',
				'labels'                => $labels,
				'supports'              => array( 'title', 'editor', 'thumbnail' ),
				'hierarchical'          => true,
				'public'                => true,
				'show_ui'               => true,
				'show_in_menu'          => true,
				'menu_position'         => 10,
				'show_in_admin_bar'     => true,
				'show_in_nav_menus'     => true,
				'can_export'            => true,
				'has_archive'           => true,       
				'exclude_from_search'   => false,
				'publicly_queryable'    => true,
				'taxonomies'			=> array('categoria_produtos'),
				'capability_type'       => 'post',
				'menu_icon'             => 'dashicons-cart'
			);

			register_post_type( 'produto', $args );

		}
		add_action( 'init', 'cpt_produtos', 0 );

	