<?php

	// Defines
	define( 'THEME_DIR', get_template_directory() );
	define( 'THEME_URI', get_template_directory_uri() );

	// Debug
	if ( isset( $_GET['debug'] ) ) {
		ini_set('display_errors', 1);
		error_reporting(E_ALL);
	} else {
		ini_set('display_errors', 0);
		error_reporting(0);
	}

	// VC_MAP
	if ( file_exists( THEME_DIR . '/vc_map') ) {
		$files = glob( THEME_DIR . "/vc_map/*.php" );
		foreach ($files as $file) {
			require_once $file;
		}
	}

	// Requireds
	require_once 'contact-form/contact-form.php';
	require_once 'acf.php';
	require_once 'cpt.php';
	require_once 'shortcodes.php';

	//Theme Support
	add_theme_support( 'post-thumbnails' );

	//Menu
	register_nav_menus(
		array(
			'menu_principal' => __( 'Menu Principal' )
		)
	);

	// Disable emojis
	add_filter( 'emoji_svg_url', '__return_false' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );

	// Post Navi
	function wpbeginner_numeric_posts_nav() {

		if( is_page( 'blog' ) || is_page( 194 ) ){

			$args = array(
				'post_type' 		=> 'post',
				'posts_per_page'	=> -1
			);

			$posts = new WP_Query( $args );
			$posts->max_num_pages = ceil( ( $posts->found_posts / 6 ) );

		}else{
			global $wp_query;
			$posts = $wp_query;
		}

		/** Stop execution if there's only 1 page */
		if( $posts->max_num_pages <= 1 )
			return;

		$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
		$max   = ceil( $posts->max_num_pages );

		/**	Add current page to the array */
		if ( $paged >= 1 )
			$links[] = $paged;

		/**	Add the pages around the current page to the array */
		if ( $paged >= 3 ) {
			$links[] = $paged - 1;
			$links[] = $paged - 2;
		}

		if ( ( $paged + 2 ) <= $max ) {
			$links[] = $paged + 2;
			$links[] = $paged + 1;
		}

		$pathUrl = $_SERVER['REQUEST_URI'];
		if( strpos( $pathUrl, '/page/' ) === false ){
			$fullUrl = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}page/";
		}else{
			$fullUrl = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
			$fullUrl = explode( '/page/', $fullUrl );
			$fullUrl = $fullUrl[0] . '/page/';
		}

		echo '<div class="navigation"><ul>' . "\n";

		/**	Previous Post Link */
		if ( $paged >= 2 )
			echo '<li class="first"><a href="'. $fullUrl . ( $paged - 1 ) .'"><</a></li>';

		/**	Link to first page, plus ellipses if necessary */
		if ( ! in_array( 1, $links ) ) {
			$class = 1 == $paged ? ' class="active"' : '';

			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

			if ( ! in_array( 2, $links ) )
				echo '<li class="nope first"><a href="javascript:void(0);">…</a></li>';
		}

		/**	Link to current page, plus 2 pages in either direction if necessary */
		sort( $links );
		foreach ( (array) $links as $link ) {
			$class = $paged == $link ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
		}

		/**	Link to last page, plus ellipses if necessary */
		if ( ! in_array( $max, $links ) ) {
			if ( ! in_array( $max - 1, $links ) )
				echo '<li class="nope last"><a href="javascript:void(0);">…</a></li>' . "\n";

			$class = $paged == $max ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
		}

		/**	Next Post Link */
		if ( $paged >= 1 && $paged < $max )
			echo '<li class="next"><a href="'. $fullUrl . ( $paged + 1 ) .'">></a></li>';

		echo '</ul></div>' . "\n";

	}

	function get_thumbnail( $size = '770x402', $bg = 'd5d5d5', $text = '000', $fontsize = '31' ) {
		$size = explode('x', $size);

		if ( has_post_thumbnail() ) {
			$img = wpb_getImageBySize( array(
				'attach_id' => get_post_thumbnail_id( get_the_ID() ),
				'thumb_size' => $size,
				'class' => 'vc_single_image-img',
			) );
		} else {
			$img['thumbnail'] = '<img src="https://placeholdit.imgix.net/~text?txtsize='. $fontsize .'&bg='. $bg .'&txtclr='. $text .'&txt='. str_replace( ' ' , '+', get_the_title() ) .'&w='. $size[0] .'&h='. $size[1] .'&txttrack=0" alt="'. get_the_title() .'">';
		}
		
		return $img['thumbnail'];
	}

	/* Automatically set the image Title, Alt-Text, Caption & Description upon upload
	--------------------------------------------------------------------------------------*/
	add_action( 'add_attachment', 'my_set_image_meta_upon_image_upload' );
	function my_set_image_meta_upon_image_upload( $post_ID ) {

		// Check if uploaded file is an image, else do nothing

		if ( wp_attachment_is_image( $post_ID ) ) {

			$my_image_title = get_post( $post_ID )->post_title;

			// Sanitize the title:  remove hyphens, underscores & extra spaces:
			$my_image_title = preg_replace( '%\s*[-_\s]+\s*%', ' ',  $my_image_title );

			// Sanitize the title:  capitalize first letter of every word (other letters lower case):
			$my_image_title = ucwords( strtolower( $my_image_title ) );

			// Create an array with the image meta (Title, Caption, Description) to be updated
			// Note:  comment out the Excerpt/Caption or Content/Description lines if not needed
			$my_image_meta = array(
				'ID'		=> $post_ID,			// Specify the image (ID) to be updated
				'post_title'	=> $my_image_title,		// Set image Title to sanitized title
				'post_excerpt'	=> $my_image_title,		// Set image Caption (Excerpt) to sanitized title
				'post_content'	=> $my_image_title,		// Set image Description (Content) to sanitized title
			);

			// Set the image Alt-Text
			update_post_meta( $post_ID, '_wp_attachment_image_alt', $my_image_title );

			// Set the image meta (e.g. Title, Excerpt, Content)
			wp_update_post( $my_image_meta );

		} 
	}