<?php

    function so_db_auto_increment(){

    global $wpdb;

    $hasTableSO = get_option( 'hasTableSO' );

    if( !$hasTableSO ){
        $wpdb->query("CREATE TABLE IF NOT EXISTS `wp_autoincrement` (
                            `id` INT(11) NOT NULL AUTO_INCREMENT,
                            PRIMARY KEY (`id`),
                            INDEX `id` (`id`)
                        )
                        ENGINE=InnoDB;"
        );

        add_option( 'hasTableSO', true );
        $wpdb->insert( 'wp_autoincrement', array( 'id' => 15000 ) );
        return $wpdb->insert_id;
    }else{
        $last = $wpdb->insert( 'wp_autoincrement', array( 'id' => '' ) );
        return $wpdb->insert_id;
    }

}

// ** LEMBRAR DE MARCAR "Usar tipo de conteúdo HTML" NO CONTACT FORM
add_action( 'wpcf7_before_send_mail', 'before_send_mail' );

function before_send_mail( $wpcf7 ) {
    // get the contact form object
    $wpcf = WPCF7_ContactForm::get_current();

    //Estilos
    $arOptions = array(
        'form_logo',
        'form_bg_logo',
        'form_cor_logo',
        'form_cor_rodape'
    );

    foreach ( $arOptions as $option ) {
        $arOptions[$option] = get_option( $option );
    }

    $lastID = so_db_auto_increment();
    $mail = $wpcf->mail;

    $newSubject = "{$mail['subject']} ({$lastID})";
    $newBody = '<div id="wrapper" dir="ltr" style="background-color: #f5f5f5; margin: 0; padding: 70px 0 70px 0; -webkit-text-size-adjust: none !important; width: 100%;">
                <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tbody><tr>
    <td align="center" valign="top">
                            <div id="template_header_image">
                                <p style="margin-top: 0;"><img src="'.$arOptions['form_logo'].'" alt="'. get_bloginfo( 'name' ) .'" /></p>                      </div>
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container" style="box-shadow: 0 1px 4px rgba(0,0,0,0.1) !important; background-color: #fdfdfd; border: 1px solid #dcdcdc; border-radius: 3px !important;">
    <tbody><tr>
    <td align="center" valign="top">
                                        <!-- Header -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style="background-color: #'.$arOptions['form_bg_logo'].'; border-radius: 3px 3px 0 0 !important; color: #'.$arOptions['form_cor_logo'].'; border-bottom: 0; font-weight: bold; line-height: 100%; vertical-align: middle; font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif;"><tbody><tr>
    <td id="header_wrapper" style="padding: 36px 48px; display: block;">
                                                    <h1 style="color: #'.$arOptions['form_cor_logo'].'; font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 20px; font-weight: 300; line-height: 150%; margin: 0; text-align: center; text-shadow: 0 1px 0 #b8d960; -webkit-font-smoothing: antialiased;">'. $newSubject .'</h1>
                                                </td>
                                            </tr></tbody></table>
    <!-- End Header -->
    </td>
                                </tr>
    <tr>
    <td align="center" valign="top">
                                        <!-- Body -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body"><tbody><tr>
    <td valign="top" id="body_content" style="background-color: #fdfdfd;">
                                                    <!-- Content -->
                                                    <table border="0" cellpadding="20" cellspacing="0" width="100%"><tbody><tr>
    <td valign="top" style="padding: 48px 48px 0px;">
                                                                <div id="body_content_inner" style="color: #737373; font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
    <p style="margin: 0 0 16px;">'. $mail['body'] .'</p>
                                                                </div>
                                                            </td>
                                                        </tr></tbody></table>
    <!-- End Content -->
    </td>
                                            </tr></tbody></table>
    <!-- End Body -->
    </td>
                                </tr>
    <tr>
    <td align="center" valign="top">
                                        <!-- Footer -->
                                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer"><tbody><tr>
    <td valign="top" style="padding: 0; -webkit-border-radius: 6px;">
                                                    <table border="0" cellpadding="10" cellspacing="0" width="100%"><tbody><tr>
    <td colspan="2" valign="middle" id="credit" style="padding: 0 48px 48px 48px; -webkit-border-radius: 6px; border: 0; color: #'.$arOptions['form_cor_rodape'].'; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;">
                                                                <p>Este e-mail foi enviado de um formulário em '. get_bloginfo( 'name' ) . ' - ' . date( 'Y' ) .' ('. get_bloginfo( 'url' ) .')</p>
                                                            </td>
                                                        </tr></tbody></table>
    </td>
                                            </tr></tbody></table>
    <!-- End Footer -->
    </td>
                                </tr>
    </tbody></table>
    </td>
                    </tr></tbody></table>
    </div>';

    $mail['subject'] = $newSubject;
    $mail['body'] = $newBody;
    $mail['use_html'] = 1;

    $wpcf->set_properties(array(
     "mail" => $mail
    ));

}

add_action('admin_menu', 'register_wpcf7_estilo_email');
function register_wpcf7_estilo_email() {
    add_submenu_page(
        'wpcf7',
        'Estilo do E-mail',
        'Estilo do E-mail',
        'manage_options',
        'wpcf7-estilo-email',
        'wpcf7_estilo_email' );
}

function wpcf7_estilo_email() {
            
        $arOptions = array(
            'form_logo',
            'form_bg_logo',
            'form_cor_logo',
            'form_cor_rodape'
        );

        if( isset( $_POST['salvar_estilo'] ) && $_GET['page'] == 'wpcf7-estilo-email' ){
            foreach ( $arOptions as $option ) {
                update_option( $option, $_POST[$option] );
            }
        }

        foreach ( $arOptions as $option ) {
            $arOptions[$option] = get_option( $option );
        }

    ?>

    <script type='text/javascript' src='/wp-content/plugins/trii-login-custom-style/assets/js/jscolor.min.js'></script>
    <style>
        .form-table td:first-child {
            width: 300px !important;
        }
    </style>
    <div class="wrap">
    <form method="POST" class="half">
        <table class="form-table">
            <tr>
                <td>Logo</td>
                <td>
                    <?php
                        $id = 'form_logo';
                    ?>
                    <input name="<?php echo $id; ?>" id="<?php echo $id; ?>" value="<?php echo $arOptions[$id]; ?>" style="margin-bottom:5px;" /> <br />
                    <i>URL completa da imagem.</i>
                </td>
            </tr>
            <tr>
                <td>Background do cabeçalho</td>
                <td>
                    <?php
                        $id = 'form_bg_logo';
                    ?>
                    <input class="jscolor" name="<?php echo $id; ?>" id="<?php echo $id; ?>" value="<?php echo $arOptions[$id]; ?>" style="margin-bottom:5px;" /> <br />
                </td>
            </tr>
            <tr>
                <td>Cor do cabeçalho</td>
                <td>
                    <?php
                        $id = 'form_cor_logo';
                    ?>
                    <input class="jscolor" name="<?php echo $id; ?>" id="<?php echo $id; ?>" value="<?php echo $arOptions[$id]; ?>" style="margin-bottom:5px;" /> <br />
                </td>
            </tr>
            <tr>
                <td>Cor do link do rodapé</td>
                <td>
                    <?php
                        $id = 'form_cor_rodape';
                    ?>
                    <input class="jscolor" name="<?php echo $id; ?>" id="<?php echo $id; ?>" value="<?php echo $arOptions[$id]; ?>" value="" style="margin-bottom:5px;" /> <br />
                </td>
            </tr>
            <tr>
                <td scope="row"></td>
                <td><p class="submit"><input type="submit" name="salvar_estilo" class="button button-primary" value="SALVAR"  /></p></td>
            </tr>
        </table>
    </form></div>

<?php } ?>