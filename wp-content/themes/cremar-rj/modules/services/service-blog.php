<?php 




class BlogServices{

    public function get_blog_categories(){

        $args = array(
            'taxonomy'=>"category",
            'orderby'=>"term_id",
            'exclude'=>'1'
        );

      $categorias = get_terms($args);
      $html = '';

      foreach ($categorias as $key => $value) {

        
        
        $nome_categoria = $value->name;

        $category_id = get_cat_ID($nome_categoria);
        $category_link = get_category_link( $category_id );
        
        $html .= '<option value="'. $category_link.'">'.$nome_categoria.'</option>';
      }
      
      return $html;
    }
}

?>