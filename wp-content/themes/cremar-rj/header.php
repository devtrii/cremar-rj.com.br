<?php global $current_user; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta name="theme-color" content="#fff">
        <meta name="msapplication-navbutton-color" content="#fff">
        <meta name="apple-mobile-web-app-status-bar-style" content="#fff">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="application-name" content="<?php bloginfo('name'); ?>">
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"/>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge;chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
        <title>
            <?php
                if( is_404() ){
                    echo 'Página não encontrada -Cremar Rj';
                }else{
                    wp_title();
                }
            ?>
        </title>

        <!-- For use in JS files -->
        <script type="text/javascript">
            var template_dir = "<?php echo get_template_directory_uri(); ?>";
            window.ajaxurl = '<?php echo admin_url("admin-ajax.php"); ?>';
        </script>
        <link rel="profile" href="http://gmpg.org/xfn/11"/>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
        <?php wp_head(); ?>
        <link rel='shortcut icon' href='/favicon.ico' type='image/x-icon' />
    </head> 

    <body <?php body_class(); ?> data-pid="<?php echo get_the_ID(); ?>">

    <header id="header">
            <div class="main-menu">
                <div class="container">
                    <div class="row container_header">
                        <div class="top_header">
                                <?php 
                                
                                
                                if(is_home() || is_front_page()){
                                    echo '	<div class="col-4 col_1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="247.804" height="34.874" >
                              <g id="atendimento_24h" data-name="atendimento 24h" transform="translate(0)">
                                <g id="_24h" data-name="24h" transform="translate(0 0)">
                                  <g id="Grupo_152" data-name="Grupo 152" transform="translate(19.423 19.988)">
                                    <g id="Grupo_150" data-name="Grupo 150" transform="translate(0 0)">
                                      <path id="Caminho_543" data-name="Caminho 543" d="M321.518,308.779a2.536,2.536,0,0,0,.532-1.456,1.614,1.614,0,0,0-.462-1.174,1.586,1.586,0,0,0-1.185-.472q-1.325,0-2.51,1.887l-2.509-1.486a7.771,7.771,0,0,1,2.158-2.309,5.286,5.286,0,0,1,2.991-.783,4.818,4.818,0,0,1,3.192,1.154,3.887,3.887,0,0,1,1.375,3.142,4.222,4.222,0,0,1-.552,2.078,13.587,13.587,0,0,1-2.038,2.54l-2.53,2.63h5.541v2.811h-9.9v-2.329l4.116-4.216A17.986,17.986,0,0,0,321.518,308.779Z" transform="translate(-315.119 -302.721)" fill="#c68d47"/>
                                      <path id="Caminho_544" data-name="Caminho 544" d="M323.984,316.068h-9.9a.266.266,0,0,1-.266-.266v-2.329a.266.266,0,0,1,.075-.186l4.116-4.216a17.831,17.831,0,0,0,1.75-1.986,2.292,2.292,0,0,0,.483-1.3,1.346,1.346,0,0,0-.386-.989,1.32,1.32,0,0,0-.995-.392c-.782,0-1.55.593-2.284,1.763a.266.266,0,0,1-.36.087l-2.51-1.486a.266.266,0,0,1-.088-.373,8.071,8.071,0,0,1,2.234-2.386,5.569,5.569,0,0,1,3.139-.828,5.1,5.1,0,0,1,3.363,1.217,4.159,4.159,0,0,1,1.47,3.346,4.5,4.5,0,0,1-.586,2.207,13.886,13.886,0,0,1-2.079,2.595l-2.1,2.18h4.917a.266.266,0,0,1,.266.266V315.8A.266.266,0,0,1,323.984,316.068Zm-9.632-.531h9.366v-2.279h-5.275a.266.266,0,0,1-.191-.45l2.53-2.63a13.41,13.41,0,0,0,2-2.484,3.978,3.978,0,0,0,.519-1.949,3.6,3.6,0,0,0-1.28-2.939,4.579,4.579,0,0,0-3.021-1.092,5.051,5.051,0,0,0-2.844.738,7.313,7.313,0,0,0-1.93,2l2.05,1.213a3.3,3.3,0,0,1,2.594-1.794,1.85,1.85,0,0,1,1.374.551,1.877,1.877,0,0,1,.538,1.361,2.806,2.806,0,0,1-.581,1.609h0a18.3,18.3,0,0,1-1.8,2.051l-4.039,4.138Zm5.627-8.3h0Z" transform="translate(-313.58 -301.182)" fill="#c68d47"/>
                                    </g>
                                    <g id="Grupo_151" data-name="Grupo 151" transform="translate(11.544 0.321)">
                                      <path id="Caminho_545" data-name="Caminho 545" d="M399.465,313.141v-2.449h3.012v2.449h1.586v2.711h-1.586V319.2h-3.012v-3.353h-5.642v-2.389l5-8.292h3.413l-4.758,7.97Z" transform="translate(-393.557 -304.904)" fill="#c68d47"/>
                                      <path id="Caminho_546" data-name="Caminho 546" d="M400.937,317.929h-3.012a.266.266,0,0,1-.266-.266v-3.087h-5.376a.266.266,0,0,1-.266-.266v-2.389a.266.266,0,0,1,.038-.137l5-8.292a.266.266,0,0,1,.228-.128H400.7a.266.266,0,0,1,.228.4l-3.06,5.126a.267.267,0,0,1,.062-.007h3.012a.266.266,0,0,1,.266.266v2.184h1.32a.266.266,0,0,1,.266.266v2.711a.266.266,0,0,1-.266.266H401.2v3.087A.266.266,0,0,1,400.937,317.929Zm-2.746-.531h2.48v-3.087a.266.266,0,0,1,.266-.266h1.32v-2.179h-1.32a.266.266,0,0,1-.266-.266v-2.184h-2.48V311.6a.266.266,0,0,1-.266.266h-1.988a.266.266,0,0,1-.228-.4l4.518-7.568h-2.795l-4.884,8.1v2.049h5.376a.266.266,0,0,1,.266.266Zm-1.785-6.063h1.254v-2.1Z" transform="translate(-392.018 -303.364)" fill="#c68d47"/>
                                    </g>
                                  </g>
                                  <g id="Grupo_153" data-name="Grupo 153" transform="translate(10.748 10.768)">
                                    <path id="Caminho_547" data-name="Caminho 547" d="M254.78,239.289l5.425,6.817a1.419,1.419,0,1,0,2-2l-6.818-5.424a.451.451,0,0,0-.635-.033.393.393,0,0,0-.1.162.48.48,0,0,0,.13.474" transform="translate(-254.631 -238.537)" fill="#c68d47"/>
                                  </g>
                                  <g id="Grupo_154" data-name="Grupo 154" transform="translate(16.101 8.063)">
                                    <path id="Caminho_548" data-name="Caminho 548" d="M300.839,220.4l-9.024,7.632c-.832.832-1.06,1.953-.509,2.5s1.673.323,2.5-.51L301.442,221c.251-.251.32-.59.154-.756a.313.313,0,0,0-.174-.086.721.721,0,0,0-.583.239" transform="translate(-291.005 -220.154)" fill="#c68d47"/>
                                  </g>
                                  <g id="Grupo_155" data-name="Grupo 155" transform="translate(0)">
                                    <path id="Caminho_549" data-name="Caminho 549" d="M198.89,197.65c0-.006,0-.012,0-.018a14.83,14.83,0,1,1,14.325-10.19,6.7,6.7,0,0,1,.215.69l2.141-3.551a2.635,2.635,0,0,1,.872-.877q.023-.448.023-.9A17.432,17.432,0,1,0,198.9,200.231c-.008-.083-.013-.167-.013-.252Z" transform="translate(-181.603 -165.37)" fill="#c68d47"/>
                                  </g>
                                  <g id="Grupo_160" data-name="Grupo 160" transform="translate(4.591 3.932)">
                                    <g id="Grupo_156" data-name="Grupo 156" transform="translate(23.479 12.729)">
                                      <path id="Caminho_550" data-name="Caminho 550" d="M372.326,282.551h1.533" transform="translate(-372.326 -281.966)" fill="#c68d47"/>
                                      <path id="Caminho_551" data-name="Caminho 551" d="M372.326,279.747v-1.17h1.533a.585.585,0,1,1,0,1.17Z" transform="translate(-372.326 -278.577)" fill="#c68d47"/>
                                    </g>
                                    <g id="Grupo_157" data-name="Grupo 157" transform="translate(0 12.729)">
                                      <path id="Caminho_552" data-name="Caminho 552" d="M218.3,282.554h-1.533" transform="translate(-216.184 -281.969)" fill="#c68d47"/>
                                      <path id="Caminho_553" data-name="Caminho 553" d="M214.913,279.75H213.38a.585.585,0,1,1,0-1.17h1.533a.585.585,0,1,1,0,1.17Z" transform="translate(-212.795 -278.58)" fill="#c68d47"/>
                                    </g>
                                    <g id="Grupo_158" data-name="Grupo 158" transform="translate(12.214 0)">
                                      <line id="Linha_1" data-name="Linha 1" y1="1.598" transform="translate(0.585 0.585)" fill="#fff" stroke="#c68d47" stroke-width="1"/>
                                      <path id="Caminho_554" data-name="Caminho 554" d="M296.368,194.857a.585.585,0,0,1-.585-.585v-1.6a.585.585,0,1,1,1.17,0v1.6A.585.585,0,0,1,296.368,194.857Z" transform="translate(-295.783 -192.089)" fill="#c68d47"/>
                                    </g>
                                    <g id="Grupo_159" data-name="Grupo 159" transform="translate(12.214 24.259)">
                                      <line id="Linha_2" data-name="Linha 2" y1="1.598" transform="translate(0.585 0.585)" fill="#fff" stroke="#c68d47" stroke-width="1"/>
                                      <path id="Caminho_555" data-name="Caminho 555" d="M296.368,359.688a.585.585,0,0,1-.585-.585v-1.6a.585.585,0,1,1,1.17,0v1.6A.585.585,0,0,1,296.368,359.688Z" transform="translate(-295.783 -356.92)" fill="#c68d47"/>
                                    </g>
                                  </g>
                                </g>
                                <text id="Atendimento_24h-2" data-name="Atendimento 24h" transform="translate(53.804 27.795)" fill="#c68d47" font-size="25" font-family="Roboto-Bold, Roboto" font-weight="700"><tspan x="0" y="0">Atendimento 24h</tspan></text>
                              </g>
                            </svg>
                            
                                </div>
                                
                                    <div class="col-4 col_2">
                                    <div class="box-logo">
                                                                                                            <a href="/" title="Cremação no Rio de Janeiro">
                                    <h1 class="logo">Cremação no Rio de Janeiro </h1>
                                                                        </a>
                                                                    </div>
                                    
                                        </div>
                                        <div class="col-4 col_3">
                                            <a href="tel:02141484107"><svg xmlns="http://www.w3.org/2000/svg" width="215.6" height="33">
                                      <g id="telefone" transform="translate(-20.203 8)">
                                        <text id="_21_4148-4107" data-name="(21) 4148-4107" transform="translate(58.804 18)" fill="#c68d47" font-size="25" font-family="Roboto-Bold, Roboto" font-weight="700"><tspan x="0" y="0">(21) 4148-4107</tspan></text>
                                        <g id="phone-call_2_" data-name="phone-call (2)" transform="translate(20.203 -4)">
                                          <g id="Grupo_161" data-name="Grupo 161" transform="translate(0 0)">
                                            <path id="Caminho_556" data-name="Caminho 556" d="M27.845,20.622,23.923,16.7a2.608,2.608,0,0,0-4.342.98A2.669,2.669,0,0,1,16.5,19.362c-2.8-.7-6.582-4.342-7.283-7.283A2.538,2.538,0,0,1,10.9,9a2.608,2.608,0,0,0,.98-4.342L7.958.735a2.8,2.8,0,0,0-3.781,0L1.515,3.4C-1.146,6.2,1.8,13.62,8.378,20.2s14.005,9.664,16.806,6.862L27.845,24.4A2.8,2.8,0,0,0,27.845,20.622Z" transform="translate(-0.539 0)" fill="#c68d47"/>
                                          </g>
                                        </g>
                                      </g>
                                    </svg></a>
                                    
                                        </div>
                                    </div>';
                                }else{

                                    echo '	<div class="col-4 col_1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="247.804" height="34.874" >
                              <g id="atendimento_24h" data-name="atendimento 24h" transform="translate(0)">
                                <g id="_24h" data-name="24h" transform="translate(0 0)">
                                  <g id="Grupo_152" data-name="Grupo 152" transform="translate(19.423 19.988)">
                                    <g id="Grupo_150" data-name="Grupo 150" transform="translate(0 0)">
                                      <path id="Caminho_543" data-name="Caminho 543" d="M321.518,308.779a2.536,2.536,0,0,0,.532-1.456,1.614,1.614,0,0,0-.462-1.174,1.586,1.586,0,0,0-1.185-.472q-1.325,0-2.51,1.887l-2.509-1.486a7.771,7.771,0,0,1,2.158-2.309,5.286,5.286,0,0,1,2.991-.783,4.818,4.818,0,0,1,3.192,1.154,3.887,3.887,0,0,1,1.375,3.142,4.222,4.222,0,0,1-.552,2.078,13.587,13.587,0,0,1-2.038,2.54l-2.53,2.63h5.541v2.811h-9.9v-2.329l4.116-4.216A17.986,17.986,0,0,0,321.518,308.779Z" transform="translate(-315.119 -302.721)" fill="#c68d47"/>
                                      <path id="Caminho_544" data-name="Caminho 544" d="M323.984,316.068h-9.9a.266.266,0,0,1-.266-.266v-2.329a.266.266,0,0,1,.075-.186l4.116-4.216a17.831,17.831,0,0,0,1.75-1.986,2.292,2.292,0,0,0,.483-1.3,1.346,1.346,0,0,0-.386-.989,1.32,1.32,0,0,0-.995-.392c-.782,0-1.55.593-2.284,1.763a.266.266,0,0,1-.36.087l-2.51-1.486a.266.266,0,0,1-.088-.373,8.071,8.071,0,0,1,2.234-2.386,5.569,5.569,0,0,1,3.139-.828,5.1,5.1,0,0,1,3.363,1.217,4.159,4.159,0,0,1,1.47,3.346,4.5,4.5,0,0,1-.586,2.207,13.886,13.886,0,0,1-2.079,2.595l-2.1,2.18h4.917a.266.266,0,0,1,.266.266V315.8A.266.266,0,0,1,323.984,316.068Zm-9.632-.531h9.366v-2.279h-5.275a.266.266,0,0,1-.191-.45l2.53-2.63a13.41,13.41,0,0,0,2-2.484,3.978,3.978,0,0,0,.519-1.949,3.6,3.6,0,0,0-1.28-2.939,4.579,4.579,0,0,0-3.021-1.092,5.051,5.051,0,0,0-2.844.738,7.313,7.313,0,0,0-1.93,2l2.05,1.213a3.3,3.3,0,0,1,2.594-1.794,1.85,1.85,0,0,1,1.374.551,1.877,1.877,0,0,1,.538,1.361,2.806,2.806,0,0,1-.581,1.609h0a18.3,18.3,0,0,1-1.8,2.051l-4.039,4.138Zm5.627-8.3h0Z" transform="translate(-313.58 -301.182)" fill="#c68d47"/>
                                    </g>
                                    <g id="Grupo_151" data-name="Grupo 151" transform="translate(11.544 0.321)">
                                      <path id="Caminho_545" data-name="Caminho 545" d="M399.465,313.141v-2.449h3.012v2.449h1.586v2.711h-1.586V319.2h-3.012v-3.353h-5.642v-2.389l5-8.292h3.413l-4.758,7.97Z" transform="translate(-393.557 -304.904)" fill="#c68d47"/>
                                      <path id="Caminho_546" data-name="Caminho 546" d="M400.937,317.929h-3.012a.266.266,0,0,1-.266-.266v-3.087h-5.376a.266.266,0,0,1-.266-.266v-2.389a.266.266,0,0,1,.038-.137l5-8.292a.266.266,0,0,1,.228-.128H400.7a.266.266,0,0,1,.228.4l-3.06,5.126a.267.267,0,0,1,.062-.007h3.012a.266.266,0,0,1,.266.266v2.184h1.32a.266.266,0,0,1,.266.266v2.711a.266.266,0,0,1-.266.266H401.2v3.087A.266.266,0,0,1,400.937,317.929Zm-2.746-.531h2.48v-3.087a.266.266,0,0,1,.266-.266h1.32v-2.179h-1.32a.266.266,0,0,1-.266-.266v-2.184h-2.48V311.6a.266.266,0,0,1-.266.266h-1.988a.266.266,0,0,1-.228-.4l4.518-7.568h-2.795l-4.884,8.1v2.049h5.376a.266.266,0,0,1,.266.266Zm-1.785-6.063h1.254v-2.1Z" transform="translate(-392.018 -303.364)" fill="#c68d47"/>
                                    </g>
                                  </g>
                                  <g id="Grupo_153" data-name="Grupo 153" transform="translate(10.748 10.768)">
                                    <path id="Caminho_547" data-name="Caminho 547" d="M254.78,239.289l5.425,6.817a1.419,1.419,0,1,0,2-2l-6.818-5.424a.451.451,0,0,0-.635-.033.393.393,0,0,0-.1.162.48.48,0,0,0,.13.474" transform="translate(-254.631 -238.537)" fill="#c68d47"/>
                                  </g>
                                  <g id="Grupo_154" data-name="Grupo 154" transform="translate(16.101 8.063)">
                                    <path id="Caminho_548" data-name="Caminho 548" d="M300.839,220.4l-9.024,7.632c-.832.832-1.06,1.953-.509,2.5s1.673.323,2.5-.51L301.442,221c.251-.251.32-.59.154-.756a.313.313,0,0,0-.174-.086.721.721,0,0,0-.583.239" transform="translate(-291.005 -220.154)" fill="#c68d47"/>
                                  </g>
                                  <g id="Grupo_155" data-name="Grupo 155" transform="translate(0)">
                                    <path id="Caminho_549" data-name="Caminho 549" d="M198.89,197.65c0-.006,0-.012,0-.018a14.83,14.83,0,1,1,14.325-10.19,6.7,6.7,0,0,1,.215.69l2.141-3.551a2.635,2.635,0,0,1,.872-.877q.023-.448.023-.9A17.432,17.432,0,1,0,198.9,200.231c-.008-.083-.013-.167-.013-.252Z" transform="translate(-181.603 -165.37)" fill="#c68d47"/>
                                  </g>
                                  <g id="Grupo_160" data-name="Grupo 160" transform="translate(4.591 3.932)">
                                    <g id="Grupo_156" data-name="Grupo 156" transform="translate(23.479 12.729)">
                                      <path id="Caminho_550" data-name="Caminho 550" d="M372.326,282.551h1.533" transform="translate(-372.326 -281.966)" fill="#c68d47"/>
                                      <path id="Caminho_551" data-name="Caminho 551" d="M372.326,279.747v-1.17h1.533a.585.585,0,1,1,0,1.17Z" transform="translate(-372.326 -278.577)" fill="#c68d47"/>
                                    </g>
                                    <g id="Grupo_157" data-name="Grupo 157" transform="translate(0 12.729)">
                                      <path id="Caminho_552" data-name="Caminho 552" d="M218.3,282.554h-1.533" transform="translate(-216.184 -281.969)" fill="#c68d47"/>
                                      <path id="Caminho_553" data-name="Caminho 553" d="M214.913,279.75H213.38a.585.585,0,1,1,0-1.17h1.533a.585.585,0,1,1,0,1.17Z" transform="translate(-212.795 -278.58)" fill="#c68d47"/>
                                    </g>
                                    <g id="Grupo_158" data-name="Grupo 158" transform="translate(12.214 0)">
                                      <line id="Linha_1" data-name="Linha 1" y1="1.598" transform="translate(0.585 0.585)" fill="#fff" stroke="#c68d47" stroke-width="1"/>
                                      <path id="Caminho_554" data-name="Caminho 554" d="M296.368,194.857a.585.585,0,0,1-.585-.585v-1.6a.585.585,0,1,1,1.17,0v1.6A.585.585,0,0,1,296.368,194.857Z" transform="translate(-295.783 -192.089)" fill="#c68d47"/>
                                    </g>
                                    <g id="Grupo_159" data-name="Grupo 159" transform="translate(12.214 24.259)">
                                      <line id="Linha_2" data-name="Linha 2" y1="1.598" transform="translate(0.585 0.585)" fill="#fff" stroke="#c68d47" stroke-width="1"/>
                                      <path id="Caminho_555" data-name="Caminho 555" d="M296.368,359.688a.585.585,0,0,1-.585-.585v-1.6a.585.585,0,1,1,1.17,0v1.6A.585.585,0,0,1,296.368,359.688Z" transform="translate(-295.783 -356.92)" fill="#c68d47"/>
                                    </g>
                                  </g>
                                </g>
                                <text id="Atendimento_24h-2" data-name="Atendimento 24h" transform="translate(53.804 27.795)" fill="#c68d47" font-size="25" font-family="Roboto-Bold, Roboto" font-weight="700"><tspan x="0" y="0">Atendimento 24h</tspan></text>
                              </g>
                            </svg>
                            
                                </div>
                                
                                    <div class="col-4 col_2">
                                    <div class="box-logo">
                                                                                                            <a href="/" title="Cremação no Rio de Janeiro">
                                    <span class="logo">Cremação no Rio de Janeiro </span>
                                                                        </a>
                                                                    </div>
                                    
                                        </div>
                                        <div class="col-4 col_3">
                                            <a href="tel:02141484107"><svg xmlns="http://www.w3.org/2000/svg" width="215.6" height="33">
                                      <g id="telefone" transform="translate(-20.203 8)">
                                        <text id="_21_4148-4107" data-name="(21) 4148-4107" transform="translate(58.804 18)" fill="#c68d47" font-size="25" font-family="Roboto-Bold, Roboto" font-weight="700"><tspan x="0" y="0">(21) 4148-4107</tspan></text>
                                        <g id="phone-call_2_" data-name="phone-call (2)" transform="translate(20.203 -4)">
                                          <g id="Grupo_161" data-name="Grupo 161" transform="translate(0 0)">
                                            <path id="Caminho_556" data-name="Caminho 556" d="M27.845,20.622,23.923,16.7a2.608,2.608,0,0,0-4.342.98A2.669,2.669,0,0,1,16.5,19.362c-2.8-.7-6.582-4.342-7.283-7.283A2.538,2.538,0,0,1,10.9,9a2.608,2.608,0,0,0,.98-4.342L7.958.735a2.8,2.8,0,0,0-3.781,0L1.515,3.4C-1.146,6.2,1.8,13.62,8.378,20.2s14.005,9.664,16.806,6.862L27.845,24.4A2.8,2.8,0,0,0,27.845,20.622Z" transform="translate(-0.539 0)" fill="#c68d47"/>
                                          </g>
                                        </g>
                                      </g>
                                    </svg></a>
                                    
                                        </div>
                                    </div>';

                                }


                                ?>

                            </div>

                                

                        <div >
                            <div class="box_right">
                                <?php dynamic_sidebar( 'top_header-rigth' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-menu header_bottom">
                <div class="container ">
                    <div class="row ">
                        <div class="col-md-12">
                            <div class="contents">


                                <div class="box-menu">
                                    <nav>
                                        <?php
                                        wp_nav_menu(array( 
                                            'theme_location' => 'menu_principal'
                                        ));
                                        ?>
                                    </nav>
                                    <div class="menu-mobile">
                                        <i class="fa fa-bars"></i>
                                    </div>
                                </div>

                                <div class="box-mobile">
                                    <div class="box-fechar">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/menu-fechar.png" alt="fechar" />
                                    </div>
                                    <nav>
                                        <?php
                                            wp_nav_menu(array( 
                                                'theme_location' => 'menu_principal'
                                            ));
                                        ?>
                                    </nav>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <?php

        $mostrar_header = (!is_home() && !is_front_page() && !is_404() && get_field('mostrar_header') && !is_archive()) || is_search() || is_page('blog') || is_singular( 'post' ) || is_category() || is_tag();

if( $mostrar_header ){
?>
    <div id="bg-cabecalho">

        <?php

            $hasThumb = get_field( 'background_imagem' );

            if( !$hasThumb && (is_page('blog') || is_singular( 'post' ) || is_category() || is_tag()) )
                $hasThumb = get_template_directory_uri() . '/assets/images/bg-blog.jpg';

            else if(is_search())
                $hasThumb = '/wp-content/uploads/2018/10/bg-quem-somos.jpg';

            else if ( !$hasThumb )
                $hasThumb = get_template_directory_uri() . '/assets/images/bg-blog.jpg';
        ?>

        <div class="box-titulo" style="<?php echo ($hasThumb && !is_numeric( $hasThumb ) ? 'background: url( '. $hasThumb .' ) no-repeat top center;' : ''); ?>">
            <div class="container">

                <div class="infos rowd">
                <?php

                    $mt = get_field('titulo_principal');
                    $st = get_field('subtitulo_customizado');
                    if ( is_page() && ($mt || $st) ) {
                ?>
                    <h1>
                        <?php if ( $mt ){ ?>
                            <div class="titulo"><?php echo $mt; ?></div>
                        <?php } if ( $st ){ ?>
                            <div class="subtitulo"><?php echo $st; ?></div>
                        <?php } ?>
                    </h1>
                <?php } else { ?>
                    <?php if ( is_singular('post') || is_category() || is_tag() || is_page( 'blog' ) ) { ?>

                        <?php if( is_singular('post') ){ ?>
                            <span class="titulo">CONHEÇA NOSSO BLOG</span>
                        <?php } else { ?>
                            <h1 class="titulo">CONHEÇA NOSSO BLOG</h1>
                        <?php } ?>

                    <?php } else { ?>
                        <h1 class = "titulo">Encontre o que você procura </h1>
                    <?php } ?>

                <?php } ?>
                </div>

            </div>
        </div>
    </div>

<?php 
}
?>

