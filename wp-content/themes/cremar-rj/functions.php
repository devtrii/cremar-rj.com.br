<?php

	function logx($data){
		echo "<pre>" . print_r($data, true) . "</pre>";
	}
	
	//Include base theme
	require_once 'modules/base.php';
	require_once 'modules/services/service-blog.php';
	require 'ajax.php';


	// CSS and JS files for the site
	function enqueue_styles_scripts() {


		//Fontes
		wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/font/fontawesome-free-5/css/all.min.css');

		//CSS
		wp_enqueue_style('bootstrap', get_template_directory_uri() . '/modules/bootstrap/bootstrap.min.css');
		wp_enqueue_style('plugin-whatsapp', get_template_directory_uri() . '/assets/css/plugin-whatsapp.css');
		wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.css');


		// Owl Carousel
		wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/modules/owl-carousel/owl.carousel.min.css' );
		wp_enqueue_style( 'owl-theme', get_template_directory_uri() . '/modules/owl-carousel/owl.theme.default.min.css' );
		wp_enqueue_script( 'owl-carousel-js', get_stylesheet_directory_uri() . '/modules/owl-carousel/owl.carousel.min.js', array( 'jquery' ) );


		if ( is_404() ){
			wp_enqueue_style('404', get_template_directory_uri() . '/assets/css/404.css');
		}

		//Javascript
		wp_enqueue_script( 'bootstrap-js', get_stylesheet_directory_uri() . '/modules/bootstrap/bootstrap.min.js', array( 'jquery' ) );
		wp_enqueue_script( 'main-js', get_stylesheet_directory_uri() . '/assets/js/main.js', array( 'jquery' ) );

		
	}

	if (!is_admin()) {
		add_action('wp_enqueue_scripts', 'enqueue_styles_scripts');
	}

	//Custom Menu	
	register_nav_menus(
		array(
			'links_uteis_menu' => __( 'Links Úteis' )
		)
	);


	//Widgets
	add_action( 'widgets_init', 'TI_widgets' );
	function TI_widgets() {
		register_sidebar( array(
			'name' => 'Top Header - Left',
			'id' => 'top_header-left',
			'description' => 'Parte esqueda do top header',
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',
			'after_title'   => '',
		) );

		register_sidebar( array(
			'name' => 'Top Header - Rigth',
			'id' => 'top_header-rigth',
			'description' => 'Parte direita do top header',
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',
			'after_title'   => '',
	    ) );

	    register_sidebar( array(
	        'name' => 'Footer - Colunna 1',
	        'id' => 'footer1',
	        'description' => '',
	        'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<p class="titulo">',
			'after_title'   => '</p>',
	    ) );

	    register_sidebar( array(
	        'name' => 'Footer - Colunna 2',
	        'id' => 'footer2',
	        'description' => '',
	        'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<p class="titulo">',
			'after_title'   => '</p>',
	    ) );

	    register_sidebar( array(
	        'name' => 'Footer - Colunna 3',
	        'id' => 'footer3',
	        'description' => '',
	        'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<p class="titulo">',
			'after_title'   => '</p>',
	    ) );

	    register_sidebar( array(
	        'name' => 'Footer - Colunna 4',
	        'id' => 'footer4',
	        'description' => '',
	        'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<p class="titulo">',
			'after_title'   => '</p>',
		) );

		
	

	}

	add_action( 'wpcf7_before_send_mail', 'before_send_mail_custom' );
	function before_send_mail_custom( $wpcf7 ) {

		// get the contact form object
	 	$wpcf = WPCF7_ContactForm::get_current();

	 	//Sem redirect de sucesso no form de parceiro
	    if( strpos( $_SERVER['REQUEST_URI'], 'parceiro' ) !== false ){ 
	    	$wpcf->set_properties(array(
		    	"additional_settings" => ""
		    ));
	    }

	}

	/**
	 * 
	 * MAILTRAP( APENAS LOCALHOST)
	 * Testar email em localhost
	 * 
	 */

	 if(strpos($_SERVER['SERVER_NAME'],".local.com.br") ){
		function mailtrap($phpmailer) {
			$phpmailer->isSMTP();
			$phpmailer->Host = 'smtp.mailtrap.io';
			$phpmailer->SMTPAuth = true;
			$phpmailer->Port = 2525;
			$phpmailer->Username = '068382fe510121';
			$phpmailer->Password = '23bdc271b14018';
		}	
		add_action('phpmailer_init', 'mailtrap');
	 }